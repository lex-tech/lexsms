<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerInfo extends Model
{
    protected $fillable = ['client', 'telephone_number', 'cellphone_number', 'email_address', 'address', 'postal_address'];
}
