<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $fillable = ['payment_status', 'payment_date'];

    /**
     * client and billing relationship
     */
    public function client(){
        return $this->belongsTo(User::class, 'owner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * the transactions that are part of this bill
     */

    public function credit_transactions(){
        return $this->hasMany(Credit::class, 'bill_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * returns the invoice if payment has been done
     */
    public function invoice(){
        return $this->hasOne(BillInvoice::class, 'bill');
    }
}
