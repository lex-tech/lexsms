<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 2/14/2018
 * Time: 12:42 PM
 */

namespace App\Traits;

use App\DailyMessages;
use App\MonthlyMessages;
use App\SmsCategory;
use App\User;
use App\WeeklyMessages;
use App\YearlyMessages;
use Carbon\Carbon;
use Faker\Provider\cs_CZ\DateTime;
use Illuminate\Http\Request;
use App\System;
use App\OzekiOut;

trait Messaging
{
    use Analysis;
    // send message function
    function send_message(Request $request, $sender, $recipient, &$number_of_sms_sent)
    {
        $sms_charge = System::all()->first()->default_sms_rate;
//        1 is a placeholder for now it should be dynamic
        if ($sender->sms_credit >= $sms_charge) {
            $message = OzekiOut::create(array_merge($request->only(['msg', 'category']),
                ['receiver' => $recipient],
                ['sender' => '80002'],
                ['status' => 'send']
            ));
//          add the sender relationship
            $sender->sent_messages()->save($message);
//          subtract sent sms
            $sender->sms_credit -= $sms_charge;
            $sender->save();
//          count and use later for the dashboard
            $this->match_time_relationships($this->counting_for_analytics($sender, $message));
//          count the number of sent sms
            $number_of_sms_sent++;
        } else {
            flash('Your SMS Credit has been depleted, you can request from the Administrator! Your message has been sent to ' . $number_of_sms_sent . ' recipients')->overlay();
            return redirect()->route('out.index');
        }
    }

    function sms_sent_per_day (OzekiOut $message, User $user){
//        find todays date
        $date = Carbon::today()->format('d/m/Y');

//       get al the rows that belong to this client
        $results = DailyMessages::whereRaw('client = '.$user->id, array(20))->get();
//         assume the dailyMessages that were sent are 0
        $dailyMessage = null;
//        check if user has sent a message for today
        $dailyMessage = $this->loop_through_days($results, $date);
//            if it exists just update
        if($dailyMessage){
            $dailyMessage->total_messages_for_the_day++;
            $dailyMessage->save();

            return $dailyMessage;
        }else{
//            else create one
            $dailyMessage = new DailyMessages();
            $dailyMessage->date = $date;
            $dailyMessage->total_messages_for_the_day++;
            $dailyMessage->status = true;
//        save to facts table
            $dailyMessage->save();
//            $dailyMessage->month()
            $user->daily_sent_messages()->save($dailyMessage);

            return $dailyMessage;
        }
    }

    function sms_sent_per_week (OzekiOut $message, User $user){
//        find todays week
        $date = Carbon::today();
        $week_today = $date->weekOfYear.'/'.$date->year;
//        dd(dump($week_today));

//       get all the rows that belong to this client
        $weeks = WeeklyMessages::whereRaw('client = '.$user->id, array(20))->get();

//         assume the weeklyMessages that were sent are 0
        $weeklyMessage = null;
//        check if user has sent a message for the week
        $weeklyMessage = $this->loop_through_weeks($weeks, $week_today);
//            if it exists just update
        if($weeklyMessage){
            $weeklyMessage->total_messages_for_the_week++;
            $weeklyMessage->save();

            return $weeklyMessage;
        }else{
//            else create one
            $weeklyMessage = new WeeklyMessages();
            $weeklyMessage->week = $week_today;
            $weeklyMessage->total_messages_for_the_week++;
            $weeklyMessage->status = true;
//        save to facts table
            $weeklyMessage->save();

            $user->weekly_sent_messages()->save($weeklyMessage);

            return $weeklyMessage;
        }
    }

    function sms_sent_per_month (OzekiOut $message, User $user){

        //        find todays week
        $date = Carbon::today();
        $month_today = $date->format('m/Y');
//        dd(dump($month_today));
//       get all the rows that belong to this client
        $months = MonthlyMessages::whereRaw('client = '.$user->id, array(20))->get();
//
//         assume the weeklyMessages that were sent are 0
        $monthlyMessage = null;
//        check if user has sent a message for the month
        $monthlyMessage = $this->loop_through_months($months, $month_today);
//            if it exists just update
        if($monthlyMessage){
            $monthlyMessage->total_messages_for_the_month++;
            $monthlyMessage->save();

            return $monthlyMessage;
        }else{
//            else create one
            $monthlyMessage = new MonthlyMessages();
            $monthlyMessage->month = $month_today;
            $monthlyMessage->total_messages_for_the_month++;
            $monthlyMessage->status = true;
//        save to facts table
            $monthlyMessage->save();

            $user->monthly_sent_messages()->save($monthlyMessage);

            return $monthlyMessage;
        }
    }

    function sms_sent_per_year (OzekiOut $message, User $user){
        //        find todays week
        $date = Carbon::today();
        $year_today = $date->format('Y');
//        dd(dump($year_today));
//       get all the rows that belong to this client
        $years = YearlyMessages::whereRaw('client = '.$user->id, array(20))->get();

//         assume the weeklyMessages that were sent are 0
        $yearlyMessage = null;
//        check if user has sent a message for the week
        $yearlyMessage = $this->loop_through_years($years, $year_today);
//            if it exists just update
        if($yearlyMessage){
            $yearlyMessage->total_messages_for_the_year++;
            $yearlyMessage->save();

            return $yearlyMessage;
        }else{
//            else create one
            $yearlyMessage = new YearlyMessages();
            $yearlyMessage->year = $year_today;
            $yearlyMessage->total_messages_for_the_year++;
            $yearlyMessage->status = true;
//        save to facts table
            $yearlyMessage->save();

            $user->yearly_sent_messages()->save($yearlyMessage);

            return $yearlyMessage;
        }
    }

    function match_time_relationships($time_array){
//        day relationship
//        dd()
        if ($time_array['day'] != null){
            if ( $time_array['week'] != null) {
                WeeklyMessages::find($time_array['week']->id)->days()->save(DailyMessages::find($time_array['day']->id));
            }
            if ($time_array['month'] != null)
                MonthlyMessages::find($time_array['month']->id)->days()->save(DailyMessages::find($time_array['day']->id));

            if ($time_array['year'] != null)
                YearlyMessages::find($time_array['year']->id)->days()->save(DailyMessages::find($time_array['day']->id));
        }
//        week relationship
        if ($time_array['week'] != null){
            if ($time_array['month'] != null)
                MonthlyMessages::find($time_array['month']->id)->weeks()->save(WeeklyMessages::find($time_array['week']->id));
            if ($time_array['year'] != null)
                YearlyMessages::find($time_array['year']->id)->weeks()->save(WeeklyMessages::find($time_array['week']->id));

        }

//        month relationship
        if ($time_array['month'] != null){
            if ($time_array['year'] != null)
                YearlyMessages::find($time_array['year']->id)->months()->save( MonthlyMessages::find($time_array['month']->id));
        }
    }

    /**
     * @param $actual_message
     * @param $recipient
     * @param User|null $sender
     */
    function send_notification_message($actual_message, $recipient,  User $sender = null)
    {
        OzekiOut::create(
            ['category' => SmsCategory::where('name','Notifications')->get()->first()->id,
                'msg' => $actual_message,
                'receiver' => $recipient,
                'sender' => '80002',
                'status' => 'send'
            ]
        );
    }
}