<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 2/19/2018
 * Time: 6:36 PM
 */

namespace App\Traits;


use App\DailyMessages;
use App\MonthlyMessages;
use App\User;
use App\WeeklyMessages;
use App\YearlyMessages;
use Carbon\Carbon;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Support\Facades\Auth;

trait AnalysisData
{
    function daily_stats(){

        $labels = []; $values = []; $total_messages_for_the_day = 0; $chart = null;

       if (Auth::user()->hasRole('Admin')){

           $messages = DailyMessages::all()->filter(function($message){
               return $message->date == Carbon::today()->format('d/m/Y');
           });

           foreach ($messages as $message){
               $total_messages_for_the_day += $message->total_messages_for_the_day;
           }

           foreach ($messages as $message){
               $labels[] = $message->sender->name;
               $values[] = $message->total_messages_for_the_day;
           }

           $chart = Charts::create('area', 'highcharts')
               ->title('Messages Sent Today By Each Client')
               ->template("material")
               ->elementLabel("Total")
               ->labels($labels)
               ->values($values)
               ->dimensions(0,500);
       }
       elseif (Auth::user()->hasRole('Client')){
            // analyse all the messages sent by this clients users
           $total_messages_for_the_day = 0;
           foreach (User::where('created_by', '=', Auth::id())->get() as $user){
               $today = false; $yesterday = false; $temp_user = null;
               $temp_user = $user;
                foreach ($user->daily_sent_messages as $message){
//                    if user has not sent any message today
                    if ($message->date ==  Carbon::today()->format('d/m/Y')){
                        $today = true;
                        $labels[$message->sender->name] = $message->sender->name;
                        $values_today[$message->sender->name] = $message->total_messages_for_the_day;
//                        move to the next iteration
                        break;
                    }

                    if ($message->date ==  Carbon::yesterday()->format('d/m/Y')){
                        $yesterday = true;
                        $labels[$message->sender->name] = $message->sender->name;
                        $values_yesterday[$message->sender->name] = $message->total_messages_for_the_day;
//                        move to the next iteration
                        break;
                    }
                }
                if (!$today){
                    $labels[$temp_user->name] = $user->name;
                    $values_today[$temp_user->name] = 0;
                }
                if (!$yesterday){
                    $labels[$temp_user->name] = $user->name;
                    $values_yesterday[$temp_user->name] = 0;
                }
           }

           foreach (Auth::user()->daily_sent_messages() as $message){
               if ($message->date = Carbon::today()->format('d/m/Y')){
                   $total_messages_for_the_day = $message->total_messages_for_the_day;
               }
           }

           $d_labels = null; $d_today = null; $d_yesterday = null;
           foreach ($labels as $user_data){
//               dd($labels);
               $d_labels[] = $labels[$user_data];
               $d_today[] = $values_today[$user_data];
               $d_yesterday[] = $values_yesterday[$user_data];
           }

           $chart = Charts::multi('areaspline', 'highcharts')
               ->title('My Users Message Chart')
               ->colors(['#ff0000', '#0ff000'])
               ->responsive(true)
               ->labels($d_labels)
               ->elementLabel("Total Messages")
               ->dataset('Yesterday', $d_yesterday)
               ->dataset('Today',  $d_today);
       }
       elseif (Auth::user()->hasRole('Sub-Client')){
           $messages = [];
           $total_messages_for_the_day = 0;

           foreach( DailyMessages::all() as $message_per_day){
               if (($message_per_day->date == Carbon::today()->format('d/m/Y') || $message_per_day->date == Carbon::yesterday()->format('d/m/Y'))  && $message_per_day->sender->id == Auth::id()){
                   $messages[] = $message_per_day;
                   if ($message_per_day->date == Carbon::today()->format('d/m/Y')){
                       $total_messages_for_the_day = $message_per_day->total_messages_for_the_day;
                   }
               }
           }

           foreach ($messages as $message){
               if ($message->date == Carbon::yesterday()->format('d/m/Y')) {
                   $labels[] = 'Yesterday';
                   $values[] = $message->total_messages_for_the_day;
               }
               if ($message->date == Carbon::today()->format('d/m/Y')) {
                   $labels[] = 'Today';
                   $values[] = $message->total_messages_for_the_day;
               }
           }

           $chart = Charts::create('bar', 'highcharts')
               ->title('Messages You have Sent The Previous Day and Today')
               ->template("material")
               ->elementLabel("Total")
               ->labels($labels)
               ->values($values)
               ->dimensions(0,500);

       }

        return [ 'chart' => $chart , 'total_messages_for_the_day' => $total_messages_for_the_day];
    }

    function monthly_stats(){
        $total_messages_for_the_month = 0;
        $chart = null;
        $message = null;

       if (Auth::user()->hasRole('Admin')){
           $users = User::all()->filter(function (User $user){
               return !$user->hasRole(['Admin']);
           });
           $values = MonthlyMessages::whereIn('client',$users->pluck('id')->toArray())->get()->filter(function ($data){
               return $data->month == Carbon::today()->format('m/Y');
           });

           foreach ($values as $value){
               $total_messages_for_the_month += $value->total_messages_for_the_month;
           }

//           $total_messages_for_the_month = MonthlyMessages::where('client', Auth::id())
//                                                            ->where('month', Carbon::today()->format('m/Y'))
//                                                            ->get()
//                                                            ->total_messages_for_the_month;

           $chart = Charts::create('area', 'highcharts')
               ->title('Users Current Month Message Chart')
               ->template("material")
               ->elementLabel("Total Messages")
               ->labels($values->pluck('sender.name')->toArray())
               ->values($values->pluck('total_messages_for_the_month')->toArray())
               ->dimensions(0,500);
       }
       elseif (Auth::user()->hasRole('Client')){

          $users = User::all()->filter(function (User $user){
              if (!$user->hasRole(['Admin'])){
                  return ($user->id == Auth::id()) || $user->user_created_by->id == Auth::id();
              }
              return;
           });
           $values = MonthlyMessages::whereIn('client',$users->pluck('id')->toArray())->get()->filter(function ($data){
               return $data->month == Carbon::today()->format('m/Y');
           });

           foreach ($values as $value){
               if ($value->sender->id == Auth::id())
                   $total_messages_for_the_month = $value->total_messages_for_the_month; break;
           }

//           $total_messages_for_the_month = MonthlyMessages::where('client', Auth::id())
//                                                            ->where('month', Carbon::today()->format('m/Y'))
//                                                            ->get()
//                                                            ->total_messages_for_the_month;

           $chart = Charts::create('area', 'highcharts')
               ->title('My Users Message Chart')
               ->template("material")
               ->elementLabel("Total Messages")
               ->labels($values->pluck('sender.name')->toArray())
               ->values($values->pluck('total_messages_for_the_month')->toArray())
               ->dimensions(0,500);

       }
       elseif (Auth::user()->hasRole('Sub-Client')){

           $user = Auth::user();
           $values = MonthlyMessages::whereIn('client',[$user->id])->get()->filter(function ($data) use ($total_messages_for_the_month){
               return $data->month == Carbon::today()->format('m/Y');
           });

           $chart = Charts::create('bar', 'highcharts')
               ->title('Messages You Have Sent This Month.')
               ->template("material")
               ->elementLabel("Total Messages")
               ->labels($values->pluck('sender.name')->toArray())
               ->values($values->pluck('total_messages_for_the_month')->toArray())
               ->dimensions(0,500);
       }

        return [ 'chart' => $chart , 'total_messages_for_the_month' => $total_messages_for_the_month];
    }
}