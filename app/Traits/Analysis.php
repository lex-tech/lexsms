<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 2/15/2018
 * Time: 5:05 PM
 */

namespace App\Traits;


trait Analysis
{
    /**
     * @param $sender
     * @param $message
     */
    public function counting_for_analytics($sender, $message)
    {
        //        count sent messages for day
        $day = $this->sms_sent_per_day($message, $sender);

        //        count sent messages per week
        $week = $this->sms_sent_per_week($message, $sender);

        //        count sent messages for the month
        $month = $this->sms_sent_per_month($message, $sender);

        //        count the messages sent for the year
        $year = $this->sms_sent_per_year($message, $sender);

        return ['day' => $day, 'week' => $week, 'month' => $month, 'year' => $year];
    }

    /**
     * @param $years
     * @param $year_today
     * @return mixed
     */
    public function loop_through_years($years, $year_today)
    {
        foreach ($years as $year) {
            if ((string) $year->year == (string) $year_today) {
                return $year;
            }
        }
    }

    /**
     * @param $months
     * @param $month_today
     * @return mixed
     */
    public function loop_through_months($months, $month_today)
    {
        foreach ($months as $month) {
            if ((string) $month->month == (string) $month_today) {
                return $month;
            }
        }
    }

    /**
     * @param $weeks
     * @param $week_today
     * @return mixed
     */
    public function loop_through_weeks($weeks, $week_today)
    {
        foreach ($weeks as $week) {
            if ((string)$week->week == (string)$week_today) {
                return $week;
            }
        }
    }

    /**
     * @param $days
     * @param $date
     * @return mixed
     */
    public function loop_through_days($days, $date)
    {
        foreach ($days as $result) {
            if ((string)$result->date == (string)$date) {
                return $result;
            }
        }
    }

}