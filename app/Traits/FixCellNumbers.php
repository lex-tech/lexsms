<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 4/5/2018
 * Time: 3:45 PM
 */

namespace App\Traits;


trait FixCellNumbers
{
    /**
     * @param $row
     * @return string
     */
    private function validate_cellphone_number($row)
    {
//        if it starts with a zero remove the zero
        $new_number = $this->strip_zero($row['number']) ;
//        after removing the zero prepend the country code '+264'
        $new_number = $this->prepend_country_code($new_number);
//                        if it starts with a 2 add +
        $new_number = $this->prepend_plus($new_number);
//                        removed decimal if there is one
        return $new_number;
    }

    /**
     * @param $cellphone_number
     * @return string
     */
//    public function prepend_zero($cellphone_number)
//    {
////        check if there is no zero
//        if (substr($cellphone_number, 0, 1) == '8') {
////            prepend a (0)
//            $new_number = '0' . $cellphone_number;
////            dd(dump($new_number));
//            return $new_number;
//        }
//        return $cellphone_number;
//    }

    /**
     * @param $cellphone_number
     * @return mixed
     */
//    public function remove_decimal($cellphone_number)
//    {
//        $pattern = "/.0$/";
//        if (preg_match($pattern, $cellphone_number)) {
////          remove decimal sign
//            $new_number = substr_replace($cellphone_number, "", -2);
////            dd(dump($new_number));
//            return $new_number;
//        }
//
//        return $cellphone_number;
//    }
    /**
     * @param $cellphone_number
     * @return string
     */
    public function prepend_plus($cellphone_number)
    {
        if (substr($cellphone_number, 0, 1) == '2') {
//            prepend a plus (+) sign
            $new_number = '+' . $cellphone_number;
//            dd(dump($new_number));
            return $new_number;
        }
        return $cellphone_number;
    }

    /**
     * @param $number
     * @return string
     */
    private function prepend_country_code($number){
        // if the string starts with 0
        if (substr($number, 0, 2) == '81') {
//            prepend a plus (+) sign
            $new_number = '+264' . $number;
            return $new_number;
        }
        return $number;
    }

    /**
     * @param $number
     * @return string
     */
    private function strip_zero($number){
        if (substr($number, 0, 3) == '081') {
//            remove the zero at the beginning
            $new_number = str_replace_first('0','',$number);
            return $new_number;
        }
        return $number;
    }
}