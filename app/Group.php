<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model 
{

    protected $table = 'groups';
    public $timestamps = true;
    protected $fillable = array('name', 'description', 'number_of_members');

    public function recipients(){
        return $this->belongsToMany(Recipient::class, 'group_recipients', 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * the creator of this group
     */
    public function creator(){
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     * user authorized to access this group
     */
    public function authorized_users(){
        return $this->belongsToMany(User::class, 'group_user', 'group_id');
    }

}