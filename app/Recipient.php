<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{

    protected $table = 'recipients';
    public $timestamps = true;
    protected $fillable = array('name', 'cellphone_number');

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     * the groups this contact belongs to
     */
    public function groups(){
        return $this->belongsToMany(Group::class, 'group_recipients', 'recipient_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *  the creator of this contact
     */

    public function creator(){
        return $this->belongsTo(User::class,'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     * users authorized to access this recipient
     */
    public function authorized_users(){
        return $this->belongsToMany(User::class, 'recipient_user', 'recipient_id');
    }

}