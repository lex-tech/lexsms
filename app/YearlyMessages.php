<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YearlyMessages extends Model
{
    protected $fillable = ['year', 'client', 'total_messages_for_the_year', 'status'];

    public function sender(){
        return $this->belongsTo(User::class, 'client');
    }

    public function days()
    {
        return $this->hasMany(DailyMessages::class, 'year');
    }

    public function weeks()
    {
        return $this->hasMany(WeeklyMessages::class, 'year');
    }

    public function months()
    {
        return $this->hasMany(MonthlyMessages::class, 'year');
    }
}
