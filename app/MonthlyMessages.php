<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyMessages extends Model
{
    protected $fillable = ['month', 'client', 'total_messages_for_the_month', 'year', 'status'];


    public function sender()
    {
        return $this->belongsTo(User::class, 'client');
    }

    public function days()
    {
        return $this->hasMany(DailyMessages::class, 'month');
    }

    public function weeks()
    {
        return $this->hasMany(WeeklyMessages::class, 'week_belongs_to_a_month');
    }

    public function year()
    {
        return $this->belongsTo(YearlyMessages::class, 'year');
    }
}