<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsCategory extends Model
{
    public $fillable = ['name', 'description'];

    public function messages(){
        return $this->hasMany(OzekiOut::class, 'category');
    }
}
