<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeeklyMessages extends Model
{
    protected $fillable = ['week', 'client', 'week_belongs_to_a_month', 'year', 'total_messages_for_the_week', 'status'];

    public function sender(){
        return $this->belongsTo(User::class, 'client');
    }

    public function days()
    {
        return $this->hasMany(DailyMessages::class, 'week');
    }

    public function month()
    {
        return $this->belongsTo(MonthlyMessages::class, 'week_belongs_to_a_month');
    }

    public function year()
    {
        return $this->belongsTo(YearlyMessages::class, 'year');
    }
}
