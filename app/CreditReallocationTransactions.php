<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditReallocationTransactions extends Model
{
    public $fillable = ['transaction_made_by','credit_sent_from','credit_sent_to','credit'];


    public function transaction_owner(){
        return $this->belongsTo(User::class, 'transaction_made_by');
    }

    public function credit_reallocated_from(){
        return $this->belongsTo(User::class, 'credit_sent_from');
    }

    public function credit_reallocated_to(){
        return $this->belongsTo(User::class, 'credit_sent_to');
    }
}
