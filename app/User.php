<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\MembershipUser;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'created_by', 'cellphone_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * mutator to encrypt out password field always
     *
    */

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function setPasswordAttribute($password){   
        $this->attributes['password'] = bcrypt($password);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany]
     * every user (admin / client can create many users)
     */
    public function creator(){
        return $this->hasMany(User::class, 'created_by');
    }

    /**
     * return the creator of the this user
     */
    public function user_created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * every user can send may messages
     */
    public function sent_messages(){
        return $this->hasMany(OzekiOut::class,'senderId');
    }

    /**
     *  @return \Illuminate\Database\Eloquent\Relations\HasMany
     *  every user (admin) can make many credit transactions
     */
    public function transactions_made(){
        return $this->hasMany(Credit::class, 'transaction_made_by');
    }

    /**
     *  @return \Illuminate\Database\Eloquent\Relations\HasMany
     *  client can be involved in many credit transactions
     */
    public function credit_transactions(){
        return $this->hasMany(Credit::class, 'client_id');
    }
    /**
     *  subscriptions relations
     *  client can have one subscription at a time only
     */
    public function subscription(){
        return $this->belongsTo(Subscription::class, 'subscription');
    }

    /**
     *  membership relations
     *  client can have many memberships
     */
    public function membership(){
        return $this->belongsToMany('App\Membership');
    }

    /**
     *  credit requests relation
     *  user can make many credit requests
     */
    public function credit_request(){
        return $this->hasMany(CreditRequest::class, 'requested_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * reallocation transaction maker
     */
    public function credit_reallocation_transaction_owner(){
        return $this->hasMany(CreditReallocationTransactions::class, 'transaction_made_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * credit reallocated from relation
     */
    public function credit_reallocation_from(){
        return $this->hasMany(CreditReallocationTransactions::class, 'credit_sent_from');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * credit reallocated to relation
     */
    public function credit_reallocation_to(){
        return $this->hasMany(CreditReallocationTransactions::class, 'credit_sent_to');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * created recipients relation
     */
    public function recipients(){
        return $this->hasMany(Recipient::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * created groups relation
     */
    public function groups(){
        return $this->hasMany(Group::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * billings and user relation
     */
    public function bills(){
        return $this->hasMany(Billing::class, 'owner');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * each client has contact details
     */
    public function contacts(){
        return $this->hasOne(CustomerInfo::class, 'client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *  the recipient groups a user is allowed to access
     */
    public function authorized_groups(){
        return $this->belongsToMany(Group::class, 'group_user', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * the contacts a user is allowed to access
     */
    public function authorized_contacts(){
            return $this->belongsToMany(Recipient::class, 'recipient_user', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * get the number of messages sent by a client per day
     */
    public function daily_sent_messages(){
        return $this->hasMany(DailyMessages::class, 'client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * get the number of messages sent by a client per week
     */
    public function weekly_sent_messages(){
        return $this->hasMany(WeeklyMessages::class, 'client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * get the number of messages sent by a client per month
     */
    public function monthly_sent_messages(){
        return $this->hasMany(MonthlyMessages::class, 'client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * get the number of messages sent by a client per year
     */
    public function yearly_sent_messages(){
        return $this->hasMany(YearlyMessages::class, 'client');
    }

}

