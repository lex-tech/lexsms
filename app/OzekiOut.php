<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OzekiOut extends Model
{
    public $timestamps = false;
    public $guarded = ['id', 'reference', 'recievedtime', 'operator', 'errormsg' ];
    protected $table = 'ozekimessageout';

    public function sent_by(){
        return $this->belongsTo(User::class, 'senderId');
    }

    public function category(){
        return $this->belongsTo(SmsCategory::class, 'category');
    }
}
