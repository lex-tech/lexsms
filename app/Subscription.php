<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model 
{

    protected $table = 'subscription';
    public $timestamps = true;
    protected $fillable = array('name', 'duration', 'sms_number');

    public function subscribers(){
        return $this->hasMany(User::class, 'subscription');
    }

}