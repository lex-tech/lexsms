<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class System extends Model 
{

    protected $table = 'system';
    public $timestamps = true;

    protected $fillable = array('telephone', 'email', 'address', 'default_sms_rate');

}