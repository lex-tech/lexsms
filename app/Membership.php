<?php
/**
 * Created by PhpStorm.
 * User: Tonata
 * Date: 18/12/2017
 * Time: 12:29
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MembershipUser;

class Membership extends Model{

    protected $table = 'membership';

    protected $fillable = ['name', 'start_date', 'end_date', 'keyword'];

//    public function members(){
//        return $this->belongsTo(MembershipUser::class, 'membership_id');
//    }

      public function users(){
          return $this->belongsToMany('App\User');
      }

}