<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyMessages extends Model
{
    protected $fillable = ['date', 'client', 'total_messages_for_the_day', 'status', 'week', 'month', 'year'];

    public function sender(){
        return $this->belongsTo(User::class, 'client');
    }

    public function week(){
        return $this->belongsTo(WeeklyMessages::class, 'week');
    }

    public function month(){
        return $this->belongsTo(MonthlyMessages::class, 'month');
    }

    public function year(){
        return $this->belongsTo(YearlyMessages::class, 'year');
    }
}
