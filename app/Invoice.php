<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillInvoice extends Model
{
    protected $fillable = ['url', 'bill'];
    protected $table = 'invoices';
    public function bill(){
        return $this->belongsTo(Billing::class, 'bill');
    }
}
