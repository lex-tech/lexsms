<?php

namespace App\Http\Controllers;

use App\CustomerInfo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Spatie\Permission\Models\Role;
use Validator;

class CustomerInfoController extends Controller
{
    /**
     * CustomerInfoController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerInfo  $customerInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerInfo $customerInfo)
    {
        $user = Auth::user();
        $roles = Role::get();

        request()->session()->flash('tab', 'update_profile');

        return view('profile.edit', compact('user', 'roles'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail(Crypt::decrypt($id));
        $contact_details = null;

        $validator = Validator::make($request->all(), [
            'telephone_number'=>'required',
            'cellphone_number'=>'required',
            'email_address'=>'required',

        ]);

        if ($validator->fails()){
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput()
                ->with('tab', 'update_contact_info');
        }

        $input = $request->all(); //Retreive the name, email and password fields
        //        if no contact exists
        if ($user->contacts === null){

            $contact_details = new CustomerInfo();
            $contact_details->save();

            $contact_details->fill($input)->save();
//            append the relationship
            $user->contacts()->save($contact_details);
        }else{
//            update the contact details if the model exists already
            $contact_details = $user->contacts;
            $contact_details->fill($input)->save();
        }

//      flash a message to notify the user of the update
        flash('Account Contact Details Updated!')->overlay();

        return redirect()->back();
    }
}