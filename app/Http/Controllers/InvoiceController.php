<?php

namespace App\Http\Controllers;

use App\BillInvoice;
use Illuminate\Http\Request;
use Validator;

class InvoiceController extends Controller
{
    /**
     * InvoiceController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'month' => 'required',
        'invoice' => 'required|mimes:pdf|max:10000'
        ]);

        if ($validator->fails()){
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput()
                ->with('tab', 'upload_invoice');
        }

        $invoice = new BillInvoice();
        $invoice->url = $request->invoice->store('/','pdf');
        $invoice->bill = $request->month[0];
        $invoice->save();

        flash('Invoice successfully uploaded!')->success()->important();
        return redirect()->back();
    }

    /**
     * @param BillInvoice $invoice
     * @return mixed
     */
    public function show(BillInvoice $invoice){
        return response()->file('pdf/'.$invoice->url);
    }

}
