<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use App\Membership;
use App\MembershipUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Input;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;



class MembershipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = Auth::user();
        $all_users = User::all();
        $groups = Group::all();
        $memberships = Membership::class;
        $clients = [];

        foreach($all_users as $user)
        {
            if ($user->hasRole('Client')){
                $clients[] = $user;
            }
        }

        if ($current_user->hasRole('Admin'))
        {
            $memberships = Membership::all();
        }

        if($current_user->hasRole('Client'))
        {
            $memberships =  $current_user->membership()->get();
        }


//        foreach ($users as $user){
//            if ($user->hasRole('Client')) {
//                $clients[] = $user;
//               // $memberships =  $user->membership()->find();
//               // dd($user);
//
//            }
//        }


        return view('memberships.index', compact('memberships', 'clients', 'groups'))->with('tab', 'view_memberships');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('memberships.create');
    }

    /**
     * Show the form for the users memberships resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

//        public function getClientMemberships(Request $request){
//            $user = Users::findorFail($request['client']);
//            $memberships = $user->membership();
//            //return $memberships;
//            return view('memberships.getClientMemberships', compact('memberships'))->with('tab', 'view_client_memberships');
//        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request['membership-form'])){
            $validator = Validator::make($request->all(), [
                'memberships' => 'required',
                'client' => 'required',
            ]);

            if ($validator->fails()){
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('tab', 'create_client_memberships');
            }

            $client = User::findorfail($request['client']);
            $memberships = Membership::findorfail($request['memberships']);

//            subscribe client
            foreach ($memberships as $membership){
                if ($client->membership()->find($membership->id) == null){
                    $membership->users()->save($client);
                    flash('Client, '. $client->name.' successfully enrolled to '.$membership->name)->success();
                }
                else{
                    flash('Client, '. $client->name.' has already been enrolled to ' .$membership->name)->error();
                }
            }

            return redirect()->route('memberships.index');
        }else{
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'keyword' => 'required|string|max:100',
                'start_date' => 'date|min:0',
                'end_date'  => 'date|min:0'
            ]);

            if ($validator->fails()){
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('tab', 'create_memberships');
            }

            $name = $request['name'];
            $keyword = $request['duration'];
            $start_date = $request['start_date'];
            $end_date = $request['end_date'];

            $membership = Membership::create($request->only('name', 'keyword', 'start_date', 'end_date'));

            $membership->users()->save(Auth::user());

            //Display a successful message upon save
            flash('Membership, '. $membership->name.' created.')->success();

            return redirect()->route('memberships.index');
            //->with('tab', 'create_memberships');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $user = Users::findorFail($request['client']);
        $memberships = $user->membership()->get();
        //return $memberships;
        return view('memberships.index', compact('memberships'))->with('tab', 'view_client_memberships');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Get user with specified id
        $membership = Membership::findOrFail($id);
//        $retrievedStartDate = $membership->start_date;
//        $retrievedEndDate = $membership->end_date;

        //dd($retrievedEndDate);

//        $newStart = Carbon::createFromFormat('MMM-dd-yyyy', 'h-mm-ss-tt', 'K', $retrievedStartDate );
//        $newEnd = Carbon::createFromFormat('MMM-dd-yyyy', 'h-mm-ss-tt', 'K',$retrievedEndDate );



//        $membership->start_date = $newStart;
//        $membership->end_date = $newEnd;

        //Get all roles
        //$roles = Role::get();

        //pass user and roles data to view
        return view('memberships.edit', compact('membership'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $membership = Membership::findOrFail($id); //Get role specified by id

        //Validate name, keyword, start date and end date fields
        $this->validate($request, [
            'name'=>'required|max:100',
            'keyword'=>'required',
            'start_date',
            'end_date'
        ]);

        //Retrieve the name, keyword start date and end date fields
        $input = $request->only(['name', 'keyword', 'start_date', 'end_date']);

        $membership->fill($input)->save();

//        if (isset($roles)) {
//            $subscriptions->roles()->sync($roles);  //If one or more role is selected associate subscriptions to roles
//        }
//        else {
//            $subscriptions->roles()->detach(); //If no role is selected remove exisiting role associated to a subscriptions
//        }

        flash('Membership, '. $membership->name. ' updated.')->overlay();

        return redirect()->route('memberships.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find a user with a given id and delete
        $membership = Membership::findOrFail($id);
        $membership->delete();

        flash('<div> Membership, <strong>'.  $membership->name .'</strong> deleted!</div>')->overlay();

        return redirect()->route('memberships.index');


    }
}
