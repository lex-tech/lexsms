<?php

namespace App\Http\Controllers;

use App\Billing;
use App\Credit;
use App\CreditReallocationTransactions;
use App\CreditRequest;
use App\System;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Traits\Messaging;
class CreditController extends Controller
{
    use Messaging;
    /**
     * CreditController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Credit::all();
//        load this object with all the relationships
        $credit_reallocation_transactions = CreditReallocationTransactions::with(
            ['transaction_owner',
            'credit_reallocated_from',
            'credit_reallocated_to']
        )->get();

//        all the user in the database
        $users = User::all();
        $clients = [];
//        if authenticated user is a client give them access to their clients
        if (Auth::user()->hasRole('Client')){
            foreach ($users as $user){
                if ($user->hasRole('Sub-Client') && $user->user_created_by->id == Auth::id()){
                    $clients[] = $user;
                }
            }
            // get the requests made by this client or those made by users created by this user
            $credit_requests = CreditRequest::all()->filter(function (CreditRequest $credit_request){
                return $credit_request->request_made_by->id == Auth::id() || $credit_request->request_made_by->user_created_by->id == Auth::id();
            });
        }

//        if the current authenticated user is an admin give them access to all the clients
        if (Auth::user()->hasRole('Admin')){
            foreach ($users as $user){
                if ($user->hasRole('Client')){
                    $clients[] = $user;
                }
            }
            $credit_requests = CreditRequest::all();
        }
//        if (Auth::user()->hasRole('Admin')){
////            dd(session('tab'));
////            request()->session()->now('tab', 'view_credit_transactions');
//        }elseif (Auth::user()->hasRole('Client')){
////            dd(session('tab'));
////            request()->session()->now('tab', 'view_credit_requests');
//        }else{
//            request()->session()->now('tab', 'request_credit');
//        }
        return view('credit.index', compact('transactions', 'clients','credit_reallocation_transactions', 'credit_requests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//          get the current logged in user
        $transactor = Auth::user();
      if (isset($request['credit-request-form'])){
          $validator = Validator::make($request->all(), [
              'credit'=>'required|numeric|min:0',
          ]);

          if ($validator->fails()){
              return redirect()
                  ->route('credits.index')
                  ->withErrors($validator)
                  ->withInput()
                  ->with('tab', 'request_credit');
          }

//          create the credit request
          $creditRequest = CreditRequest::create($request->all());
          $transactor->credit_request()->save($creditRequest);

//          notify the Administrator about the credit request event
          $this->send_notification_message(Auth::user()->name. ' has requested '. $request->credit .' SMS Credit. Please allocate the credit now! '.config('app.name'), Auth::user()->user_created_by->cellphone_number);
//          flash a message with name of the the person who created this user
          flash('Credit request has been sent to '. $transactor->user_created_by->name);
//          redirect to the credits index page
          return redirect()->route('credits.index');

      }
      if (isset($request['reallocate-credit-form'])){

          if ($request['reallocate_from'] != null){
              $max_credit = User::find($request['reallocate_from'])->sms_credit;
          }

//          dd(var_dump($max_credit));
          $validator = Validator::make($request->all(), [
              'credit'=>'required|numeric|min:0'.(isset($max_credit)? '|max:'.$max_credit:''),
              'reallocate_from'=>'required|numeric',
              'reallocate_to'=>'required|numeric',
          ]);
//          check if validation has failed
          if ($validator->fails()){
              return redirect()
                  ->route('credits.index')
                  ->withErrors($validator)
                  ->withInput()
                  ->with('tab', 'reallocate_credit');
          }
//          reallocate the credit
          $this->reallocate_credit($request);

          return redirect()->back()->with('tab','reallocate_credit');

      } else{
//      allocate credit to client
        $validator = Validator::make($request->all(), [
            'credit'=>'required',
            'client'=>'required',
        ]);

          if ($validator->fails()){
              return redirect()
                  ->route('credits.index')
                  ->withErrors($validator)
                  ->withInput()
                  ->with('tab', 'allocate_credit');
          }

//          get the client being assigned credit to
        $client = User::findorfail($request['client']);

        $transaction = Credit::create(array_merge($request->only('credit'))); //Retrieving the credit transaction
        $transactor->transactions_made()->save($transaction);
        $client->credit_transactions()->save($transaction);

//      allocate (append) the new credit to the client
        $client->sms_credit += $request['credit'];
        $client->save();

//          add the transaction to the current month's bill
//          check if user has client role
          if ($client->hasRole('Client')){

              $date = Carbon::now();
              $month = $date->format('m/Y');

//          check if current month billing instance for client exists
              $results = Billing::whereRaw('owner = '.$client->id, array(20))->get();
              $bill = null;
              foreach ($results as $result){
                  if ( (string) $result->subscription_month == (string) $month){
                      $bill = $result;
                      break;
                  }
              }
              if ($bill){
//              update bill amount
                  $bill->amount += System::all()->first()->default_sms_rate * $transaction->credit;
                  $bill->save();
              }else{
//          create new billing instance if it does not exists already
                  $bill = new Billing();
                  $bill->subscription_month = $month;
//              increment bill amount
                  $bill->amount += System::all()->first()->default_sms_rate * $transaction->credit;
                  $bill->save();
//              add the owner of the bill
                  $client->bills()->save($bill);
              }
//          add transaction to it bill
              $bill->credit_transactions()->save($transaction);

          }

          // notify the Client Receiving Credit
          $this->send_notification_message(Auth::user()->name. ' has allocated you with '. $request->credit.' SMS credit! '.config('app.name'), $client->cellphone_number);

        flash('Credit of '. $request['credit'] . ' allocated to '. $client->name . '! ')->success();
        if ($request->has('credit_request_id')){
            $creditRequest = CreditRequest::findOrFail($request->credit_request_id);

            $creditRequest->noted = 1;
            $creditRequest->save();
            return redirect()->back()->with('tab', 'view_credit_requests');
        }
        return redirect()->route('credits.index')->with('tab', 'allocate_credit');
      }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){

        return $this->store($request);
    }

    /**
     * @param Request $request
     */
    private function reallocate_credit(Request $request)
    {
//          get the client loosing the credit
        $from_client = User::findorfail($request['reallocate_from']);
//          get the client getting the credit
        $to_client = User::findorfail($request['reallocate_to']);

//          subtract credit from client
        $from_client->sms_credit -= $request['credit'];
        $from_client->save();

//          add credit to client receiving the credit
        $to_client->sms_credit += $request['credit'];
        $to_client->save();

//          record transaction into the database
        $creditReallocation = CreditReallocationTransactions::create($request->only('credit'));

//          associate relationships
        Auth::user()->credit_reallocation_transaction_owner()->save($creditReallocation);

        $from_client->credit_reallocation_from()->save($creditReallocation);

        $to_client->credit_reallocation_to()->save($creditReallocation);

        flash('Credit of '.$request['credit'].' has been reallocated from '.$from_client->name .' to '.$to_client->name.'.');
    }
}
