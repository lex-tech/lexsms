<?php

namespace App\Http\Controllers;

use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        $daily_stats = $this->daily_stats();
//        $daily_stats_chart = $daily_stats['chart'];
//        $total_messages_for_the_day = $daily_stats['total_messages_for_the_day'];

        $monthly_stats = $this->monthly_stats();
        $monthly_stats_chart = $monthly_stats['chart'];
        $total_messages_for_the_month = $monthly_stats['total_messages_for_the_month'];

        return view('dashboard', compact('daily_stats_chart',  'total_messages_for_the_day', 'monthly_stats_chart' , 'total_messages_for_the_month', 'weekly_stats_chart',  'total_messages_for_the_week', 'yearly_stats_chart',  'total_messages_for_the_year'));
    }
}
