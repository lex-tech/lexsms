<?php

namespace App\Http\Controllers;

use App\Group;
use App\OzekiOut;
use App\Recipient;
use App\SmsCategory;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class OzekiOutController extends Controller
{
    public function  __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        get all the sms_categories
        $categories = SmsCategory::all();
//        get all the message that were sent by a user
        $outbox_messages = OzekiOut::where('senderId','!=', null)->orderby('id', 'desc')->with(['sent_by'])->get(); // show only 5 items at a time
        $messages = [];
        if (Auth::user()->hasRole('Admin')){
//            assign array to another variable for display on the frontend
            $messages = $outbox_messages;
        }
        if (Auth::user()->hasRole('Client')){
            foreach ($outbox_messages as $outbox_message){
//                check if the message was sent by one of this clients sub-clients
                // or by this client
                // find the the user who sent this message,
                // and check if they were created by the authenticated client
//                dd(dump($outbox_message->sent_by->creator));
                if ($outbox_message->sent_by->creator === Auth::user() || $outbox_message->sent_by->id === Auth::id())
                    $messages[] = $outbox_message;
            }
        }
        if (Auth::user()->hasRole('Sub-Client')){
            foreach ($outbox_messages as $outbox_message){
//                check if the message was sent by this subclient
//                find the user that sent this message and check if it is the authenticated sub client user
                if ($outbox_message->sent_by->id === Auth::id() )
                    $messages[] = $outbox_message;
            }
        }

        // user should only have access to the recipients they created or have permissions to access
        $recipients = Recipient::all()->filter(function($recipient){
            return $recipient->creator->id == Auth::id();
        });
        // users should only have access to the groups they created or have permission to access
        $groups = Group::all()->filter(function ($group){
            return $group->creator->id == Auth::id() || $group->authorized_users->contains(Auth::id());
        });

        request()->session()->flash('tab', 'view_outbox');
        return view('messaging.out.index', compact('messages', 'recipients', 'groups', 'categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate message and receiver fields
//        dd(var_dump($request['groups']));
        $sender = User::find(Auth::id());
        $number_of_sms_sent = 0;

        if (isset($request['bulk-message'])){
            $validator = Validator::make($request->all(), [
                'msg'=>'required|max:160',
                'groups'=>'required',
                'category' => 'required'
            ]);

            if ($validator->fails()){
                return redirect()
                    ->route('out.index')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('tab', 'create_bulk');
            }

            foreach ($request['groups'] as $group ){

//                find all the recipients in this group using the relationship
                $recipients = Group::findorfail($group)->recipients;

//                dd(var_dump($recipients->toJson()));
                foreach ($recipients as $recipient) {
//                    get the recipients number, and pass it through to the send message method when you call it
                    $recipient_number = $recipient->cellphone_number;
                    $this->send_message($request, $sender, $recipient_number, $number_of_sms_sent);
                }
            }

        }else{
            $validator = Validator::make($request->all(), [
                'msg'=>'required|max:160',
                'receiver'=>'required',
                'category' => 'required'
            ]);

            if ($validator->fails()){
                return redirect()
                    ->route('out.index')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('tab', 'create_custom');
            }

            foreach ($request['receiver'] as $recipient ){
                $this->send_message($request, $sender, $recipient, $number_of_sms_sent);
            }
        }

        flash($number_of_sms_sent.' messages added to the outbox queue!')->important()->success();
        /**
         * Redirect to the out.index view and flash messages
         *
         * and return to the recent tab
         */
        return redirect()->route('out.index')->with('tab', isset($request['bulk-message'])? 'create_bulk' : 'create_custom');
    }

}
