<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Validator;
use Illuminate\Http\Request;

use App\User;
use Auth;

// importing laravel-permission models
use Spatie\Permission\Models\Role;

//Enables us to output flash messaging
use Session;
use App\Traits\FixCellNumbers;


class UserController extends Controller
{
    use FixCellNumbers;
    /**
     * UserController constructor.
     */
    public function __construct(){
         $this->middleware(['auth']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all users and pass it to the view
        if (Auth::user()->hasRole(['Client', 'Demo'])){
            $users = User::where('created_by','=', Auth::id())->get();
//        used by the create users form
            $roles = Role::get();
        }
        elseif (Auth::user()->hasRole('Admin')){
//            logged in admin should not see their account among the users
            $users = User::all()->filter(function (User $user){
                return $user->id != \Illuminate\Support\Facades\Auth::id();
            });
//        used by the create users form
            $roles = Role::where('name', '!=', 'Sub-Client')->get();
        }

        request()->session()->flash('tab', 'view_users');
        return view('users.index', compact('users', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name, email and password fields
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'cellphone_number' => 'required|max:14',
            'password'=>'required|min:6|confirmed'
        ]);

        if ($validator->fails()){
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput()
                ->with('tab', 'create_users');
        }
        $new_cellphone_number = $this->validate_cellphone_number($request->cellphone_number);
        $user = User::create(array_merge($request->only('email', 'name', 'cellphone_number' , 'password'), ['cellphone_number' => $new_cellphone_number])); //Retrieving only the email and password data

//        assign created by relation
        $creator = User::find(Auth::id());
        $creator->creator()->save($user);

        $roles = $request['roles']; //Retrieving the roles field
//        Checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
            $role_r = Role::where('id', '=', $role)->firstOrFail();            
            $user->assignRole($role_r); //Assigning role to user
            }
        }

        flash('User, '. $user->name.' created!')->success();
    //Redirect to the users.index view and display message
        return redirect()->route('users.index')->with('tab', 'create_users');
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail(Crypt::decrypt($id)); //Get user with specified id
        $roles = Role::get(); //Get all roles

        return view('users.edit', compact('user', 'roles')); //pass user and roles data to view
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $user = User::findOrFail(Crypt::decrypt($id)); //Get role specified by id
//      when a user updates their profile
        if (isset($request['update_profile'])) {
            //Validate name, email and password fields
            if ($request['password'] !== null){
                $this->validate($request, [
                    'name'=>'required|max:120',
                    'email'=>'required|email|unique:users,email,'.Crypt::decrypt($id),
                    'cellphone_number' => 'required|max:14',
                    'password'=>'required|min:4|confirmed'
                ]);

                $new_cellphone_number = $this->validate_cellphone_number($request->cellphone_number);
                $input = array_merge($request->only(['name', 'email', 'password']), ['cellphone_number' => $new_cellphone_number]); //Retreive the name, email and password fields
            }else{
                $this->validate($request, [
                    'name'=>'required|max:120',
                    'email'=>'required|email|unique:users,email,'.Crypt::decrypt($id),
                    'cellphone_number' => 'required|max:14',
                ]);
                $new_cellphone_number = $this->validate_cellphone_number($request->cellphone_number);
                $input = array_merge($request->only(['name', 'email']), ['cellphone_number' => $new_cellphone_number]); //Retreive the name, email and password fields
            }
            $roles = $request['roles']; //Retreive all roles
            $user->fill($input)->save();

            if (isset($roles)) {
                $user->roles()->sync($roles);  //If one or more role is selected associate user to roles
            }
            else {
                $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
            }
            flash('Your profile has been updated!')->overlay();

            return redirect()->back();
        }else{ // when administrator updates the users profile
            //Validate name, email and password fields
            if ($request['password'] !== null){
                $this->validate($request, [
                    'name'=>'required|max:120',
                    'email'=>'required|email|unique:users,email,'.Crypt::decrypt($id),
                    'cellphone_number' => 'required|max:14',
                    'password'=>'required|min:4|confirmed'
                ]);
                $new_cellphone_number = $this->validate_cellphone_number($request->cellphone_number);
                $input = array_merge($request->only(['name', 'email', 'password']), ['cellphone_number' => $new_cellphone_number]); //Retreive the name, email and password fields
            }else{
                $this->validate($request, [
                    'name'=>'required|max:120',
                    'cellphone_number' => 'required|max:14',
                    'email'=>'required|email|unique:users,email,'.Crypt::decrypt($id)
                ]);
                $new_cellphone_number = $this->validate_cellphone_number($request->cellphone_number);
                $input = array_merge($request->only(['name', 'email']), ['cellphone_number' => $new_cellphone_number]); //Retreive the name, email and password fields
            }

            $roles = $request['roles']; //Retreive all roles
            $user->fill($input)->save();

            if (isset($roles)) {
                $user->roles()->sync($roles);  //If one or more role is selected associate user to roles
            }
            else {
                $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
            }
            flash('User, '. $user->name. ' updated!')->overlay();

            return redirect()->route('users.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find a user with a given id and delete
        $user = User::findOrFail($id);

        $user->delete();

        flash('User,'. $user->name.' deleted!')->overlay();

        return redirect()->route('users.index');
    }

    /**
     * @param $number
     * @return string
     *
     * Overrides the validate_cellphone_number by removing the array
     * parameter and replacing it with the variable parameter
     */
    private function validate_cellphone_number($number)
    {
//        if it starts with a zero remove the zero
        $new_number = $this->strip_zero($number) ;
//        after removing the zero prepend the country code '+264'
        $new_number = $this->prepend_country_code($new_number);
//        if it starts with a 2 add +
        $new_number = $this->prepend_plus($new_number);
//        removed decimal if there is one
        return $new_number;
    }
}
