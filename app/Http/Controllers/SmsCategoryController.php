<?php

namespace App\Http\Controllers;

use App\SmsCategory;
use Validator;
use Illuminate\Http\Request;

class SmsCategoryController extends Controller
{
    /**
     * SmsCategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request['create_category'])){
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()){
                return redirect()
                    ->route('out.index')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('tab', 'view_sms_category');
            }

            $sms_category = SmsCategory::create($request->all());

            flash('Category '. $request['name']. ' successfully created!');
            return redirect()->route('out.index')->with('tab', 'view_sms_category');

        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $sms_category = SmsCategory::findorfail($id);
        return view('sms-categories.edit',compact('sms_category'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $sms_category = SmsCategory::findorfail($id);

        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'description' => 'required'
        ]);
//        if validation fails
        if ($validator->fails()){
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput()
                ->with('');
        }

        $input = $request->all();
//        update the category
        $sms_category->fill($input)->save();

        flash('Category '.$request['name'] .' has been successfully updated!')->overlay();
        return redirect()->route('out.index')->with('tab','view_sms_category');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SmsCategory  $smsCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmsCategory $smsCategory)
    {
        $smsCategory->delete();

        flash($smsCategory->name . ' Deleted!')->success()->overlay();
        return back()->with('tab', 'view_sms_category');
    }
}
