<?php 

namespace App\Http\Controllers;

use App\Group;
use App\Recipient;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class GroupController extends Controller 
{

    /**
     * GroupController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $groups = Group::all()->filter(function ($group){
          return $group->creator == Auth::user() || $group->authorized_users->contains(Auth::id());
      });

//      user should only have access to the recipients they created or have permissions to access
      $recipients = Recipient::all()->filter(function($recipient){
          return $recipient->creator == Auth::user();
      });
//      every user should only have access to the users they created
      $users = User::all()->filter(function ($user) {
          return $user->user_created_by == Auth::user();
      });
      request()->session()->flash('tab', 'view_groups');
      return view('groups.index', compact('groups', 'recipients','users'));
  }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
  public function store(Request $request)
  {
      if (isset($request['assign_recipients_to_group'])){
          $validator = Validator::make($request->all(),[
              'recipients' => 'required',
              'group' => 'required'
          ]);

          if ($validator->fails()){
              return redirect()
                  ->route('out.index')
                  ->withErrors($validator)
                  ->withInput()
                  ->with('tab', 'assign_recipients_to_group');
          }

          $group = Group::findorfail($request['group']);
//          variable to mark the recipients already existing in the group
          $exists = 0;
//          variable to mark the recipients successfully added to the group
          $added = 0;
          foreach ($request['recipients'] as $recipient_id){
              $recipient = Recipient::findorfail($recipient_id);

//              check if duplicate exists already
              if ($group->recipients->contains($recipient_id)){
//                  in case it exists in this group already
                  $exists++;
              }else{
//                  when it is successfully added to the group
                  $group->recipients()->save($recipient);
                  $group->number_of_recipients ++;
                  $added++;
              }
          }
          flash(($exists > 0)? $added .' Recipient (s) have been successfully added to the group, ' . $exists. ' , existed already!': $added .' Recipient (s) have been successfully added to the group');

          return redirect()->route('groups.index')->with('tab', 'assign_recipients_to_groups');

      }
      elseif (isset($request['give_users_access_to_groups'])){
          $validator = Validator::make($request->all(),[
              'user' => 'required',
              'groups' => 'required'
          ]);

//          back end Validation
          if ($validator->fails()){
              return redirect()
                  ->back()
                  ->withErrors($validator)
                  ->withInput()
                  ->with('tab', 'give_users_access_to_groups');
          }

          $user = User::findorfail($request['user']);
          $exists = 0;
          $added = 0;
          foreach ($request['groups'] as $group_id){
              $group = Group::findorfail($group_id);
              if (!$group->authorized_users->contains($user)){
                  $group->authorized_users()->save($user);
                  $added++;
              }else{
                   $exists++;
              }
          }

          if ($added > 0){
             flash( $user->name. ' now has access to the '. $added .' Group (s)')->success()->important();
          }
          if ($exists > 0){
              flash($user->name.' has access to '. $exists.' of the Group (s) already!')->error()->important();
          }
          return redirect()->back()->with('tab', 'give_users_access_to_groups');
      }
      else{ // create group
          //validate the name and description of the group to be created
          $validator = Validator::make($request->all(), [
              'name'=>'required|max:20',
              'description'=>'required|max:512',
          ]);

          if ($validator->fails()){
              return redirect()
                  ->route('groups.index')
                  ->withErrors($validator)
                  ->withInput()
                  ->with('tab', 'create_groups');
          }

          $group = Group::create($request->only('name', 'description')); //Retrieving only the email and password data
//          attach the creator of the group
          Auth::user()->groups()->save($group);

          flash('Group, '. $group->name.' created!')->success();
          //Redirect to the recipients.index view and display message
          if (isset($request['add-group-modal'])){
              return redirect()->route('recipients.index')->with('tab', 'create_bulk_recipients');
          }else{
              return redirect()->route('groups.index')->with('tab', 'create_groups');
          }
      }
  }


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
        $group = Group::findorfail($id);

        $recipients = $group->recipients;
        request()->session()->flash('tab','update_group');
      return view('groups.edit', compact('group', 'recipients'));
  }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
  public function update(Request $request, $id)
  {
      if (isset($request['recipients-in-group'])){

          $validator = Validator::make($request->all(), [
              'recipients' => 'required|min:1'
          ]);

          if ($validator->fails()){
              return redirect()
                  ->back()
                  ->withErrors($validator)
                  ->withInput()
                  ->with('tab', 'remove_recipients_from_group');
          }

          $group = Group::findorfail($id);
          $removed = $group->recipients()->detach($request['recipients']);

          request()->session()->flash('tab','remove_recipients_from_group');

          flash($removed. ' Recipients successfully removed from group '. $group->name)->success();
          return redirect()->back();

      }else{

          $group = Group::findOrFail($id); //Get group specified by id

          //Validate name, and cellphone fields
          $validator = Validator::make($request->all(), [
              'name'=>'required|max:20',
              'description'=>'required:max:512'
          ]);

          if ($validator->fails()){
              return redirect()
                  ->back()
                  ->withErrors($validator)
                  ->withInput()
                  ->with('tab', 'update_group');
          }

          $input = $request->only(['name', 'description']); //Retreive the name, email and password fields
          $group->fill($input)->save();

          flash('Group, '. $group->name. ' updated!')->overlay();

          request()->session()->flash('tab','update_group');

          return redirect()->back()->with('tab', 'update_group');
      }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $group = Group::findOrFail($id);
      $group->delete();

      flash('Group,'. $group->name.' deleted!')->overlay();
      return redirect()->route('groups.index');
  }
//    /**
//     * @param $id
//     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
//     */
//    private function view_recipients_in_group($id)
//    {
//        $group = Group::findorfail($id);
//
//        $recipients = $group->recipients;
//        return view('groups.show', compact('group', 'recipients'));
//    }
}
?>