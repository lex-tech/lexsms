<?php

namespace App\Http\Controllers;

use App\Subscription;
use App\User;
use Illuminate\Http\Request;
use Validator;

class SubscriptionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    /**
     * @return $this
     */
    public function index()
    {
        $users = User::all();
        $subscriptions = Subscription::all();
        $clients = [];
        foreach ($users as $user){
            if ($user->hasRole('Client')) {
                $clients[] = $user;
            }
        }
        request()->session()->flash('tab', 'view_subscriptions');
        return view('subscriptions.index', compact('subscriptions', 'clients'))->with('tab', 'view_subscriptions');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        if (isset($request['subscription-form'])){
            $validator = Validator::make($request->all(), [
                'subscription' => 'required',
                'client' => 'required',
            ]);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput()->with('tab', 'subscribe_users');
            }

            $client = User::findorfail($request['client']);
            $subscription = Subscription::findorfail($request['subscription']);

//            subscribe client
            $subscription->subscribers()->save($client);
//             allocate sms to client
            $client->sms_credit += $subscription->sms_number;
            $client->save();

            flash('Client, '. $client->name.' successfully subscribed!')->success();
            return redirect()->route('subscriptions.index');
        }else{
             $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'duration' => 'required|numeric|min:0',
                'sms_number' => 'required|numeric|min:0'
            ]);

            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput()->with('tab', 'create_subscriptions');
            }

            $subscription = Subscription::create($request->only('name', 'duration', 'sms_number'));

            //Display a successful message upon save
            flash('Subscription, '. $subscription->name.' created!')->success();

            return redirect()->route('subscriptions.index')->with('tab', 'create_subscriptions');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $subscription = Subscription::findOrFail($id); //Get user with specified id

        return view('subscriptions.edit', compact('subscription')); //pass user and data to view
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $subscription = Subscription::findOrFail($id); //Get role specified by id

        //Validate name, email and password fields
        $this->validate($request, [
            'name'=>'required|max:100',
            'duration'=>'required',
            'sms_number'=>'required'
        ]);
        $input = $request->only(['name', 'duration', 'sms_number']); //Retreive the name, email and password fields
//        $roles = $request['roles']; //Retreive all roles
        $subscription->fill($input)->save();

        flash('Subscription, '. $subscription->name. ' updated!')->overlay();

        return redirect()->route('subscriptions.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        //Find a user with a given id and delete
        $subscription = Subscription::findOrFail($id);
        $subscription->delete();

        flash('<div> Subscription, <strong>'.  $subscription->name .'</strong> deleted!</div>')->overlay();

        return redirect()->route('subscriptions.index');
    }
}
?>