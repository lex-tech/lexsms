<?php 

namespace App\Http\Controllers;

use App\System;
use Illuminate\Http\Request;

class SystemController extends Controller 
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'isAdmin']);
    }
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $settings = System::all()->first();

      return view('systems.index', compact('settings'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
      $settings = System::findorfail($id);

      return view('systems.edit', compact('settings'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
      $settings = System::findOrFail($id); //Get role specified by id

      //Validate name, email and password fields
//        'name'=>'required|max:100|unique:subscription,name',
      $this->validate($request, [
          'default_sms_rate'=>'required|min:0|numeric',
          'telephone'=>'required',
          'email'=>'required|email',
          'address'=>'required',
      ]);

      $settings->fill($request->all())->save();

      flash('System settings updated!')->overlay();

      return redirect()->route('systems.index');
  }
}
?>