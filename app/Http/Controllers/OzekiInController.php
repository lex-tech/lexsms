<?php

namespace App\Http\Controllers;

use App\OzekiIn;
use Illuminate\Http\Request;

class OzekiInController extends Controller
{
    /**
     * OzekiInController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OzekiIn  $ozekiIn
     * @return \Illuminate\Http\Response
     */
    public function show(OzekiIn $ozekiIn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OzekiIn  $ozekiIn
     * @return \Illuminate\Http\Response
     */
    public function edit(OzekiIn $ozekiIn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OzekiIn  $ozekiIn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OzekiIn $ozekiIn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OzekiIn  $ozekiIn
     * @return \Illuminate\Http\Response
     */
    public function destroy(OzekiIn $ozekiIn)
    {
        //
    }
}
