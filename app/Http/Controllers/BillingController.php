<?php

namespace App\Http\Controllers;

use App\Billing;
use App\Credit;
use App\System;
use Carbon\Carbon;
use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BillingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_bills  = Billing::all();
        $bills = [];
        $user = Auth::user();
        $months = null;
//       render the clients bills only
        if (Auth::user()->hasRole('Client')){
            foreach ($all_bills as $bill){

                if ($bill->client->id == Auth::id()){
                    $bills[] = $bill;
                }
            }

            $months = Billing::all()->filter(function ($month){
                return $month->client->id == Auth::id() && $month->payment_status == 0;
            });
        }
//        render all the bills
        if (Auth::user()->hasRole('Admin')){
            $bills = $all_bills;
        }
        $client_contact_info = $user->contacts;
//        request()->session()->flash('tab', 'view_billings');
        return view('billing.index', compact('bills', 'client_contact_info', 'user', 'months'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bill = Billing::findorfail($id);
        $bill_owner = $bill->client;
//        check if user has updated their contact details
        if ($bill_owner->contacts == null){

            if (Auth::user()->hasRole('Admin')){
                flash('This user needs to update their contact details!')->warning()->important();

                return redirect()->back()->with('tab', 'view_billings');
            }

            flash('Please Update Your Contact Details To View Your Bill!')->info()->important();

            return redirect()->back()->with('tab', 'update_contact_info');
        }
        $transactions = $bill->credit_transactions;
        $config = System::all()->first();
//        $invoice =
            Invoice::make()
            ->add_bulk_items($transactions)
            ->number($bill->id)
            ->tax(15)

            ->logo('images/logo/logo.png')
            ->business([
                'name' => 'Lex Technologies (PTY) Ltd',
//                'id' => $config->id ,
                'phone' => $config->telephone,
                'email' => $config->email,
                'address' => $config->address,
                'location' => $config->address,
                'zip',
                'city' => 'Windhoek',
                'country' => 'Namibia'
            ])
            ->decimals(2)
            ->notes('Lrem ipsum dolor sit amet, consectetur adipiscing elit.')
            ->customer([
                'name'      => $bill_owner->name,
                'id'        => $bill_owner->id,
                'phone'     => $bill_owner->contacts->telephone_number,
                'location'  => $bill_owner->contacts->address,
                'zip'       => '9000',
                'city'      => 'Windhoek',
                'country'   => 'Namibia',
            ])
            ->show('Demo_Invoice');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function edit(Billing $billing)
    {
//        $billing = Billing::findOrFail($billing->id);

        return view('billing.edit', compact('billing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Billing  $billing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bill = Billing::findOrFail($id); //Get bill specified by id

        //Validate name, and cellphone fields
        $this->validate($request, [
            'payment_status',
//            'cellphone_number' => 'required:max:25'
        ]);
//        dd(var_dump($request->all()));
        $input = array_merge($request->only(['payment_status']), ['payment_date' => Carbon::now()]); //Retreive the payment status field

//        dd(var_dump($input));

        $bill->fill($input)->save();

        flash('Bill for ' . $bill->client->name . ' updated!')->overlay();

        return redirect()->route('billing.index');
    }

}
