<?php

namespace App\Http\Controllers;

use App\Billing;
use App\DailyMessages;
use App\MonthlyMessages;
use App\User;
use App\YearlyMessages;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller
{
    /**
     * ReportsController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->hasRole('Admin')){
            $yearlyMessages = YearlyMessages::all();
            // Admin can see all the bills
            $bills = Billing::all();
        }

        if (Auth::user()->hasRole('Client')){
            $yearlyMessages = YearlyMessages::all()->filter(function (YearlyMessages $message){
                return $message->sender->id == Auth::id() || $message->sender->user_created_by->id ==  Auth::id();
            });

            // clients should only see their bills
            $bills = Billing::all()->filter(function (Billing $bill){
                return  $bill->client->id == Auth::id();
            });
        }

        $summary = new Collection();
        $summary->add(['name' => 'day']);
        $summary->add(['name' => 'month']);
        $summary->add(['name' => 'year']);

        $fa_summary = new Collection();
        $fa_summary->add(['name' => 'month']);
        $fa_summary->add(['name' => 'year']);

        $year = true;
        $day = false;
        $month = false;
        // financial report toggling variables
        $fa_year = false;
        $fa_month = true;

        request()->session()->flash('tab', 'sms_reports');
        return view('reports.index', compact('yearlyMessages', 'year', 'day', 'month', 'summary', 'bills', 'fa_year', 'fa_month', 'fa_summary'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $summary = new Collection();
        $bills = Billing::all();
        $summary->add(['name' => 'day']);
        $summary->add(['name' => 'month']);
        $summary->add(['name' => 'year']);

        $fa_summary = new Collection();
        $fa_summary->add(['name' => 'month']);
        $fa_summary->add(['name' => 'year']);

        $year = false;
        $day = false;
        $month = false;

        // financial report toggling variables
        $fa_year = false;
        if ($request['summary'] == 'day'){
//            dd('day');
            $day = true;

            if (Auth::user()->hasRole('Admin')){
                $dailyMessages = DailyMessages::all();
            }

            if (Auth::user()->hasRole('Client')){
                $dailyMessages = DailyMessages::all()->filter(function (DailyMessages $message){
                   return $message->sender->id == Auth::id() || $message->sender->created_by == Auth::id();
                });
            }
            request()->session()->flash('tab', 'sms_reports');
            return view('reports.index', compact('dailyMessages', 'year', 'day', 'month', 'summary', 'bills', 'fa_summary'));
        }
        if ($request['summary'] == 'month'){
//            dd('month');
            $month = true;
            if (Auth::user()->hasRole('Admin')){
                $monthlyMessages = MonthlyMessages::all();
            }

            if (Auth::user()->hasRole('Client')){
                $monthlyMessages = MonthlyMessages::all()->filter(function (MonthlyMessages $message){
                    return $message->sender->id == Auth::id() || $message->sender->created_by == Auth::id();
                });
            }
            request()->session()->flash('tab', 'sms_reports');
            return view('reports.index', compact('monthlyMessages', 'year', 'day', 'month', 'summary', 'bills', 'fa_summary'));
        }
        if ($request['summary'] == 'year'){
            $year = true;
            if (Auth::user()->hasRole('Admin')){
                $yearlyMessages = YearlyMessages::all();
            }

            if (Auth::user()->hasRole('Client')){
                $yearlyMessages = YearlyMessages::all()->filter(function (YearlyMessages $message){
                    return $message->sender->id == Auth::id() || $message->sender->created_by == Auth::id();
                });
            }
            request()->session()->flash('tab', 'sms_reports');
            return view('reports.index', compact('yearlyMessages', 'year', 'day', 'month', 'summary', 'bills', 'fa_summary'));
        }
        if ($request['expand-year']){

            $monthlyMessages = User::find($request['expand-year'])->monthly_sent_messages;
            $month = true;
            request()->session()->flash('tab', 'sms_reports');
            return view('reports.index', compact('monthlyMessages', 'year', 'day', 'month', 'summary', 'bills', 'fa_summary'));
        }
        if ($request['expand-month']){

            $dailyMessages = MonthlyMessages::find($request['month'])->days;
//            dd($dailyMessages);
            $day = true;
            request()->session()->flash('tab', 'sms_reports');
            return view('reports.index', compact('dailyMessages', 'year', 'day', 'month', 'summary', 'bills', 'fa_summary'));
        }
        if (isset($request['sms_report_filter'])){
            $from = $request['start_date'];
            $to = $request['end_date'];
            $dailyMessages = null;
            $day = true;
            if (Auth::user()->hasRole('Admin')){
                $dailyMessages = DailyMessages::where('date', '>=', $from)
                    ->where('date', '<=', $to)
                    ->get();
            }

            if (Auth::user()->hasRole('Client')) {
                // needs to be adjusted
                $dailyMessageTemp = DailyMessages::where('date', '>=', $from)
                    ->where('date', '<=', $to)
                    ->get();
                $dailyMessages = $dailyMessageTemp->filter(function ($message){
//                    dd($message);
                    return $message->sender->id == Auth::id() || $message->sender->created_by == Auth::id();
                });
            }
            request()->session()->flash('tab', 'sms_reports');
            return view('reports.index', compact('dailyMessages', 'year', 'day', 'month', 'summary', 'bills', 'fa_summary'));
        }
        // financial reports
        if ($request['fa_summary'] == 'month'){
//            dd($request->all());
            $yearlyMessages = null;
            $year = true;
            $fa_month = true;
            if (Auth::user()->hasRole('Admin')){
                $bills = Billing::all();

                $yearlyMessages = YearlyMessages::all();
            }

            if (Auth::user()->hasRole('Client')){
                $bills = Billing::all()->filter(function (Billing $bill){
                    return $bill->client->id == Auth::id();
                });

                $yearlyMessages = YearlyMessages::all()->filter(function (YearlyMessages $message){
                    return $message->sender->id == Auth::id() || $message->sender->created_by == Auth::id();
                });
            }
            request()->session()->flash('tab', 'financial_reports');
            return view('reports.index', compact('year', 'yearlyMessages', 'day', 'month', 'summary', 'fa_summary', 'bills', 'fa_year', 'fa_month'));
        }
        if ($request['fa_summary'] == 'year'){
            $yearlyMessages = null;
            $year = true;
            $fa_year = true;
            if (Auth::user()->hasRole('Admin')){
                $bills = Billing::all();

                $yearlyMessages = YearlyMessages::all();
            }

            if (Auth::user()->hasRole('Client')){
                $bills = Billing::all()->filter(function (Billing $bill){
                    return $bill->client->id == Auth::id();
                });

                $yearlyMessages = YearlyMessages::all()->filter(function (YearlyMessages $message){
                    return $message->sender->id == Auth::id() || $message->sender->created_by == Auth::id();
                });
            }
            request()->session()->flash('tab', 'financial_reports');
            return view('reports.index', compact('year', 'yearlyMessages', 'day', 'month', 'summary', 'fa_summary', 'bills', 'fa_year', 'fa_month'));
        }
        if (isset($request['financial_report_filter'])){
            $yearlyMessages = null;
            $year = true;
            $fa_month = true;
            $from = $request['start_date'];
            $to = $request['end_date'];
            if (Auth::user()->hasRole('Admin')){
                $bills = Billing::where('subscription_month', '>=', $from)
                    ->where('subscription_month', '<=', $to)
                    ->get();

                $yearlyMessages = YearlyMessages::all();
            }

            if (Auth::user()->hasRole('Client')) {
                // filter bills
                $billTemp = Billing::where('subscription_month', '>=', $from)
                    ->where('subscription_month', '<=', $to)
                    ->get();
                $bills = $billTemp->filter(function ($bill){
                    // client needs to see only their bills
                    return $bill->client->id == Auth::id();
                });

                $yearlyMessages = YearlyMessages::all()->filter(function (YearlyMessages $message){
                    return $message->sender->id == Auth::id() || $message->sender->created_by == Auth::id();
                });

            }
            request()->session()->flash('tab', 'financial_reports');
            return view('reports.index', compact('yearlyMessages', 'year', 'day', 'month', 'summary', 'bills', 'fa_month', 'fa_summary'));
        }
        else {
            return redirect()->back();
        }
    }

}
