<?php

namespace App\Http\Controllers;

use App\Group;
use App\Recipient;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Illuminate\Http\Request;
use App\Traits\FixCellNumbers;

class RecipientsController extends Controller
{
    use FixCellNumbers;
    /**
     * RecipientsController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $recipients = Recipient::all()->filter(function($recipient){
            return $recipient->creator == Auth::user();
        });
        $groups = Group::all()->filter(function ($group){
            return $group->creator == Auth::user();
        });
        return view('recipients.contacts.index', compact('recipients', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (isset($request['contacts_upload_form'])) {
//            dd(dump($request['group']));
            $validator = Validator::make($request->all(), [
                'csv_file' => 'mimes:xlsx,csv|required'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput()->with('tab', 'create_bulk_recipients');
            }
            if ($request->hasFile('csv_file')) {
//              return the number of failed rows and the rows that passed
                $success = $this->importFile($request);
            }
//            in case the number column does not exist
            if ($success == false) {
                flash('Make sure that your contacts column heading has the name "number", if you are using an Excel file make sure that it contains one worksheet only!')->error()->important();
                return redirect()->back()->with('tab', 'create_bulk_recipients');
            }
//            in case the file has data and the required columns
            if (!$success == null) {
                flash($success['created'] . ' Recipients created Successfully, ' . $success['duplicates'] . ' recipient(s) not created as they exist already!')->success();
                if ($success['number_null'] > 0){
                    flash('Please fill in the number for '. $success['number_null'] . ' of the rows in your file and try again to add them!')->error()->important();
                }
            }
//            in case the files is empty
            else
                flash('Your excel file is empty, please fill it with data!')->error();

            return redirect()->back()->with('tab', 'create_bulk_recipients');

        } else {
//            add single recipient
            //Validate name, email and password fields
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:100',
                'cellphone_number' => 'required|unique:recipients',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput()->with('tab', 'create_recipients');
            }

            $new_number = $this->strip_zero($request->cellphone_number);
            $new_number = $this->prepend_country_code($new_number);
            $contact = Recipient::create(['name' => $request['name'], 'cellphone_number' => $new_number]); //Retrieving only the email and password data
//            attach the creator of the recipient
            Auth::user()->recipients()->save($contact);

            flash('Recipient, ' . $contact->name . ' created!')->success();
            //Redirect to the recipients.index view and display message
            return redirect()->route('recipients.index')->with('tab', 'create_recipients');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $recipient = Recipient::findOrFail($id); //Get recipient with specified id

        return view('recipients.contacts.edit', compact('recipient')); //pass recipient data to the view
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $contact = Recipient::findOrFail($id); //Get role specified by id

        //Validate name, and cellphone fields
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'cellphone_number' => 'required:max:25'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request->only(['name', 'cellphone_number']); //Retrieve the name and cellphone_number fields
        $contact->fill($input)->save();

        request()->session()->now('tab', 'view_recipients');
        flash('Contact, ' . $contact->name . ' updated!')->overlay();

        return redirect()->route('recipients.index');

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $contact = Recipient::findOrFail($id);
        $contact->delete();

        flash('Contact,' . $contact->name . ' deleted!')->overlay();

        return redirect()->route('recipients.index');

    }

    /**
     * @param Request $request
     * @return array|bool|null
     */
    public function importFile(Request $request)
    {
        $duplicates = 0;
        $created = 0;
        $number_null = 0;
        $path = $request->file('csv_file')->getRealPath();
        $recipientsImported = [];
//        find the group selected
        if (isset($request['group'])) {
            $group = Group::findorfail($request['group']);
        }
//        load the excel file
        $data = Excel::load($path, function ($reader) {
        })->get();

        if (!empty($data) && $data->count()) {
//            dd(dump($data));
//          when the column number does not exist
            if (!isset($data[0]['number'])) {
                return false;
            }
            $data = $data->toArray();
//            loop through the rows
            foreach ($data as $row) {
//                if there is a header name add the name to the database as well

                if (isset($row['name'])) {
//                    make sure that the number column is not null
                    if  (isset($row['number'])){
                        //                  validate cellphone number
                        $new_number = $this->validate_cellphone_number($row);
//                        $pattern = "/d(?<=+264)\d{9}/";
//                        if (preg_match($pattern, $new_number)){
                            if (Recipient::where('cellphone_number', $new_number)->exists() == false) {
//                        add all the valid recipients that are not in the database to this array
                                $recipient_added = Recipient::create(['name' => $row['name'], 'cellphone_number' => $new_number, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
//                      attach the created by relationship on the recipient
                                Auth::user()->recipients()->save($recipient_added);
//                        check if group value is set
                                if (isset($group)) {
                                    $group->recipients()->save($recipient_added);
                                }
                                $created++;
                            } else {
//                        count the number of duplicates
                                $duplicates++;
                            }
//                        }else{
//                            dd('testing');
//                        }
                    }else{
//                        in case the number is empty
                        $number_null++;
                    }
                } else {
//                  validate cellphone number
                    $new_number = $this->validate_cellphone_number($row);
//                    if there is no header name add the numbers to the database only
//                    $pattern = "/d(?<=+264)\d{9}/";
//                    if (preg_match($pattern, $new_number)){
                        if (Recipient::where('cellphone_number', $new_number)->exists() == false) {
//                        add all the valid recipients that are not in the database to this array
                            $recipientsImported[] = ['name' => '', 'cellphone_number' => $new_number, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
                            $recipient_added = Recipient::create(['name' => '', 'cellphone_number' => $new_number, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
//                         attach the created by relationship on the recipient
                            Auth::user()->recipients()->save($recipient_added);
//                            check if group has a value
                            if (isset($group)) {
                                $group->recipients()->save($recipient_added);
                            }
                            $created++;
                        } else {
                            $duplicates++;
                        }
//                    }else{
//                        dd('testing');
//                    }
                }
            }

            return ['duplicates' => $duplicates, 'created' => $created, 'number_null' => $number_null];
        }
        return null;
    }
}
