<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditRequest extends Model
{
    public $fillable = ['credit','requested_by','noted'];
//    who made the request
    public function request_made_by(){
        return $this->belongsTo(User::class, 'requested_by');
    }
}
