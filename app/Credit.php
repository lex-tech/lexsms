<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Client;

class Credit extends Model
{
    public $fillable = ['transaction_made_by', 'credit', 'client_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * the user who made the transaction
     */
    public function transactor(){
        return $this->belongsTo(User::class, 'transaction_made_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * the client the sms credit was sent to
     */
    public function client(){
        return $this->belongsTo(User::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * the bill to which this transaction will be added
     */
    public function bill(){
        return $this->belongsTo(Billing::class, 'bill_id');
    }
}


