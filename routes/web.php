<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::middleware('auth')->get('/home', 'HomeController@index');

Route::get('/', 'HomeController@index')->name('home');

Route::resource('users', 'UserController', ['except' => 'show']);

Route::resource('roles', 'RoleController', ['except' => 'show']);

Route::resource('permissions', 'PermissionController', ['except' => 'show']);

// not needed
//Route::resource('posts', 'PostController');

// business routes
//Route::resource('subscriptions/subscribe', 'SubscriptionController@subscribe');
Route::resource('subscriptions', 'SubscriptionController', ['except' => ['create', 'show']]);

Route::resource('systems', 'SystemController', ['only' => ['index', 'edit', 'update']]);

Route::resource('recipients', 'RecipientsController', ['except' => ['create', 'show']]);

Route::resource('groups', 'GroupController', ['except' => ['create', 'show']]);

//Route::resource('in', 'OzekiInController');

Route::resource('out', 'OzekiOutController', ['only' => ['index', 'store']]);

Route::resource('credits', 'CreditController', ['only' => ['index', 'create', 'store', 'update']]);

Route::resource('sms-categories', 'SmsCategoryController',  ['only' => ['destroy' ,'edit', 'update', 'store']]);

//Route::resource('memberships', 'MembershipController');

Route::resource('billing', 'BillingController',  ['only' => ['index', 'show', 'edit', 'update']]);

Route::resource('profile', 'CustomerInfoController',  ['only' => ['edit', 'update']]);

Route::resource('reports', 'ReportsController',  ['only' => ['index', 'update', 'store']]);

Route::resource('invoices', 'InvoiceController',  ['only' => ['show', 'store']]);