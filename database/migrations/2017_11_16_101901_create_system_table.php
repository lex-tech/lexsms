<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemTable extends Migration {

	public function up()
	{
		Schema::create('system', function(Blueprint $table) {
			$table->increments('id');
            $table->float('default_sms_rate',8,2)->default(1.00);
			$table->string('telephone', 20)->nullable();
			$table->string('email', 80);
			$table->string('address', 100);
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('system');
	}
}