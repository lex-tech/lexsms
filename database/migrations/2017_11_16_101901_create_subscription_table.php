<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionTable extends Migration {

	public function up()
	{
		Schema::create('subscription', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name', 20);
			$table->integer('duration')->unsigned();
			$table->integer('sms_number')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('subscription');
	}
}