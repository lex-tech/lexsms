<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeklyMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekly_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('week');
            $table->integer('client')->unsigned()->nullable();
            $table->integer('week_belongs_to_a_month')->unsigned()->nullable();
            $table->integer('year')->unsigned()->nullable();
            $table->integer('total_messages_for_the_week')->default(0);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weekly_messages');
    }
}
