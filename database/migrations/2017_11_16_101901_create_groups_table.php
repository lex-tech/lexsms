<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable extends Migration {

	public function up()
	{
		Schema::create('groups', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
            $table->string('created_by')->nullable();
			$table->string('name', 20);
			$table->string('description', 512)->nullable();
			$table->integer('number_of_recipients')->unsigned()->default('0');
		});
	}

	public function down()
	{
		Schema::drop('groups');
	}
}