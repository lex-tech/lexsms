<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->integer('week')->unsigned()->nullable();
            $table->integer('month')->unsigned()->nullable();
            $table->integer('year')->unsigned()->nullable();
            $table->integer('client')->unsigned()->nullable();
            $table->integer('total_messages_for_the_day')->default(0);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_messages');
    }
}
