<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditReallocationTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_reallocation_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_made_by')->nullable();
            $table->integer('credit_sent_from')->nullable();
            $table->integer('credit_sent_to')->nullable();
            $table->integer('credit')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_reallocation_transactions');
    }
}
