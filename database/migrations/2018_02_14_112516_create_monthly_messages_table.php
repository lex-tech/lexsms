<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('month');
            $table->integer('client')->unsigned()->nullable();
            $table->integer('year')->unsigned()->nullable();
            $table->integer('total_messages_for_the_month')->default(0);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_messages');
    }
}
