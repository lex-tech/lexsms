<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecipientsTable extends Migration {

	public function up()
	{
		Schema::create('recipients', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('created_by')->nullable();
            $table->string('name', 50)->nullable();
            $table->string('cellphone_number', 50)->unique();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('recipients');
	}
}