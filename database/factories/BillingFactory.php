<?php

use Faker\Generator as Faker;

$factory->define(App\Billing::class, function (Faker $faker) {
    return [
        'owner' => 2,
        'amount' => $faker->numberBetween(100.78,20000.69),
        'subscription_month' => $faker->date('m/Y'),
        'payment_status' => $faker->boolean(),
        'payment_date' => $faker->dateTime()
    ];
});
