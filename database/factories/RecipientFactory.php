<?php

use Faker\Generator as Faker;
use App\Recipient;

$factory->define(Recipient::class, function (Faker $faker) {
    return [
        'name' => $faker->name(['Male']),
        'cellphone_number' => $faker->phoneNumber
    ];
});
