<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        // 'email' => $faker->unique()->safeEmail,
        'email' => $faker->email,
        'password' => $password ?: $password = 'secret',
        'created_by' => null,
        'remember_token' => str_random(10),
    ];
});

// $factory->define(\Spatie\Permission\Models\Permission::class, function (Faker $faker) {
//     static $password;

//     return [
//         'name' => 'Administer roles & permissions',
//         'guard_name' => 'web',
//     ];
// });