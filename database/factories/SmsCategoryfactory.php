<?php

use Faker\Generator as Faker;
use App\SmsCategory;
$factory->define(SmsCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->sentence(4)
    ];
});
