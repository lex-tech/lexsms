<?php

use Illuminate\Database\Seeder;
use App\Post;
use App\User;
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        // clear previous permission
//         Permission::truncate();
        $this->call(PermissionTableSeeder::class);

        // clear previous roles
//         Role::truncate();
        $this->call(RoleTableSeeder::class);

        // clear previous users
//         User::truncate();
        $this->call(UserTableSeeder::class);
       
        // clear previous posts
//         Post::truncate();
//        $this->call(PostTableSeeder::class);

//        create groups
//        $this->call(GroupsTableSeeder::class);

//        create Recipients
//        $this->call(RecipientsTableSeeder::class);

//        create sms categories
        $this->call(MessageCategorySeeder::class);

//        create subscriptions
        $this->call(SubscriptionsTableSeeder::class);

//      create memberships
        $this->call(MembershipTableSeeder::class);

//      create settings
        $this->call(SettingsTableSeeder::class);

//      create bills
//        $this->call(BillsTableSeeder::class);
    }
}
