<?php

use Illuminate\Database\Seeder;
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Admin role and permissions
         */

        require_once 'role_permissions/adminRole.php';


        /**
         * Client role and permissions
         */
        require_once 'role_permissions/clientRole.php';


        /**
         * Sub-client role and permissions
         */
        require_once 'role_permissions/sub-clientRole.php';

        /**
         * Demo-role and permissions
         */
        require_once 'role_permissions/demoRole.php';
    }
}
