<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 11:51 AM
 */
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

$sub_client_role = new Role();
$sub_client_role->name = 'Sub-Client';
$sub_client_role->guard_name = 'web';
$sub_client_role->save();
$sub_client_role->givePermissionTo(Permission::where('name', 'Create Posts')->first());

// groups
$sub_client_role->givePermissionTo(Permission::where('name', 'Manage Groups')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Create Groups')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Delete Groups')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'View Groups')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Update Groups')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Assign Recipients To Groups')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Remove Recipients From Groups')->first());
// sms service
$sub_client_role->givePermissionTo(Permission::where('name', 'Manage SMS Services')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Create SMS')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Delete SMS')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'View SMS')->first());

// recipients
$sub_client_role->givePermissionTo(Permission::where('name', 'Manage Recipients')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Create Recipients')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Delete Recipients')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'View Recipients')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Update Recipients')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Create Bulk Recipients')->first());

// credit request
$sub_client_role->givePermissionTo(Permission::where('name', 'Manage Credit')->first());
$sub_client_role->givePermissionTo(Permission::where('name', 'Request Credit')->first());