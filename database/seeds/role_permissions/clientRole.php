<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 11:51 AM
 */
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

$client_role = new Role();
$client_role->name = 'Client';
$client_role->guard_name = 'web';
$client_role->save();

//$client_role->givePermissionTo(Permission::where('name', 'Create Posts')->first());

// sub-users
$client_role->givePermissionTo(Permission::where('name', 'Manage Users')->first());
$client_role->givePermissionTo(Permission::where('name', 'Create Users')->first());
$client_role->givePermissionTo(Permission::where('name', 'Delete Users')->first());
$client_role->givePermissionTo(Permission::where('name', 'View Users')->first());
$client_role->givePermissionTo(Permission::where('name', 'Update Users')->first());

// groups
$client_role->givePermissionTo(Permission::where('name', 'Manage Groups')->first());
$client_role->givePermissionTo(Permission::where('name', 'Create Groups')->first());
$client_role->givePermissionTo(Permission::where('name', 'Delete Groups')->first());
$client_role->givePermissionTo(Permission::where('name', 'View Groups')->first());
$client_role->givePermissionTo(Permission::where('name', 'Update Groups')->first());
$client_role->givePermissionTo(Permission::where('name', 'Assign Recipients To Groups')->first());
$client_role->givePermissionTo(Permission::where('name', 'Remove Recipients From Groups')->first());

// sms service
$client_role->givePermissionTo(Permission::where('name', 'Manage SMS Services')->first());
$client_role->givePermissionTo(Permission::where('name', 'Create SMS')->first());
$client_role->givePermissionTo(Permission::where('name', 'Delete SMS')->first());
$client_role->givePermissionTo(Permission::where('name', 'View SMS')->first());

// recipients
$client_role->givePermissionTo(Permission::where('name', 'Manage Recipients')->first());
$client_role->givePermissionTo(Permission::where('name', 'Create Recipients')->first());
$client_role->givePermissionTo(Permission::where('name', 'Delete Recipients')->first());
$client_role->givePermissionTo(Permission::where('name', 'View Recipients')->first());
$client_role->givePermissionTo(Permission::where('name', 'Update Recipients')->first());
$client_role->givePermissionTo(Permission::where('name', 'Create Bulk Recipients')->first());

// credit
$client_role->givePermissionTo(Permission::where('name', 'Manage Credit')->first());
$client_role->givePermissionTo(Permission::where('name', 'Create Credit')->first());
$client_role->givePermissionTo(Permission::where('name', 'Request Credit')->first());
$client_role->givePermissionTo(Permission::where('name', 'Reallocate Credit')->first());
$client_role->givePermissionTo(Permission::where('name', 'View Credit Reallocation Transactions')->first());
$client_role->givePermissionTo(Permission::where('name', 'View Credit Requests')->first());

//billing
$client_role->givePermissionTo(Permission::where('name', 'View Billings')->first());

// membership
//$client_role->givePermissionTo(Permission::where('name', 'View Memberships')->first());
//$client_role->givePermissionTo(Permission::where('name', 'Create Memberships')->first());
//$client_role->givePermissionTo(Permission::where('name', 'Delete Memberships')->first());
//$client_role->givePermissionTo(Permission::where('name', 'Manage Memberships')->first());
//$client_role->givePermissionTo(Permission::where('name', 'Messaging')->first());

//contact details
$client_role->givePermissionTo(Permission::where('name', 'Update Contact Details')->first());

//group accessability
$client_role->givePermissionTo(Permission::where('name', 'Give Users Access To Groups')->first());

//reports
$admin_role->givePermissionTo(Permission::where('name', 'View Reports')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View SMS Reports')->first());