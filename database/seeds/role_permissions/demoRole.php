<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 12:09 PM
 */

use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

$demo_role = new Role();
$demo_role->name = 'Demo';
$demo_role->guard_name = 'web';
$demo_role->save();
//$admin_role->givePermissionTo(Permission::where('name', 'Create Users')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Create Posts')->first());

// sub-users
$demo_role->givePermissionTo(Permission::where('name', 'Manage Users')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Create Users')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Delete Users')->first());
$demo_role->givePermissionTo(Permission::where('name', 'View Users')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Update Users')->first());

// groups
$demo_role->givePermissionTo(Permission::where('name', 'Manage Groups')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Create Groups')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Delete Groups')->first());
$demo_role->givePermissionTo(Permission::where('name', 'View Groups')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Update Groups')->first());

// sms service
$demo_role->givePermissionTo(Permission::where('name', 'Manage SMS Services')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Create SMS')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Delete SMS')->first());
$demo_role->givePermissionTo(Permission::where('name', 'View SMS')->first());

// recipients
$demo_role->givePermissionTo(Permission::where('name', 'Manage Recipients')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Create Recipients')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Delete Recipients')->first());
$demo_role->givePermissionTo(Permission::where('name', 'View Recipients')->first());
$demo_role->givePermissionTo(Permission::where('name', 'Update Recipients')->first());
