<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 11:51 AM
 */
use \Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;

$admin_role = new Role();
$admin_role->name = 'Admin';
$admin_role->guard_name = 'web';
$admin_role->save();
$admin_role->givePermissionTo(Permission::where('name', 'Administer roles & permissions')->first());

// users
$admin_role->givePermissionTo(Permission::where('name', 'Manage Users')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Users')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Delete Users')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Users')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Update Users')->first());

// posts
$admin_role->givePermissionTo(Permission::where('name', 'Manage Posts')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Posts')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Delete Posts')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Posts')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Update Posts')->first());

// groups
$admin_role->givePermissionTo(Permission::where('name', 'Manage Groups')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Groups')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Delete Groups')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Groups')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Update Groups')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Remove Recipients From Groups')->first());

// permissions
$admin_role->givePermissionTo(Permission::where('name', 'Manage Permissions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Permissions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Delete Permissions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Permissions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Update Permissions')->first());

// recipients
$admin_role->givePermissionTo(Permission::where('name', 'Manage Recipients')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Recipients')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Delete Recipients')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Recipients')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Update Recipients')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Bulk Recipients')->first());

// roles
$admin_role->givePermissionTo(Permission::where('name', 'Manage Roles')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Roles')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Delete Roles')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Roles')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Update Roles')->first());

// subscriptions
$admin_role->givePermissionTo(Permission::where('name', 'Manage Subscriptions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Subscriptions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Delete Subscriptions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Subscriptions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Update Subscriptions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Subscribe Users')->first());

//sms service
$admin_role->givePermissionTo(Permission::where('name', 'Manage SMS Services')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Create SMS')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Delete SMS')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View SMS')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View SMS Category')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Update Subscriptions')->first());

// Credits
$admin_role->givePermissionTo(Permission::where('name', 'Manage Credit')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Create Credit')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Reallocate Credit')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Credit Transactions')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'View Credit Reallocation Transactions')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Assign Recipients To Groups')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Credit Requests')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Delete Credit')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Update Credit')->first());

//// memberships
//$admin_role->givePermissionTo(Permission::where('name', 'Manage Memberships')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Create Memberships')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Delete Memberships')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'View Memberships')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Update Memberships')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'Create Client Memberships')->first());
//$admin_role->givePermissionTo(Permission::where('name', 'View Client Memberships')->first());

//settings
$admin_role->givePermissionTo(Permission::where('name', 'Edit System Settings')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View System Settings')->first());

//billing
$admin_role->givePermissionTo(Permission::where('name', 'View Billings')->first());
$admin_role->givePermissionTo(Permission::where('name', 'Update Billings')->first());

//contact details
//$admin_role->givePermissionTo(Permission::where('name', 'Update Contact Details')->first());

//group accessability
$admin_role->givePermissionTo(Permission::where('name', 'Give Users Access To Groups')->first());

//reports accessability
$admin_role->givePermissionTo(Permission::where('name', 'View Reports')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View SMS Reports')->first());
$admin_role->givePermissionTo(Permission::where('name', 'View Financial Reports')->first());
