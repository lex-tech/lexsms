<?php

use Illuminate\Database\Seeder;
use App\Recipient;
class RecipientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        factory(Recipient::class, 50)->create()->each(function (Recipient $recipient){
            $boolean = random_int(0, 1);
            $creator = \App\User::findorfail(2);
            $ids = range(1,10);
//                    dd(dump($creator));
            shuffle($ids);

            if ($boolean){
                $sliced = array_slice($ids, 0, 2);

                $recipient->groups()->attach($sliced);
//                attach creator
                $creator->recipients()->save($recipient);
            } else{

                $sliced = array_slice($ids, 0, 1);
                $recipient->groups()->attach($sliced);
//                attach creator
                $creator->recipients()->save($recipient);

            }
        });

//        //        select all the groups
//        $all_groups = \App\Group::all();
//
//        foreach ($all_groups as$group){
////            dd(dump($all_groups->count()));
//            $group->number_of_recipients += $group->recipients()->count();
//        }

    }
}
