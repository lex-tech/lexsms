<?php

use Illuminate\Database\Seeder;
use \Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(Permission::class,1)->create();
        /**
         * Admin roles and permissions
         */
        $admin_permission = new Permission();
        $admin_permission->name = 'Administer roles & permissions';
        $admin_permission->guard_name = 'web';
        $admin_permission->save();
        // $admin_permission->givePermissionTo(Role::where('name', 'Admin'));

        /**
         * user permissions
         */

        require_once 'model_permissions/userModel.php';

        /**
         * post permissions
         */
        require_once 'model_permissions/postModel.php';

        /**
         * permission permissions
         */
        require_once 'model_permissions/permissionModel.php';

        /**
         * subscription role and permissions
         */

        require_once 'model_permissions/subscriptionModel.php';

        /**
         * group model permission
         */

        require_once 'model_permissions/groupModel.php';

        /**
         * recipient model permissions
         */

        require_once 'model_permissions/recipientModel.php';

        /**
         * role model permission
         */

        require_once 'model_permissions/roleModel.php';

        /**
         * SMS model permission
         */

        require_once 'model_permissions/smsModels.php';

        /**
         * Credit model permissions
         */

        require_once 'model_permissions/creditModel.php';

        /**
         * Membership model permissions
         */

        require_once 'model_permissions/membershipModel.php';

        /**
         * Settings model permissions
         */

        require_once 'model_permissions/settingsModel.php';

        /**
         * billings model permissions
         */

        require_once 'model_permissions/billingsModel.php';

        /**
         * reports model permission
         */

        require_once 'model_permissions/reportsModel.php';

    }
}
