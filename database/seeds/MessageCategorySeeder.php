<?php

use Illuminate\Database\Seeder;
use App\SmsCategory;
class MessageCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(SmsCategory::class, 10)->create();

        $category_1 = new SmsCategory();
        $category_1->name = 'Advert';
        $category_1->description = 'SMS Adverts';
        $category_1->save();

        $category_2 = new SmsCategory();
        $category_2->name = 'Anouncement';
        $category_2->description = 'General Announcement';
        $category_2->save();

        $category_3 = new SmsCategory();
        $category_3->name = 'Meeting Invitation';
        $category_3->description = 'Meeting Invitation';
        $category_3->save();

        $category_4 = new SmsCategory();
        $category_4->name = 'Reminder';
        $category_4->description = 'Event Reminder';
        $category_4->save();

        $category_5 = new SmsCategory();
        $category_5->name = 'Promotion';
        $category_5->description = 'Product Promotion';
        $category_5->save();

        $category_6 = new SmsCategory();
        $category_6->name = 'Other Category';
        $category_6->description = 'Non mentioned categories';
        $category_6->save();

        $category_7 = new SmsCategory();
        $category_7->name = 'Notifications';
        $category_7->description = 'System Event Notifications';
        $category_7->save();


    }
}
