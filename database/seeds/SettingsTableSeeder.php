<?php

use Illuminate\Database\Seeder;
use App\System;
class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $system_settings = new System();
        $system_settings->telephone = '+26461413800';
        $system_settings->email = 'support@lexconsult.na';
        $system_settings->address = 'Corner of Brandberg and Erongo Street, Eros, Windhoek, Namibia';
        $system_settings->save();
    }
}
