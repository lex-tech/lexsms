<?php

use Illuminate\Database\Seeder;
use App\User;
use \Spatie\Permission\Models\Role;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(User::class, 1)->create();
        /**
         * testing users
         */
        $admin = new User();
        $admin->name = "Administrator";
        $admin->email = "admin@admin.com";
        $admin->password = "admin";
        $admin->remember_token = str_random(10);
        $admin->save();
        $admin->assignRole(Role::where('name', 'Admin')->first());

        $client = new User();
        $client->name = "Client";
        $client->email = "client@client.com";
        $client->password = "client";
        $client->remember_token = str_random(10);
        $client->sms_credit = 400;
        $client->save();
        $admin->creator()->save($client);
        $client->assignRole(Role::where('name', 'Client')->first());

        $sub_client = new User();
        $sub_client->name = "Sub-Client";
        $sub_client->email = "subclient@client.com";
        $sub_client->password = "subclient";
        $sub_client->remember_token = str_random(10);
//        $client->sms_credit = 20;
        $sub_client->save();
        $client->creator()->save($sub_client);
        $sub_client->assignRole(Role::where('name', 'Sub-Client')->first());

        $demo_client = new User();
        $demo_client->name = "Demo-Client";
        $demo_client->email = "demo@client.com";
        $demo_client->password = "demo";
        $demo_client->remember_token = str_random(10);
//        $client->sms_credit = 20;
        $demo_client->save();
        $admin->creator()->save($demo_client);
        $demo_client->assignRole(Role::where('name', 'Demo')->first());

        /**
         * Real Users
         */

        $lex_client = new User();
        $lex_client->name = "Lex Technologies";
        $lex_client->email = "support@lexconsult.na";
        $lex_client->password = "lexsupport";
        $lex_client->remember_token = str_random(10);
//        $client->sms_credit = 20;
        $lex_client->save();
        $admin->creator()->save($lex_client);
        $lex_client->assignRole(Role::where('name', 'Client')->first());

        $peni_subclient = new User();
        $peni_subclient->name = "Peni";
        $peni_subclient->email = "pshilunga@lexconsult.na";
        $peni_subclient->password = "peni";
        $peni_subclient->remember_token = str_random(10);
//        $client->sms_credit = 20;
        $peni_subclient->save();
        $lex_client->creator()->save($peni_subclient);
        $peni_subclient->assignRole(Role::where('name', 'Sub-Client')->first());

        $paulina_subclient = new User();
        $paulina_subclient->name = "Paulina";
        $paulina_subclient->email = "pshanyenganje@lexconsult.na";
        $paulina_subclient->password = "paulina";
        $paulina_subclient->remember_token = str_random(10);
//        $client->sms_credit = 20;
        $paulina_subclient->save();
        $lex_client->creator()->save($paulina_subclient);
        $paulina_subclient->assignRole(Role::where('name', 'Sub-Client')->first());

        $tonata_subclient = new User();
        $tonata_subclient->name = "Tonata";
        $tonata_subclient->email = "tnakashololo@lexconsult.na";
        $tonata_subclient->password = "tonata";
        $tonata_subclient->remember_token = str_random(10);
//        $client->sms_credit = 20;
        $tonata_subclient->save();
        $lex_client->creator()->save($tonata_subclient);
        $tonata_subclient->assignRole(Role::where('name', 'Sub-Client')->first());
    }
}   
