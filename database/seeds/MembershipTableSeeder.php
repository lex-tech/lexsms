<?php

use Illuminate\Database\Seeder;
use App\Membership;
use Carbon\Carbon;

class MembershipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $start_dt = Carbon::create(2017, 10, 10, 2, 30);
        $end_dt = Carbon::create(2017, 12, 10, 2, 30);

        $membership = new Membership();
        $membership->name = 'Bible Verses';
        $membership->keyword = 'bible';
        $membership->start_date = $start_dt ;
        $membership->end_date = $end_dt;
        $membership->save();

        $membership = new Membership();
        $membership->name = 'Inspiration Quotes';
        $membership->keyword = 'quotes';
        $membership->start_date = $start_dt;
        $membership->end_date = $end_dt;
        $membership->save();

        $membership = new Membership();
        $membership->name = 'Horoscopes';
        $membership->keyword = 'scopes';
        $membership->start_date = $start_dt;
        $membership->end_date = $end_dt;
        $membership->save();
    }
}
