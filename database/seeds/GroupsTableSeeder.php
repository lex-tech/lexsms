<?php

use Illuminate\Database\Seeder;
class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Group::class, 10)->create()->each(function (\App\Group $group){
            $creator = \App\User::findorfail(1);

            $creator->groups()->save($group);
        });
    }
}
