<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 12:15 PM
 */

use \Spatie\Permission\Models\Permission;

$manage_subscription_model_permission = new Permission();
$manage_subscription_model_permission->name = 'Manage Subscriptions';
$manage_subscription_model_permission->guard_name = 'web';
$manage_subscription_model_permission->save();

$view_subscription_model_permission = new Permission();
$view_subscription_model_permission->name = 'View Subscriptions';
$view_subscription_model_permission->guard_name = 'web';
$view_subscription_model_permission->save();

$create_subscription_model_permission = new Permission();
$create_subscription_model_permission->name = 'Create Subscriptions';
$create_subscription_model_permission->guard_name = 'web';
$create_subscription_model_permission->save();


$delete_subscription_model_permission = new Permission();
$delete_subscription_model_permission->name = 'Delete Subscriptions';
$delete_subscription_model_permission->guard_name = 'web';
$delete_subscription_model_permission->save();


$update_subscription_model_permission = new Permission();
$update_subscription_model_permission->name = 'Update Subscriptions';
$update_subscription_model_permission->guard_name = 'web';
$update_subscription_model_permission->save();

$subscribe_users_permission = new Permission();
$subscribe_users_permission->name = 'Subscribe Users';
$subscribe_users_permission->guard_name = 'web';
$subscribe_users_permission->save();
