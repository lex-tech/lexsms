<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 12:15 PM
 */

use \Spatie\Permission\Models\Permission;

$manage_sms_model_permission = new Permission();
$manage_sms_model_permission->name = 'Manage SMS Services';
$manage_sms_model_permission->guard_name = 'web';
$manage_sms_model_permission->save();

$view_sms_model_permission = new Permission();
$view_sms_model_permission->name = 'Create SMS';
$view_sms_model_permission->guard_name = 'web';
$view_sms_model_permission->save();

$create_sms_model_permission = new Permission();
$create_sms_model_permission->name = 'View SMS';
$create_sms_model_permission->guard_name = 'web';
$create_sms_model_permission->save();


$delete_sms_model_permission = new Permission();
$delete_sms_model_permission->name = 'Delete SMS';
$delete_sms_model_permission->guard_name = 'web';
$delete_sms_model_permission->save();

// sms category
$view_sms_category_model_permission = new Permission();
$view_sms_category_model_permission->name = 'View SMS Category';
$view_sms_category_model_permission->guard_name = 'web';
$view_sms_category_model_permission->save();

$delete_sms_category_model_permission = new Permission();
$delete_sms_category_model_permission->name = 'Update SMS Category';
$delete_sms_category_model_permission->guard_name = 'web';
$delete_sms_category_model_permission->save();


$update_sms_category_model_permission = new Permission();
$update_sms_category_model_permission->name = 'Delete SMS Category';
$update_sms_category_model_permission->guard_name = 'web';
$update_sms_category_model_permission->save();


$create_sms_category_model_permission = new Permission();
$create_sms_category_model_permission->name = 'Create SMS Category';
$create_sms_category_model_permission->guard_name = 'web';
$create_sms_category_model_permission->save();


//$update_sms_model_permission = new Permission();
//$update_sms_model_permission->name = 'Update S';
//$update_sms_model_permission->guard_name = 'web';
//$update_sms_model_permission->save();
