<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 11:30 AM
 */
use \Spatie\Permission\Models\Permission;

$manage_recipient_model_permission = new Permission();
$manage_recipient_model_permission->name = 'Manage Recipients';
$manage_recipient_model_permission->guard_name = 'web';
$manage_recipient_model_permission->save();

$view_recipient_model_permission = new Permission();
$view_recipient_model_permission->name = 'View Recipients';
$view_recipient_model_permission->guard_name = 'web';
$view_recipient_model_permission->save();

$create_recipient_model_permission = new Permission();
$create_recipient_model_permission->name = 'Create Recipients';
$create_recipient_model_permission->guard_name = 'web';
$create_recipient_model_permission->save();

$update_recipient_model_permission = new Permission();
$update_recipient_model_permission->name = 'Update Recipients';
$update_recipient_model_permission->guard_name = 'web';
$update_recipient_model_permission->save();

$delete_recipient_model_permission = new Permission();
$delete_recipient_model_permission->name = 'Delete Recipients';
$delete_recipient_model_permission->guard_name = 'web';
$delete_recipient_model_permission->save();

$create_bulk_recipient_model_permission = new Permission();
$create_bulk_recipient_model_permission->name = 'Create Bulk Recipients';
$create_bulk_recipient_model_permission->guard_name = 'web';
$create_bulk_recipient_model_permission->save();
