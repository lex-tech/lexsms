<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 11:30 AM
 */
use \Spatie\Permission\Models\Permission;

$manage_role_model_permission = new Permission();
$manage_role_model_permission->name = 'View Reports';
$manage_role_model_permission->guard_name = 'web';
$manage_role_model_permission->save();

$manage_role_model_permission = new Permission();
$manage_role_model_permission->name = 'View SMS Reports';
$manage_role_model_permission->guard_name = 'web';
$manage_role_model_permission->save();

$manage_role_model_permission = new Permission();
$manage_role_model_permission->name = 'View Financial Reports';
$manage_role_model_permission->guard_name = 'web';
$manage_role_model_permission->save();


