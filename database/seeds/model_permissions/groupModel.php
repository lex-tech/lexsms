<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 11:30 AM
 */
use \Spatie\Permission\Models\Permission;

$manage_group_model_permission = new Permission();
$manage_group_model_permission->name = 'Manage Groups';
$manage_group_model_permission->guard_name = 'web';
$manage_group_model_permission->save();

$view_group_model_permission = new Permission();
$view_group_model_permission->name = 'View Groups';
$view_group_model_permission->guard_name = 'web';
$view_group_model_permission->save();

$create_group_model_permission = new Permission();
$create_group_model_permission->name = 'Create Groups';
$create_group_model_permission->guard_name = 'web';
$create_group_model_permission->save();

$update_group_model_permission = new Permission();
$update_group_model_permission->name = 'Update Groups';
$update_group_model_permission->guard_name = 'web';
$update_group_model_permission->save();

$update_group_model_permission = new Permission();
$update_group_model_permission->name = 'Remove Recipients From Groups';
$update_group_model_permission->guard_name = 'web';
$update_group_model_permission->save();

$delete_group_model_permission = new Permission();
$delete_group_model_permission->name = 'Delete Groups';
$delete_group_model_permission->guard_name = 'web';
$delete_group_model_permission->save();

$assign_recipients_to_groups_group_model_permission = new Permission();
$assign_recipients_to_groups_group_model_permission->name = 'Assign Recipients To Groups';
$assign_recipients_to_groups_group_model_permission->guard_name = 'web';
$assign_recipients_to_groups_group_model_permission->save();

$give_users_access_to_groups_permission = new Permission();
$give_users_access_to_groups_permission->name = 'Give Users Access To Groups';
$give_users_access_to_groups_permission->guard_name = 'web';
$give_users_access_to_groups_permission->save();
