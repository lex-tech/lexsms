<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 11:31 AM
 */
use \Spatie\Permission\Models\Permission;

$manage_post_model_permission = new Permission();
$manage_post_model_permission->name = 'Manage Posts';
$manage_post_model_permission->guard_name = 'web';
$manage_post_model_permission->save();

$view_post_model_permission = new Permission();
$view_post_model_permission->name = 'View Posts';
$view_post_model_permission->guard_name = 'web';
$view_post_model_permission->save();

$create_post_model_permission = new Permission();
$create_post_model_permission->name = 'Create Posts';
$create_post_model_permission->guard_name = 'web';
$create_post_model_permission->save();
// $post_model_permission->givePermissionTo(Role::where('name', 'Client'));

$update_post_model_permission = new Permission();
$update_post_model_permission->name = 'Update Posts';
$update_post_model_permission->guard_name = 'web';
$update_post_model_permission->save();

$delete_post_model_permission = new Permission();
$delete_post_model_permission->name = 'Delete Posts';
$delete_post_model_permission->guard_name = 'web';
$delete_post_model_permission->save();
