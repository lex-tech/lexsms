<?php
/**
 * Created by PhpStorm.
 * User: Tonata
 * Date: 18/12/2017
 * Time: 12:46
 */

use \Spatie\Permission\Models\Permission;

$manage_membership_model_permission = new Permission();
$manage_membership_model_permission->name = 'Manage Memberships';
$manage_membership_model_permission->guard_name = 'web';
$manage_membership_model_permission->save();

$view_membership_model_permission = new Permission();
$view_membership_model_permission->name = 'View Memberships';
$view_membership_model_permission->guard_name = 'web';
$view_membership_model_permission->save();

$view_client_membership_model_permission = new Permission();
$view_client_membership_model_permission->name = 'View Client Memberships';
$view_client_membership_model_permission->guard_name = 'web';
$view_client_membership_model_permission->save();

$create_membership_model_permission = new Permission();
$create_membership_model_permission->name = 'Create Memberships';
$create_membership_model_permission->guard_name = 'web';
$create_membership_model_permission->save();

$messaging_membership_model_permission = new Permission();
$messaging_membership_model_permission->name = 'Messaging';
$messaging_membership_model_permission->guard_name = 'web';
$messaging_membership_model_permission->save();


$delete_membership_model_permission = new Permission();
$delete_membership_model_permission->name = 'Delete Memberships';
$delete_membership_model_permission->guard_name = 'web';
$delete_membership_model_permission->save();


$update_membership_model_permission = new Permission();
$update_membership_model_permission->name = 'Update Memberships';
$update_membership_model_permission->guard_name = 'web';
$update_membership_model_permission->save();

$client_membership_permission = new Permission();
$client_membership_permission->name = 'Create Client Memberships';
$client_membership_permission->guard_name = 'web';
$client_membership_permission->save();