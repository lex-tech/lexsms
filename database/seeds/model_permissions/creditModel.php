<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 11:30 AM
 */
use \Spatie\Permission\Models\Permission;

$manage_credit_model_permission = new Permission();
$manage_credit_model_permission->name = 'Manage Credit';
$manage_credit_model_permission->guard_name = 'web';
$manage_credit_model_permission->save();

$view_credit_model_permission = new Permission();
$view_credit_model_permission->name = 'View Credit Transactions';
$view_credit_model_permission->guard_name = 'web';
$view_credit_model_permission->save();

$create_credit_model_permission = new Permission();
$create_credit_model_permission->name = 'Create Credit';
$create_credit_model_permission->guard_name = 'web';
$create_credit_model_permission->save();
// $credit_model_permission->givePermissionTo(Role::where('name', 'Admin'));

$update_credit_model_permission = new Permission();
$update_credit_model_permission->name = 'Update Credit';
$update_credit_model_permission->guard_name = 'web';
$update_credit_model_permission->save();

$delete_credit_model_permission = new Permission();
$delete_credit_model_permission->name = 'Delete Credit';
$delete_credit_model_permission->guard_name = 'web';
$delete_credit_model_permission->save();

$request_credit_model_permission = new Permission();
$request_credit_model_permission->name = 'Request Credit';
$request_credit_model_permission->guard_name = 'web';
$request_credit_model_permission->save();

$realocate_credit_model_permission = new Permission();
$realocate_credit_model_permission->name = 'Reallocate Credit';
$realocate_credit_model_permission->guard_name = 'web';
$realocate_credit_model_permission->save();

$view_credit_reallocation_transactions_credit_model_permission = new Permission();
$view_credit_reallocation_transactions_credit_model_permission->name = 'View Credit Reallocation Transactions';
$view_credit_reallocation_transactions_credit_model_permission->guard_name = 'web';
$view_credit_reallocation_transactions_credit_model_permission->save();

$view_credit_requestt_transactions_credit_model_permission = new Permission();
$view_credit_requestt_transactions_credit_model_permission->name = 'View Credit Requests';
$view_credit_requestt_transactions_credit_model_permission->guard_name = 'web';
$view_credit_requestt_transactions_credit_model_permission->save();
