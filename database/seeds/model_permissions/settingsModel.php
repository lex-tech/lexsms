<?php
/**
 * Created by PhpStorm.
 * User: MT_IT
 * Date: 11/14/2017
 * Time: 12:15 PM
 */

use \Spatie\Permission\Models\Permission;

$manage_settings_model_permission = new Permission();
$manage_settings_model_permission->name = 'Edit System Settings';
$manage_settings_model_permission->guard_name = 'web';
$manage_settings_model_permission->save();

$manage_settings_model_permission = new Permission();
$manage_settings_model_permission->name = 'View System Settings';
$manage_settings_model_permission->guard_name = 'web';
$manage_settings_model_permission->save();