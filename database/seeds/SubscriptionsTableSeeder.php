<?php

use Illuminate\Database\Seeder;
use App\Subscription;

class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscription = new Subscription();
        $subscription->name = 'Basic';
        $subscription->duration = 30;
        $subscription->sms_number = 1000;
        $subscription->save();

        $subscription = new Subscription();
        $subscription->name = 'Silver';
        $subscription->duration = 30;
        $subscription->sms_number = 3000;
        $subscription->save();

        $subscription = new Subscription();
        $subscription->name = 'Gold';
        $subscription->duration = 30;
        $subscription->sms_number = 5000;
        $subscription->save();

        $subscription = new Subscription();
        $subscription->name = 'Platinum';
        $subscription->duration = 30;
        $subscription->sms_number = 10000;
        $subscription->save();
    }
}
