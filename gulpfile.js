var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var pug = require('gulp-pug');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var beautify = require('gulp-html-beautify');
var pkg = require('./package.json');

// Set the banner content
var banner = ['/*!\n',
    ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * Licensed under <%= pkg.license %> (https://github.com/BlackrockDigital/<%= pkg.name %>/blob/master/LICENSE)\n',
    ' */\n',
    ''
].join('');

// Compiles SCSS files from /scss into /css
// gulp.task('sass', function() {
//     return gulp.src('resources/assets/sass/sb-admin.scss')
//         .pipe(sass())
//         .pipe(header(banner, {
//             pkg: pkg
//         }))
//         .pipe(gulp.dest('public/css'))
//         .pipe(browserSync.reload({
//             stream: true
//         }))
// });

// Minify compiled CSS
// gulp.task('minify-css', ['sass'], function() {
//     return gulp.src('css/sb-admin.css')
//         .pipe(cleanCSS({
//             compatibility: 'ie8'
//         }))
//         .pipe(rename({
//             suffix: '.min'
//         }))
//         .pipe(gulp.dest('public/css'))
//         .pipe(browserSync.reload({
//             stream: true
//         }))
// });

// Minify custom JS
// gulp.task('minify-js', function() {
//     return gulp.src(['public/js/**/*.js', '!public/js/**/*.min.js'])
//         .pipe(uglify())
//         .pipe(header(banner, {
//             pkg: pkg
//         }))
//         .pipe(rename({
//             suffix: '.min'
//         }))
//         .pipe(gulp.dest('public/js'))
//         .pipe(browserSync.reload({
//             stream: true
//         }))
// });

// Compiles Pug files from /pug into the views folder and beautifies the HTML
// gulp.task('pug', function buildHTML() {
//     return gulp.src('resources/assets/pug/*.pug')
//         .pipe(pug())
//         .pipe(beautify())
//         .pipe(gulp.dest('resources/views/pugs'))
//         .pipe(browserSync.reload({
//             stream: true
//         }))
// });

// Copy vendor files from /node_modules into /vendor
// NOTE: requires `npm install` before running!
gulp.task('copy', function() {
    // gulp.src([
    //     'node_modules/bootstrap/dist/**/*',
    //     '!**/npm.js',
    //     '!**/bootstrap-theme.*',
    //     '!**/*.map'
    // ])
    //     .pipe(gulp.dest('public/vendor/bootstrap'))

    // gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js'])
    //     .pipe(gulp.dest('public/vendor/jquery'))

    // gulp.src(['node_modules/jquery.easing/*.js'])
    //     .pipe(gulp.dest('public/vendor/jquery-easing'))

    // gulp.src([
    //     'node_modules/font-awesome/**',
    //     '!node_modules/font-awesome/**/*.map',
    //     '!node_modules/font-awesome/.npmignore',
    //     '!node_modules/font-awesome/*.txt',
    //     '!node_modules/font-awesome/*.md',
    //     '!node_modules/font-awesome/*.json'
    // ])
    //     .pipe(gulp.dest('public/vendor/font-awesome'))

    // gulp.src(['node_modules/chart.js/dist/*.js'])
    //     .pipe(gulp.dest('public/vendor/chart.js'))

    // gulp.src([
    //     'node_modules/datatables.net/js/*.js',
    //     'node_modules/datatables.net-bs4/js/*.js',
    //     'node_modules/datatables.net-bs4/css/*.css'
    // ])
    //     .pipe(gulp.dest('public/vendor/datatables/'))

    // gulp.src([
    //     'node_modules/bootstrap-select/dist/css/*.css',
    //     'node_modules/bootstrap-select/dist/js/*.js',
    // ])
    //     .pipe(gulp.dest('public/vendor/bootstrap-select/'))

    // gulp.src([
    //     'node_modules/popper.js/dist/*.js',
    // ])
    //     .pipe(gulp.dest('public/vendor/popper-js/'))

    // gulp.src([
    //     'bower_components/eonasdan-bootstrap-datetimepicker/build/js/*.js',
    // ])
    //     .pipe(gulp.dest('public/vendor/bootstrap-datepicker/js'))
    //
    // gulp.src([
    //     'bower_components/eonasdan-bootstrap-datetimepicker/build/css/*.css',
    // ])
    //     .pipe(gulp.dest('public/vendor/bootstrap-datepicker/css'))
    //
    // gulp.src([
    //     'bower_components/moment/*/*.js',
    // ])
    //     .pipe(gulp.dest('public/vendor/moment'))
})

// Default task
// gulp.task('default', ['sass', 'minify-css', 'minify-js', 'copy']);
gulp.task('default', ['copy']);

// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: ''
        },
    })
})

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'sass', 'minify-css', 'minify-js', 'pug'], function() {
    gulp.watch('scss/**/*', ['sass']);
    gulp.watch('pug/**/*', ['pug']);
    gulp.watch('css/*.css', ['minify-css']);
    gulp.watch('js/*.js', ['minify-js']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('*.html', browserSync.reload);
    gulp.watch('js/**/*.js', browserSync.reload);
});
