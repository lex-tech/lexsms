@extends('layouts.app')

@section('title', 'Edit Post')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{url('billing')}}">Billing</a></li>
    <li class="breadcrumb-item active">Edit billing</li>
@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel panel-heading"><h4>Edit Bill</h4></div>

        <div class="panel panel-body">

            @include('partials.forms.edit_billing')

        </div>
    </div>
@endsection