@extends('layouts.app')

@section('title', 'Billing')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item">Billing</li>
@endsection

@section('content')
    @include('flash::message')

    <div>
        <ul class="nav nav-tabs">
            {{--@can('Update Profile')--}}
            <li class="{{ session('tab') == 'view_billings' || !session('tab') ? 'active': ''  }}">
                <a href="#view_billings" data-toggle="tab"> <i class="fa fa-dollar"></i> Bills</a>
            </li>
            {{--@endcan--}}

            @can('Update Contact Details')
                <li class="{{ session('tab') == 'update_contact_info' ? 'active': ''  }}">
                    <a href="#update_contact_info" data-toggle="tab"> <i class="fa fa-phone"></i> Update Contact Details</a>
                </li>
            @endcan

            @hasrole('Client')
                <li class="{{ session('tab') == 'upload_invoice' ? 'active': ''  }}">
                    <a href="#upload_invoice" data-toggle="tab"> <i class="fa fa-upload"></i> Upload Payment Invoice </a>
                </li>
            @endhasrole

        </ul>

        <div class="tab-content">
            @can('View Billings')
            <div class="tab-pane {{ session('tab') == 'view_billings' || !session('tab')? 'active': ''  }}"
                 id="view_billings">
                    <div class="panel panel-default panel-body">
                        <fieldset>
                            @include('partials.tables.bills')
                        </fieldset>
                    </div>
            </div>
            @endcan

            @can('Update Contact Details')
                <div class="tab-pane {{ session('tab') == 'update_contact_info'? 'active': ''  }}" id="update_contact_info">
                    <div class="panel panel-default">
                        <div class="panel panel-body">
                            <fieldset>
                                <legend>Contact Details</legend>
                                @include('partials.forms.edit_contact_details')
                            </fieldset>
                        </div>
                    </div>
                </div>
            @endcan


            @hasrole('Client')
                <div class="tab-pane {{ session('tab') == 'upload_invoice'? 'active': ''  }}" id="upload_invoice">
                    <div class="panel panel-default">
                        <div class="panel panel-body">
                            <fieldset>
                                <legend>Invoice</legend>
                                @include('partials.forms.upload_invoice')
                            </fieldset>
                        </div>
                    </div>
                </div>
            @endhasrole
        </div>
    </div>
@endsection