@extends('layouts.app')

@section('title', 'Messaging')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item">Outbox</li>
@endsection

@section('content')

    <div>
        @include('flash::message')
        <ul class="nav nav-tabs">
            @can('View SMS')
                <li class="{{ session('tab') == 'view_outbox' || session('tab') == '' ? 'active': ''  }}">
                    <a href="#view_outbox" data-toggle="tab"> <i class="fa fa-inbox"></i> Sent Messages</a>
                </li>
            @endcan

            @can('View SMS Category')
                <li class="{{ session('tab') == 'view_sms_category' ? 'active': ''  }}">
                    <a href="#view_sms_category" data-toggle="tab"> <i class="fa fa-list-ul"></i>  SMS Category</a>
                </li>
            @endcan

            @can('Create SMS')
                <li class="{{ session('tab') == 'create_custom' ? 'active': ''  }}">
                    <a href="#create_custom" data-toggle="tab"> <i class="fa fa-angle-up"></i> SMS Custom Recipients</a>
                </li>
            @endcan

            @can('Create SMS')
                <li class="{{ session('tab') == 'create_bulk' ? 'active': ''  }}">
                    <a href="#create_bulk" data-toggle="tab"> <i class="fa fa-angle-double-up"></i> SMS Bulk Group Recipients</a>
                </li>
            @endcan
        </ul>

        <div class="tab-content">



            @can('View SMS')
                <div class="tab-pane {{ session('tab') == 'view_outbox' || session('tab') == '' ? 'active': ''  }}" id="view_outbox">
                   <div class="panel panel-default panel-body">
                       <fieldset>
                           @include('partials.outbox')
                       </fieldset>
                   </div>
                </div>
            @endcan

            @can('View SMS Category')
                <div class="tab-pane {{ session('tab') == 'view_sms_category' ? 'active': ''  }}" id="view_sms_category">
                    <div class="panel panel-default panel-body">
                        <fieldset>
                        @include('partials.forms.sms_categories')
                        </fieldset>
                    </div>
                </div>
            @endcan

            @can('Create SMS')
                <div class="tab-pane {{ session('tab') == 'create_custom' ? 'active': ''  }}" id="create_custom">

                    <div class="panel panel-default panel-body">
                        <fieldset>
                            @include('partials.forms.recipients')
                        </fieldset>
                    </div>
                </div>
            @endcan

            @can('Create SMS')
                <div class="tab-pane {{ session('tab') == 'create_bulk' ? 'active': ''  }}" id="create_bulk">
                    <div class="panel panel-default panel-body">
                       <fieldset>
                           @include('partials.forms.groups')
                       </fieldset>
                    </div>
                </div>
            @endcan
        </div>
    </div>

@endsection