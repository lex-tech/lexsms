{{-- \resources\views\permissions\index.blade.php --}}
@extends('layouts.app')

@section('title', 'Permissions')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item">Permissions </li>
@endsection

@section('content')
        <div>
            @include('flash::message')
            <ul class="nav nav-tabs">
                @can('View Permissions')
                    <li class="{{ session('tab') == 'view_permissions' || !session('tab') ? 'active': ''  }}">
                        <a href="#view_permissions" data-toggle="tab"> <i class="fa fa-list-ul"></i>  Permissions</a>
                    </li>
                @endcan
                @can('Create Permissions')
                    <li class="{{ session('tab') == 'create_permissions' ? 'active': ''  }}">
                        <a href="#create_permissions" data-toggle="tab"> <i class="fa fa-plus"></i> Create Permissions</a>
                    </li>
                @endcan

            </ul>
            <div class="tab-content">
                @can('View Permissions')
                    <div class="tab-pane {{ session('tab') == 'view_permissions'? 'active': ''  }}" id="view_permissions">
                        <div class="panel panel-default">
                            <fieldset>
                                @include('partials.tables.permissions')
                            </fieldset>
                        </div>
                    </div>
                @endcan

                @can('Create Permissions')
                    <div class="tab-pane {{ session('tab') == 'create_permissions'? 'active': ''  }}" id="create_permissions">
                        <div class="panel panel-default panel-flush">
                            <div class="panel panel-body">
                                @include('partials.forms.create_permissions')
                            </div>
                        </div>
                    </div>
                @endcan
            </div>
        </div>

@endsection