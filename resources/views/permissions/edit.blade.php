@extends('layouts.app')

@section('title', '| Edit Permission')

@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{url('/permissions')}}">Permissions</a> </li>
    <li class="breadcrumb-item action">Edit Permission </li>
@endsection

@section('content')

    {{--  <div class='col-lg-4 col-lg-offset-4'>  --}}
    <div class="panel panel-default">
        <div class="panel panel-heading">
            <h4><i class='fa fa-key'></i> Edit {{$permission->name}}</h4>
        </div>

        <div class="panel panel-body">
            {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}

           <fieldset>
               <div class="form-group">
                   {{ Form::label('name', 'Permission Name') }}
                   {{ Form::text('name', null, array('class' => 'form-control')) }}

                   @if ($errors->has('name'))
                       <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                   @endif
               </div>
           </fieldset>
            <br>
            {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit"></i> Update Permission </button>';
            }) !!}
            {!! Form::SubmitBtn() !!}

            {{ Form::close() }}
        </div>
    </div>


    {{--  </div>  --}}

@endsection