{!! Form::open(array('route' => 'subscriptions.store', 'method' => 'POST')) !!}

<fieldset>

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name',null, array('class' => 'form-control')) !!}
        @if ($errors->has('name'))
            <span class="help-block">
        <strong >{{ $errors->first('name') }}</strong>
    </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
        {!! Form::label('duration', 'Duration (days):') !!}
        {!! Form::number('duration',null, array('class' => 'form-control')) !!}
        @if ($errors->has('duration'))
            <span class="help-block">
        <strong >{{ $errors->first('duration') }}</strong>
    </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('sms_number') ? ' has-error' : '' }}">
        {!! Form::label('sms_number', 'SMS Quantity:') !!}
        {!! Form::number('sms_number',null, array('class' => 'form-control')) !!}
        @if ($errors->has('sms_number'))
            <span class="help-block">
        <strong >{{ $errors->first('sms_number') }}</strong>
    </span>
        @endif
    </div>

</fieldset>
<br>
{!! Form::macro('SubmitBtn',function (){
            return '<button type="submit" class="btn btn-primary"> <i class="fa fa-plus" ></i> Create Subscription </button>';
}) !!}

<div class="form-group">
    {!! Form::SubmitBtn() !!}
</div>
{!! Form::close() !!}