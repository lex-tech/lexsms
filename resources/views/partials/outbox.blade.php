@component('components.tables.table')
@slot('title')
Sent Messages
@endslot

{{--<a href="{{ route('out.create') }}" class="btn btn-success">Create SMS</a>--}}

@slot('thead')
    <th>#</th>
    <th>Sender Id</th>
    <th>Receiver</th>
    <th>Message</th>
    <th>Time Sent</th>
    <th>Time Received</th>
    {{--<th>Operator</th>--}}
    <th>Status</th>
    {{--<th>Error Message</th>--}}
@endslot

@slot('tfoot')
<th>#</th>
<th>Sender Id</th>
<th>Reciever</th>
<th>Message</th>
<th>Time Sent</th>
<th>Time Recieved</th>
{{--<th>Operator</th>--}}
<th>Status</th>
{{--<th>Error Message</th>--}}
@endslot

@slot('tbody')

    @foreach($messages as $message)

        <tr>
            <td>{{ $message->id  }}</td>
            <td>{{ $message->sent_by->name  }}</td>
            <td>{{ $message->receiver  }}</td>
            <td>{{ $message->msg  }}</td>
            <td>{{ $message->senttime  }}</td>
            <td>{{ $message->receivedtime  }}</td>
            {{--<td> {{ $message->operator }} </td>--}}
            <td> {{ $message->status  }}</td>
            {{--<td>{{ $message->errormsg  }}</td>--}}
        </tr>

        @endforeach

@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent
