<div class="col-md-8">
    {{ Form::open(array('route' => 'groups.store', 'method' => 'POST')) }}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', null, array('class' => 'form-control', 'rows' => 5)) }}
        @if ($errors->has('description'))
            <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group">
        {!! Form::macro('SubmitBtn',function (){
        return '<button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Create Group </button>';
        }) !!}
        {!! Form::SubmitBtn() !!}
    </div>

    {{ Form::close() }}
</div>
