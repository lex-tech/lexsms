{{ Form::open(array('route' => 'credits.store', 'method' => 'POST')) }}


<div class="form-group{{ $errors->has('reallocate_from') ? ' has-error' : '' }}">
    {{ Form::label('reallocate_from', 'Reallocate Credit From:') }}
    {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('reallocate_from', ['class="selectpicker form-control show-menu-arrow"',
                                            'data-live-search="true"', 'data-header="Select Client"',
                                             'showSubtext="true"', 'data-width="75%"', 'title="Choose client to allocate credit from"'], [],  $clients)
     !!}
    @if ($errors->has('reallocate_from'))
        <span class="help-block">
             <strong>{{ $errors->first('reallocate_from') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('credit') ? ' has-error' : '' }}">
    {{ Form::label('name', 'Credit') }}
    {{ Form::number('credit', null, array('class' => 'form-control')) }}
    @if ($errors->has('credit'))
        <span class="help-block">
            <strong>{{ $errors->first('credit') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('reallocate_to') ? ' has-error' : '' }}">
    {{ Form::label('reallocate_to', 'Reallocate Credit To:') }}
    {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('reallocate_to', ['class="selectpicker form-control show-menu-arrow"',
                                            'data-live-search="true"', 'data-header="Select Client"',
                                             'showSubtext="true"', 'data-width="75%"', 'title="Choose client to allocate credit to"'], [],  $clients)
     !!}
    @if ($errors->has('reallocate_to'))
        <span class="help-block">
             <strong>{{ $errors->first('reallocate_to') }}</strong>
        </span>
    @endif
</div>

{{ Form::text('reallocate-credit-form', null, array('class' => 'form-control', 'value' => 'reallocate-credit-form', 'hidden')) }}

{!! Form::macro('SubmitBtn',function (){
               return '<button type="submit" class="btn btn-primary"> <i class="fa fa-reply-all" ></i> Reallocate Credit </button>';
}) !!}

<div class="form-group">
    {!! Form::SubmitBtn() !!}
</div>
{{ Form::close() }}