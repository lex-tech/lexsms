{{ Form::model($recipient ,array('route' => array('recipients.update', $recipient->id) , 'method' => 'PUT')) }}
<fieldset>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name:') }}
        {{ Form::text('name',null, array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('cellphone_number') ? ' has-error' : '' }}">
        {{ Form::label('cellphone_number', 'Cellphone_number:') }}
        {{ Form::text('cellphone_number',null, array('class' => 'form-control')) }}
        @if ($errors->has('cellphone_number'))
            <span class="help-block">
            <strong>{{ $errors->first('cellphone_number') }}</strong>
        </span>
        @endif
    </div>
</fieldset>
<br>
<div class="form-group">
    {{ Form::submit('Update Recipient', array('class' => 'btn btn-primary')) }}
</div>
{{ Form::close() }}