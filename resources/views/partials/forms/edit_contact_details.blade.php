{{ Form::model($client_contact_info, array('route' => array('profile.update', \Illuminate\Support\Facades\Crypt::encrypt($user->id)), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

{{--form name--}}
{{ Form::text('update_profile', null, array('class' => 'form-control', 'value' => 'update_profile', 'hidden')) }}

<div class="form-group{{ $errors->has('telephone_number') ? ' has-error' : '' }}">
    {{ Form::label('telephone_number', 'Telephone Number') }}
    {{ Form::text('telephone_number', null, array('class' => 'form-control')) }}

    @if ($errors->has('telephone_number'))
        <span class="help-block">
                <strong>{{ $errors->first('telephone_number') }}</strong>
        </span>
    @endif

</div>

<div class="form-group{{ $errors->has('cellphone_number') ? ' has-error' : '' }}">
    {{ Form::label('cellphone_number', 'Cellphone Number') }}
    {{ Form::text('cellphone_number', null, array('class' => 'form-control')) }}

    @if ($errors->has('cellphone_number'))
        <span class="help-block">
                <strong>{{ $errors->first('cellphone_number') }}</strong>
        </span>
    @endif

</div>

<div class="form-group{{ $errors->has('email_address') ? ' has-error' : '' }}">
    {{ Form::label('email_address', 'Contact Email') }}
    {{ Form::email('email_address', null, array('class' => 'form-control')) }}
    @if ($errors->has('email_address'))
        <span class="help-block">
            <strong>{{ $errors->first('email_address') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    {{ Form::label('address', 'Physical Address') }}
    {{ Form::textarea('address', null, array('class' => 'form-control', 'rows' => '4')) }}
    @if ($errors->has('address'))
        <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
        </span>
    @endif
</div>


<div class="form-group{{ $errors->has('postal_address') ? ' has-error' : '' }}">
    {{ Form::label('postal_address', 'Postal Address') }}<br>
    {{ Form::textarea('postal_address',null, array('class' => 'form-control', 'rows' => '3')) }}
    @if ($errors->has('postal_address'))
        <span class="help-block">
            <strong>{{ $errors->first('postal_address') }}</strong>
        </span>
    @endif
</div>

{!! Form::macro('SubmitBtn',function (){
    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit"></i> Update Contact Details </button>';
}) !!}
{!! Form::SubmitBtn() !!}
{{ Form::close() }}