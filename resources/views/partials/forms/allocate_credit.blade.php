{{ Form::open(array('route' => 'credits.store', 'method' => 'POST')) }}

<div class="form-group{{ $errors->has('credit') ? ' has-error' : '' }}">
    {{ Form::label('name', 'Credit') }}
    {{ Form::text('credit', null, array('class' => 'form-control')) }}
    @if ($errors->has('credit'))
        <span class="help-block">
            <strong>{{ $errors->first('credit') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('client') ? ' has-error' : '' }}">
    {{ Form::label('client', 'Allocate Credit To:') }}
    {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('client', ['class="selectpicker form-control show-menu-arrow"',
                                            'data-live-search="true"', 'data-header="Select Client"',
                                             'showSubtext="true"', 'data-width="75%"', 'title="Choose client to allocate credit to"'], [],  $clients)
     !!}
    @if ($errors->has('client'))
        <span class="help-block">
             <strong>{{ $errors->first('client') }}</strong>
        </span>
    @endif
</div>

{!! Form::macro('SubmitBtn',function (){
               return '<button type="submit" class="btn btn-primary"> <i class="fa fa-credit-card" ></i> Allocate Credit </button>';
           }) !!}

<div class="form-group">
    {!! Form::SubmitBtn() !!}
</div>

{{ Form::close() }}