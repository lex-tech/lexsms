@include('flash::message')

{{ Form::open(array('url' => 'permissions')) }}

<fieldset>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}

        @if ($errors->has('name'))
            <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif

    </div>
</fieldset>
    <br>

<fieldset>
    @if(!$roles->isEmpty()) {{--  If no roles exist yet  --}}
    <legend>
        Assign Permission to Roles
    </legend>

    @if ($errors->has('permissions'))
        <span class="help-block">
                    <strong>{{ $errors->first('permissions') }}</strong>
                </span>
    @endif
    @foreach ($roles as $role)
        {{ Form::checkbox('roles[]',  $role->id ) }}
        {{ Form::label($role->name, ucfirst($role->name)) }}<br>

    @endforeach
    @endif
</fieldset>

<br>
{!! Form::macro('SubmitBtn',function (){
    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Create Permission </button>';
}) !!}
{!! Form::SubmitBtn() !!}

{{ Form::close() }}