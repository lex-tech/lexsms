{{ Form::model($billing, array('route' => array('billing.update', $billing->id), 'method' => 'PUT')) }}
<div class="form-group{{ $errors->has('payment_status') ? ' has-error' : '' }}">
    <fieldset>
        <legend>{{ Form::label('payment_status', 'Payment Status') }}</legend>
        {{ Form::radio('payment_status' ,1, ['class' => ' field', 'checked' => $billing->payment_status? true : false]) }}
        {{ Form::label('', 'Paid') }}
        <br>
        {{ Form::radio('payment_status' ,0, ['class' => ' field', 'checked' => !$billing->payment_status? false : true]) }}
        {{ Form::label('', 'Payment Pending') }}
        @if ($errors->has('payment_status'))
            <span class="help-block">
            <strong>{{ $errors->first('payment_status') }}</strong>
        </span>
        @endif
    </fieldset>
</div>
{{--<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">--}}
    {{--{{ Form::label('description', 'Description') }}--}}
    {{--{{ Form::textarea('description', null, array('class' => 'form-control')) }}--}}
    {{--@if ($errors->has('description'))--}}
        {{--<span class="help-block">--}}
                        {{--<strong>{{ $errors->first('description') }}</strong>--}}
                    {{--</span>--}}
    {{--@endif--}}
{{--</div>--}}
{!! Form::macro('SubmitBtn',function (){
    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit"></i> Update </button>';
}) !!}
{!! Form::SubmitBtn() !!}

{{ Form::close() }}