{!! Form::open(array('route' => 'out.store', 'method' => 'POST')) !!}

    @include('partials.inputs.msg')

    <div class="form-group{{ $errors->has('receiver') ? ' has-error' : '' }}">
        {!! Form::label('receiver', 'Recipient:') !!}
        {{--create lists component--}}
        {!! Form::component('customSelect', 'components.select.select', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
        {!! Form::customSelect('receiver[ ]', ['class="selectpicker form-control show-menu-arrow"',
                                'data-live-search="true"', 'data-header="Select recipients"',
                                 'showSubtext="true"', 'data-width="75%"', 'title="Choose your custom contacts"', 'multiple'], [],  $recipients)
         !!}
        @if ($errors->has('receiver'))
            <span class="help-block">
                <strong >{{ $errors->first('receiver') }}</strong>
            </span>
        @endif
    </div>

    @include('components.inputs.category')

    {!! Form::macro('SubmitBtn',function (){
               return '<button type="submit" class="btn btn-primary"> <i class="fa fa-location-arrow"></i> Send Message </button>';
    }) !!}

    <div class="form-group">
        {!! Form::SubmitBtn() !!}
    </div>

{!! Form::close() !!}