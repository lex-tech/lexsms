<div class="col-md-8">
    {{ Form::open(array('route' => 'invoices.store', 'method' => 'POST', 'enctype' => 'multipart/form-data')) }}

    <div class="form-group{{ $errors->has('month') ? ' has-error' : '' }}">
        {!! Form::label('month', 'Outstanding Month:') !!}
        {{--create lists component--}}
        {!! Form::component('customSelect', 'components.select.select_month', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
        {!! Form::customSelect('month', ['class="selectpicker form-control show-menu-arrow"',
                                'data-live-search="true"', 'data-header="Select Outstanding Payment Month"',
                                 'showSubtext="true"', 'data-width="75%"', 'title="Choose Outstanding Payment"', 'required'], [],  $months)
         !!}
        @if ($errors->has('month'))
            <span class="help-block">
                <strong >{{ $errors->first('month') }}</strong>
            </span>
        @endif
    </div>
    <br>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('invoice', 'Invoice Document') }}
        {{ Form::file('invoice', null, array('class' => 'form-control')) }}
        @if ($errors->has('invoice'))
            <span class="help-block">
            <strong>{{ $errors->first('invoice') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group">
        {!! Form::macro('SubmitBtn',function (){
        return '<button type="submit" class="btn btn-primary"> <i class="fa fa-upload"></i> Submit </button>';
        }) !!}
        {!! Form::SubmitBtn() !!}
    </div>

    {{ Form::close() }}
</div>
