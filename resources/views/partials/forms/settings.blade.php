{{ Form::model($settings, array('route' => array('systems.update', $settings->id), 'method' => 'PUT')) }}
<div class="form-group{{ $errors->has('default_sms_rate') ? ' has-error' : '' }}">
    {{ Form::label('default_sms_rate', 'SMS Charge') }}
    {{ Form::number('default_sms_rate', number_format($settings->default_sms_rate, 0), array('class' => 'form-control')) }}
    @if ($errors->has('default_sms_rate'))
        <span class="help-block">
            <strong>{{ $errors->first('default_sms_rate') }}</strong>
        </span>
    @endif
</div>
<div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
    {{ Form::label('telephone', 'Telephone') }}
    {{ Form::text('telephone', null, array('class' => 'form-control')) }}
    @if ($errors->has('telephone'))
        <span class="help-block">
            <strong>{{ $errors->first('telephone') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    {{ Form::label('email', 'Support Email') }}
    {{ Form::email('email', null, array('class' => 'form-control')) }}
    @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    {{ Form::label('address', 'Office Address') }}
    {{ Form::textarea('address', null, array('class' => 'form-control', 'rows' => 4)) }}
    @if ($errors->has('address'))
        <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
        </span>
    @endif
</div>
{!! Form::macro('SubmitBtn',function (){
    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit"></i> Update </button>';
}) !!}
{!! Form::SubmitBtn() !!}

{{ Form::close() }}