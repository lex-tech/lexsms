{{ Form::open(array('url' => 'users')) }}

<fieldset>
    <legend>Personal Info</legend>

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('cellphone_number') ? ' has-error' : '' }}">
        {{ Form::label('cellphone_number', 'Cellphone #') }}
        {{ Form::text('cellphone_number', null, array('class' => 'form-control', 'placeholder' => 'Format: 0812190776')) }}
        @if ($errors->has('cellphone_number'))
            <span class="help-block">
                    <strong>{{ $errors->first('cellphone_number') }}</strong>
                </span>
        @endif

    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', '', array('class' => 'form-control')) }}
        @if ($errors->has('email'))
            <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
        @endif
    </div>

</fieldset>
<br>


    <div class='form-group{{ $errors->has('roles') ? ' has-error' : '' }}'>
        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('Admin'))
            <fieldset>
                <legend>Roles</legend>
        @endif
            @foreach ($roles as $role)

                {{-- loop through and only show roles applicatable to the client --}}
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('Client') && $role->name == 'Sub-Client')
                    <div hidden>
                        {{ Form::checkbox('roles[]',  $role->id, array('value'=>true)) }}
                        {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                    </div>
                @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('Admin'))
                            {{ Form::radio('roles[]',  $role->id ) }}
                            {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                @endif

            @endforeach
        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('Admin'))
            </fieldset>
        @endif
        @if ($errors->has('roles'))
            <span class="help-block">
                    <strong>{{ $errors->first('roles') }}</strong>
            </span>
        @endif
    </div>


<br>
<fieldset>
    <legend>Secure Account</legend>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {{ Form::label('password', 'Password') }}<br>
        {{ Form::password('password', array('class' => 'form-control')) }}
        @if ($errors->has('password'))
            <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {{ Form::label('password', 'Confirm Password') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
        @if ($errors->has('password'))
            <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
        @endif
    </div>
</fieldset>

<br>

{!! Form::macro('SubmitBtn',function (){
               return '<button type="submit" class="btn btn-primary"> <i class="fa fa-plus" ></i> Create User </button>';
           }) !!}

<div class="form-group">
    {!! Form::SubmitBtn() !!}
</div>
{{ Form::close() }}