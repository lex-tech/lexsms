{!! Form::open(array('route' => 'out.store', 'method' => 'POST')) !!}

    @include('partials.inputs.msg')

    <div class="form-group{{ $errors->has('groups') ? ' has-error' : '' }}">
        {!! Form::label('groups', 'Groups (Recipients):') !!}
        {{--create lists component--}}
        {!! Form::component('customSelect', 'components.select.select_groups', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
        {!! Form::customSelect('groups[ ]', ['class="selectpicker form-control show-menu-arrow"',
                                'data-live-search="true"', 'data-header="Select Groups"',
                                 'showSubtext="true"', 'data-width="75%"', 'title="Choose your custom groups"', 'multiple'], [],  $groups)
         !!}
        @if ($errors->has('groups'))
            <span class="help-block">
                <strong >{{ $errors->first('groups') }}</strong>
            </span>
        @endif
    </div>

    @include('components.inputs.category')

    <div class="form-group">
        {!! Form::text('bulk-message',null,array('hidden', 'value'=>'bulk-message')) !!}
    </div>

    {!! Form::macro('SubmitBtn',function (){
           return '<button type="submit" class="btn btn-primary"> <i class="fa fa-location-arrow"></i> Send Bulk Message </button>';
    }) !!}

    <div class="form-group">
        {!! Form::SubmitBtn() !!}
    </div>
{!! Form::close() !!}