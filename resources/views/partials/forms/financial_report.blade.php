<div class="col-md-4">
    {{ Form::open(array('route' => 'reports.store', 'method' => 'POST')) }}

    <div class="col-md-8">
        <div class="form-group{{ $errors->has('summary') ? ' has-error' : '' }}">
            {!! Form::component('customSelect', 'components.select.select_sms_summary', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
            {!! Form::customSelect('fa_summary', ['class="selectpicker form-control show-menu-arrow"',
                                                    'data-live-search="true"', 'data-header="Specific Summary"',
                                                     'showSubtext="true"', 'data-width="100%"', 'title="Financial Report Summary"'], [], $fa_summary)
             !!}
            @if ($errors->has('fa_summary'))
                <span class="help-block">
                <strong>{{ $errors->first('fa_summary') }}</strong>
             </span>
            @endif

        </div>
    </div>

    <div class="col-md-2">
        <button type="submit" class="btn btn-primary pull-right" > <i class="fa fa-refresh"></i></button>
    </div>
    {{ Form::close() }}
    <div class="col-md-2">
        <button class="btn btn-primary" role="button" data-toggle="modal" data-target="#financial_report_filter">
            <i class="fa fa-filter"></i> Filter Financial Report
        </button>
    </div>
</div>
<br>
@include('includes.modals.financial_report_filter')