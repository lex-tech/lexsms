{{ Form::model($group, array('route' => array('groups.update', $group->id), 'method' => 'PUT')) }}
<fieldset>
    <legend>Group Info</legend>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', null, array('class' => 'form-control', 'rows' => 5)) }}
        @if ($errors->has('description'))
            <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
        @endif
    </div>
</fieldset>
<br>
{!! Form::macro('SubmitBtn',function (){
    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit"></i> Update </button>';
}) !!}
{!! Form::SubmitBtn() !!}

{{ Form::close() }}