<div class="row">
    <div class="col-md-8">
                 <fieldset>
                     @include('partials.tables.sms_categories')
                 </fieldset>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
               <h4><i class="fa fa-plus"></i> Create Category</h4>
            </div>
            <div class="panel-body">
                {{ Form::open(array('route' => 'sms-categories.store', 'method' => 'POST')) }}

                <fieldset>
                    {{--<legend>Category Info</legend>--}}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', null, array('class' => 'form-control')) }}
                        @if ($errors->has('name'))
                            <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        {{ Form::label('description', 'Description') }}
                        {{ Form::text('description', null, array('class' => 'form-control')) }}
                        @if ($errors->has('description'))
                            <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                        @endif
                    </div>
                </fieldset>
                    {{--hidden field--}}
                {!! Form::text('create_category', null, array('value' => 'create_category', 'hidden' )) !!}
                <br>
                {!! Form::macro('SubmitBtn',function (){
                    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Create SMS Category </button>';
                }) !!}
                {!! Form::SubmitBtn() !!}

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
