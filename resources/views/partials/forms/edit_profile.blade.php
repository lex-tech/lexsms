{{ Form::model($user, array('route' => array('users.update', \Illuminate\Support\Facades\Crypt::encrypt($user->id)), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

{{--form name--}}
{{ Form::text('update_profile', null, array('class' => 'form-control', 'value' => 'update_profile', 'hidden')) }}

<fieldset>
    <legend>
        Profile Info
    </legend>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}

        @if ($errors->has('name'))
            <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
        @endif

    </div>

    <div class="form-group{{ $errors->has('cellphone_number') ? ' has-error' : '' }}">
        {{ Form::label('cellphone_number', 'Cellphone #') }}
        {{ Form::text('cellphone_number', null, array('class' => 'form-control', 'placeholder' => 'Format: 0812190776')) }}

        @if ($errors->has('cellphone_number'))
            <span class="help-block">
                <strong>{{ $errors->first('cellphone_number') }}</strong>
            </span>
        @endif

    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', null, array('class' => 'form-control')) }}
        @if ($errors->has('email'))
            <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
        @endif
    </div>

    <div class='form-group'>
        @foreach ($roles as $role)
            {{-- loop through and only show roles applicatable to the client --}}

            <div hidden>
                {{ Form::checkbox('roles[]',  $role->id ) }}
                {{ Form::label($role->name, ucfirst($role->name)) }}<br>
            </div>

        @endforeach
    </div>
</fieldset>
<br>
<fieldset>
    <legend>
        Secure Account
    </legend>
    <div class="form-group">
        {{ Form::label('password', 'Password') }}<br>
        {{ Form::password('password', array('class' => 'form-control')) }}
        @if ($errors->has('password'))
            <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
        {{ Form::label('password_confirmation', 'Confirm Password') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
        @endif
    </div>
</fieldset>

<br>

{!! Form::macro('SubmitBtn',function (){
    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit"></i> Update User Profile </button>';
}) !!}
{!! Form::SubmitBtn() !!}
{{ Form::close() }}