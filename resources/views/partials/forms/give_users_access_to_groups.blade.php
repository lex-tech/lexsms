{{ Form::open(array('route' => 'groups.store', 'method' => 'POST')) }}

<div class="form-group">
    {!! Form::text('give_users_access_to_groups', null, array('hidden', 'value'=>'give_users_access_to_groups')) !!}
</div>

<div class="form-group{{ $errors->has('user') ? ' has-error' : '' }}">
    {{ Form::label('user', 'User:') }}
    {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('user', ['class="selectpicker form-control show-menu-arrow"',
                                            'data-live-search="true"', 'data-header="Select Client"',
                                             'showSubtext="true"', 'data-width="75%"', 'title="Choose user to authorize"'], [],  $users)
     !!}
    @if ($errors->has('user'))
        <span class="help-block">
             <strong>{{ $errors->first('user') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('groups') ? ' has-error' : '' }}">
    {!! Form::label('groups', 'Groups:') !!}
    {{--create lists component--}}
    {!! Form::component('customSelect', 'components.select.select_groups', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('groups[]', ['class="selectpicker form-control show-menu-arrow"',
                            'data-live-search="true"', 'data-header="Select Group"',
                             'showSubtext="true"', 'data-width="75%"', 'title="Choose groups to authorize to user"', 'multiple' ], [],  $groups)
     !!}
    @if ($errors->has('groups'))
        <span class="help-block">
            <strong >{{ $errors->first('groups') }}</strong>
        </span>
    @endif
</div>

{!! Form::macro('SubmitBtn',function (){
       return '<button type="submit" class="btn btn-primary"> <i class="fa fa-superpowers" ></i> Authorize User </button>';
}) !!}

<div class="form-group">
    {!! Form::SubmitBtn() !!}
</div>

{{ Form::close() }}