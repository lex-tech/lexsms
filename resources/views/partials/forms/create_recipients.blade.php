{!! Form::open(array('route' => 'recipients.store', 'method' => 'POST')) !!}
    <fieldset>
        <legend>Recipient Info</legend>

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name',null, array('class' => 'form-control')) !!}
            @if ($errors->has('name'))
                <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('cellphone_number') ? ' has-error' : '' }}">
            {!! Form::label('cellphone_number', 'Cellphone_number:') !!}
            {!! Form::text('cellphone_number',null, array('class' => 'form-control')) !!}
            @if ($errors->has('cellphone_number'))
                <span class="help-block">
                <strong>{{ $errors->first('cellphone_number') }}</strong>
            </span>
            @endif
        </div>
    </fieldset>
    <br>
    {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-primary"> <i class="fa fa-plus" ></i> Create Recipient </button>';
    }) !!}

    <div class="form-group">
        {!! Form::SubmitBtn() !!}
    </div>
{!! Form::close() !!}