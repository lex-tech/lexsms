 <div class="col-md-4">
     {{ Form::open(array('route' => 'reports.store', 'method' => 'POST')) }}

     <div class="col-md-8">
         <div class="form-group{{ $errors->has('summary') ? ' has-error' : '' }}">
             {!! Form::component('customSelect', 'components.select.select_sms_summary', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
             {!! Form::customSelect('summary', ['class="selectpicker form-control show-menu-arrow"',
                                                     'data-live-search="true"', 'data-header="Specific Summary"',
                                                      'showSubtext="true"', 'data-width="100%"', 'title="Report Summary"'], [], $summary)
              !!}
             @if ($errors->has('client'))
                 <span class="help-block">
                <strong>{{ $errors->first('client') }}</strong>
             </span>
             @endif

         </div>
     </div>

     <div class="col-md-2">
         <button type="submit" class="btn btn-primary pull-right" > <i class="fa fa-refresh"></i></button>
     </div>
     {{ Form::close() }}

     <div class="col-md-2 push-right">
         <button class="btn btn-primary" role="button" data-toggle="modal" data-target="#sms_report_filter">
             <i class="fa fa-filter"></i> Filter SMS Options
         </button>
     </div>
 </div>


@include('includes.modals.sms_report_filter')