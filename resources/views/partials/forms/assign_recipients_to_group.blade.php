{{ Form::open(array('route' => 'groups.store', 'method' => 'POST')) }}


<div class="form-group{{ $errors->has('receiver') ? ' has-error' : '' }}">
    {!! Form::label('recipients', 'Recipient:') !!}
    {{--create lists component--}}
    {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('recipients[ ]', ['class="selectpicker form-control show-menu-arrow"',
                            'data-live-search="true"', 'data-header="Select recipients"',
                             'showSubtext="true"', 'data-width="75%"', 'title="Choose your custom contacts"', 'multiple'], [],  $recipients)
     !!}
    @if ($errors->has('receiver'))
        <span class="help-block">
            <strong style="color: red" >{{ $errors->first('receiver') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    {!! Form::text('assign_recipients_to_group', null, array('hidden', 'value'=>'assign_recipients_to_group')) !!}
</div>

<div class="form-group{{ $errors->has('groups') ? ' has-error' : '' }}">
    {!! Form::label('group', 'Groups (Recipients):') !!}
    {{--create lists component--}}
    {!! Form::component('customSelect', 'components.select.select_groups', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('group', ['class="selectpicker form-control show-menu-arrow"',
                            'data-live-search="true"', 'data-header="Select Group"',
                             'showSubtext="true"', 'data-width="75%"', 'title="Assign to group"'], [],  $groups)
     !!}
    @if ($errors->has('groups'))
        <span class="help-block">
            <strong style="color: red" >{{ $errors->first('groups') }}</strong>
        </span>
    @endif
</div>


{!! Form::macro('SubmitBtn',function (){
       return '<button type="submit" class="btn btn-primary"> <i class="fa fa-address-book" ></i> Assign To Group </button>';
}) !!}

<div class="form-group">
    {!! Form::SubmitBtn() !!}
</div>

{{ Form::close() }}