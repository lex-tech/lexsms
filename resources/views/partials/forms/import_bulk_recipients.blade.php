<div class="panel panel-body">
    <div class="row" style="padding-bottom: 10px; margin-left: 3px;">
        <div class="alert alert-important panel panel-body panel-warning col-md-6 ">
            Make sure that your file uses the following headings <b class="text-danger">number</b> and <b
                    class="text-danger">name</b>, however the name is optional!
        </div>
    </div>
    <div class="panel panel-default panel-body">
        <div class="row">
            <class class="form-group">

                <div class="col-md-8">
                    <label for=""> Create <b>Group</b> if it does not exist already </label>
                    <button type="button" id="create_group" class="btn btn-default" data-toggle="modal"
                            data-target="#createGroup">Create group for recipients
                    </button>
                </div>
            </class>
        </div>
    </div>
    @include('includes.modals.create_group')

    <div class="panel panel-default panel-body">
        {!! Form::open(array('route'=>'recipients.store', 'method'=>'POST', 'enctype' => 'multipart/form-data'))  !!}
        {{--{{csrf_field()}}--}}

        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('groups') ? ' has-error' : '' }}">
                        {{--create lists component--}}
                        {!! Form::label('group', 'Add Recipients to group') !!}
                        {!! Form::component('customSelect', 'components.select.select_groups', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
                        {!! Form::customSelect('group', ['class="selectpicker form-control show-menu-arrow"',
                                                'data-live-search="true"', 'data-header="Select Groups"',
                                                 'showSubtext="true"', 'data-width="75%"', 'title="Select group"', 'required'], [],  $groups)
                         !!}
                        @if ($errors->has('groups'))
                            <span class="help-block">
                				<strong style="color: red">{{ $errors->first('groups') }}</strong>
            				</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        {!! Form::label('csv_file', 'Select a CSV Or Excel file to import') !!}
        <div class="form-group panel panel-body panel-default {{ $errors->has('csv_file') ? ' has-error' : '' }}">
            {!! Form::file('csv_file',null, array('class' => 'form-control', 'id'=>'csv_file')) !!}
            {{--<input name="csv_file" id="csv_file" type="file" class="" formenctype="multipart/form-data">--}}
            @if ($errors->has('csv_file'))
                <span class="help-block">
                    <strong>{{ $errors->first('csv_file') }}</strong>
                </span>
            @endif
        </div>

        {{-- used to track the form --}}
        {!! Form::text('contacts_upload_form', null, array('value' => 'contacts_upload_form', 'hidden') ) !!}

        <div class="form-group">
            {!! Form::macro('SubmitBtn',function (){
            return '<button type="submit" class="btn btn-primary"> <i class="fa fa-upload"></i> Upload </button>';
            }) !!}
            {!! Form::SubmitBtn() !!}
        </div>

        {{Form::close()}}
    </div>

</div>


