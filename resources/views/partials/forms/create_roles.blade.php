
{{ Form::open(array('url' => 'roles')) }}

<fieldset>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
        @endif
    </div>

    <h5><b>Assign Permissions To Role</b></h5>
    <div class='form-group{{ $errors->has('permissions') ? ' has-error' : '' }}'>
        @if ($errors->has('permissions'))
            <span class="help-block">
                        <strong>{{ $errors->first('permissions') }}</strong>
                    </span>
        @endif
        @foreach ($permissions as $permission)
            {{ Form::checkbox('permissions[]',  $permission->id ) }}
            {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
        @endforeach
    </div>
</fieldset>
<br>
{{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}