@include('flash::message')

{{--{{ Form::open(array('url' => 'memberships')) }}--}}
{!! Form::open(array('route' => 'memberships.store', 'method' => 'POST')) !!}

<div class="form-group{{ $errors->has('client') ? ' has-error' : '' }}">
    {!! Form::label('client', 'Client:') !!}
    {{--instantiate lists component--}}
    {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('client', ['class="selectpicker form-control show-menu-arrow"',
                            'data-live-search="true"', 'data-header="Select client"',
                             'showSubtext="true"', 'data-width="75%"', 'title="Choose your client"'], [],  $clients)
     !!}
    @if ($errors->has('client'))
        <span class="help-block">
            <strong >{{ $errors->first('client') }}</strong>
        </span>
    @endif
</div>
<br>

<h4>Assign Membership(s) to a Client</h4>

@if ($errors->has('memberships'))
    <span class="help-block">
                    <strong>{{ $errors->first('memberships') }}</strong>
                </span>
@endif
@foreach ($memberships as $membership)
    {{ Form::checkbox('memberships[]',  $membership->id ) }}
    {{ Form::label($membership->name, ucfirst($membership->name)) }}<br>

@endforeach

<br>
<div class="form-group">
    {!! Form::text('membership-form',null,array('hidden', 'value' => 'membership-form')) !!}
</div>
{!! Form::macro('SubmitBtn',function (){
    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Create Client Membership </button>';
}) !!}
{!! Form::SubmitBtn() !!}

{{ Form::close() }}