<div class="col-md-8">
    {{ Form::model($membership, array('route' => array('memberships.update', $membership->id), 'method' => 'PUT')) }}
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name',null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('keyword', 'Keyword:') !!}
        {!! Form::text('keyword',null, array('class' => 'form-control', 'required')) !!}
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                {!! Form::label('start_date', 'Starting Date:') !!}
                {{--create datetimepicker component--}}
                {!! Form::component('dateTimePicker', 'components.timepicker.timepicker', ['name', 'id', 'attributes']) !!}
                {!! Form::dateTimePicker('start_date', 'start-date-time-picker' , [])
                 !!}
                @if ($errors->has('start_date'))
                    <span class="help-block">
                                <strong>{{ $errors->first('start_date') }}</strong>
                            </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            {{--Validate so that end date is after the selected start date--}}
            <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                {!! Form::label('end_date', 'Ending Date:') !!}
                {{--instantiate datetimepicker component--}}
                {!! Form::dateTimePicker('end_date', 'end-date-time-picker' , [])
                 !!}
                @if ($errors->has('end_date'))
                    <span class="help-block">
                                <strong>{{ $errors->first('end_date') }}</strong>
                            </span>
                @endif
            </div>
        </div>
    </div>
    {!! Form::macro('SubmitBtn',function (){
                 return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit" ></i> Update Membership </button>';
     }) !!}

    <div class="form-group">
        {!! Form::SubmitBtn() !!}
    </div>

    {{ Form::close() }}
</div>