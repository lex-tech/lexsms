{!! Form::open(array('route' => 'credits.store', 'method' => 'POST')) !!}
<div class="form-group{{ $errors->has('credit') ? ' has-error' : '' }}">
    {!! Form::label('credit', 'Credit:') !!}
    {{--create lists component--}}
    {!! Form::text('credit', null , array('class'=> 'form-control')) !!}

    @if ($errors->has('credit'))
        <span class="help-block">
            <strong >{{ $errors->first('credit') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    {!! Form::text('credit-request-form',null,array('hidden', 'value' => 'credit-request-form')) !!}
</div>

<div class="form-group">
    {!! Form::macro('SubmitBtn',function (){
               return '<button type="submit" class="btn btn-primary"> <i class="fa fa-exchange" ></i> Request SMS Credit </button>';
    }) !!}

    <div class="form-group">
        {!! Form::SubmitBtn() !!}
    </div>
</div>
{{Form::close()}}