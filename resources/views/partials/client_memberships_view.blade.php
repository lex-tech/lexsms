
@include('flash::message')
{{--{{ Form::open(array('route' => 'memberships.show', 'client')) }}--}}
<div class="form-group{{ $errors->has('client') ? ' has-error' : '' }}">
    {!! Form::label('client', 'Client:') !!}
    {{--instantiate lists component--}}
    {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
    {!! Form::customSelect('client', ['class="selectpicker form-control show-menu-arrow"',
                            'data-live-search="true"', 'data-header="Select client"',
                             'showSubtext="true"', 'data-width="75%"', 'title="Choose your client"'], [], $clients)
     !!}
    @if ($errors->has('client'))
        <span class="help-block">
            <strong >{{ $errors->first('client') }}</strong>
        </span>
    @endif


    {{--<div class="form-group">--}}
        {{--{!! Form::SubmitBtn() !!}--}}
    {{--</div>--}}

    {{ Form::close() }}

</div>
<br>

@component('components.tables.table')
    @slot('title')
        Memberships
    @endslot

@slot('thead')
    <th>Name</th>
    <th>Starting Date</th>
    <th>Ending Date</th>
    <th>Keyword</th>
@endslot

@slot('tfoot')
    <th>Name</th>
    <th>Starting Date</th>
    <th>Ending Date</th>
    <th>Keyword</th>
@endslot


@slot('tbody')
    @foreach($memberships as $membership)
        <tr>
            <td>{{ $membership->name  }}</td>
            <td>{{ $membership->start_date }}</td>
            <td>{{ $membership->end_date }}</td>
            <td>{{ $membership->keyword }}</td>
            {{--<td>    <a href="{{ route('memberships.edit', $membership->id) }}" class="btn btn-default pull-left" data-toggle="tooltip" rel="tooltip" title="Edit" style="margin-right: 3px;">--}}
                    {{--<i class="fa fa-edit"></i> </a>--}}
                {{--{!! Form::open(['method' => 'DELETE', 'route' => ['memberships.destroy', $membership->id], 'id' => 'delete-form', 'onsubmit' => 'return ConfirmDelete()' ]) !!}--}}
                {{--{!! Form::macro('SubmitBtn',function (){--}}
                    {{--return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" data-toggle="tooltip" rel="tooltip" title="Delete" style="color: red;"></i> </button>';--}}
                {{--}) !!}--}}
                {{--{!! Form::SubmitBtn() !!}--}}
                {{--{!! Form::close() !!}--}}
            {{--</td>--}}
        </tr>
    @endforeach

@endslot

@slot('card_footer')
    Updated yesterday at 11:59 PM
@endslot

@endcomponent


{{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">--}}
    {{--$('#client').change(function(e)--}}
    {{--{--}}
        {{--e.preventDefault();--}}

        {{--$y = $(this).val();--}}
        {{--alert($y);--}}

        {{--$.ajax--}}
        {{--({--}}
            {{--url: '{{ url('memberships') }}/'+$y,--}}
            {{--type: 'GET',--}}
            {{--dataType: 'html',--}}
            {{--success: function(data)--}}
            {{--{--}}
                {{--console.log(data);--}}
            {{--}--}}
        {{--});--}}
    {{--});--}}

{{--</script>--}}