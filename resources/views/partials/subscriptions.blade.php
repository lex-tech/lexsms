
@component('components.tables.table')
    @slot('title')
    Subscriptions
    @endslot

{{--    <a href="{{ route('subscriptions.create') }}" class="btn btn-success">Add Subscriptions</a>--}}

    @slot('thead')
        <th>Name</th>
        <th>Durations (Days)</th>
        <th>Number of SMSs</th>
        <th>Operations</th>
    @endslot

    @slot('tfoot')
        <th>Name</th>
        <th>Durations (Days)</th>
        <th>Number of SMSs</th>
        <th>Operations</th>
    @endslot

    @slot('tbody')
        @foreach($subscriptions as $subscription)
            <tr>
                <td>{{ $subscription->name  }}</td>
                <td>{{ $subscription->duration }}</td>
                <td>{{ $subscription->sms_number }}</td>
                <td>    <a href="{{ route('subscriptions.edit', $subscription->id) }}" class="btn btn-default pull-left" style="margin-right: 3px;">
                        <i class="fa fa-edit"></i> </a>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['subscriptions.destroy', $subscription->id], 'id' => 'delete-form' ]) !!}
                    {!! Form::macro('SubmitBtn',function (){
                        return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';
                    }) !!}
                    {!! Form::SubmitBtn() !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

    @endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent