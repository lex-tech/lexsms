{!! Form::open(array('route' => 'subscriptions.store', 'method' => 'POST')) !!}

<fieldset>
    {{--<legend></legend>--}}

    <div class="form-group{{ $errors->has('client') ? ' has-error' : '' }}">
        {!! Form::label('client', 'Client:') !!}
        {{--instantiate lists component--}}
        {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
        {!! Form::customSelect('client', ['class="selectpicker form-control show-menu-arrow"',
                                'data-live-search="true"', 'data-header="Select client"',
                                 'showSubtext="true"', 'data-width="75%"', 'title="Choose your client"'], [],  $clients)
         !!}
        @if ($errors->has('client'))
            <span class="help-block">
            <strong >{{ $errors->first('client') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('subscription') ? ' has-error' : '' }}">
        {!! Form::label('subscription', 'Subscription:') !!}
        {{--instantiate lists component--}}
        {!! Form::component('customSelect', 'components.select.select_subscriptions', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
        {!! Form::customSelect('subscription', ['class="selectpicker form-control show-menu-arrow"',
                                'data-live-search="true"', 'data-header="Select subscription"',
                                 'showSubtext="true"', 'data-width="75%"', 'title="Choose subscription"'], [],  $subscriptions)
         !!}
        @if ($errors->has('subscription'))
            <span class="help-block">
            <strong >{{ $errors->first('subscription') }}</strong>
        </span>
        @endif
    </div>

</fieldset>

<div class="form-group">
    {!! Form::text('subscription-form',null,array('hidden', 'value' => 'subscription-form')) !!}
</div>
<div class="form-group">
    {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-primary"> <i class="fa fa-get-pocket" ></i> Subscribe Client </button>';
    }) !!}

    <div class="form-group">
        {!! Form::SubmitBtn() !!}
    </div>
</div>
{{Form::close()}}