@component('components.tables.table')

@slot('title')
Roles
@endslot

{{--<a href="{{ URL::to('roles/create') }}" class="btn btn-success">Add Role</a>--}}

@slot('thead')
<th>Role</th>
<th>Permissions</th>
<th>Operation</th>
@endslot

@slot('tfoot')
<th>Role</th>
<th>Permissions</th>
<th>Operation</th>
@endslot

@slot('tbody')
@foreach ($roles as $role)
    <tr>

        <td>{{ $role->name }}</td>

        <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
        <td>
            <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-default pull-left"
               style="margin-right: 3px;"> <i class="fa fa-edit"></i> </a>

            {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
            {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';
            }) !!}
            {!! Form::SubmitBtn() !!}
            {{--            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
            {!! Form::close() !!}

        </td>
    </tr>
@endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot

@endcomponent