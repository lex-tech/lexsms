@component('components.tables.table')
@slot('title')
Testing
@endslot

{{--<a href="{{ route('model.create') }}" class="btn btn-success">Add Model</a>--}}

@slot('thead')
<th>#</th>
<th>Credit Reallocated By</th>
<th>Reallocated From</th>
<th>Reallocated To</th>
<th>Reallocated Credit</th>
<th>Time</th>


@endslot

@slot('tfoot')
<th>#</th>
<th>Credit Reallocated By</th>
<th>Reallocated From</th>
<th>Reallocated To</th>
<th>Reallocated Credit</th>
<th>Time</th>

@endslot

@slot('tbody')
    @foreach($credit_reallocation_transactions as $credit_reallocation_transaction)
        <tr>
            <td>{{ $credit_reallocation_transaction->id }}</td>
            <td>{{ $credit_reallocation_transaction->transaction_owner->name }}</td>
            <td>{{ $credit_reallocation_transaction->credit_reallocated_from->name }}</td>
            <td>{{ $credit_reallocation_transaction->credit_reallocated_to->name }}</td>
            <td>{{ $credit_reallocation_transaction->credit }}</td>
            <td>{{ $credit_reallocation_transaction->created_at }}</td>
        </tr>
    @endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent
