@component('components.tables.table')
@slot('title')
    Users
@endslot
{{--<a href="{{ route('users.create') }}" class="btn btn-success">Add User</a>--}}

@slot('thead')
{{--<tr>--}}
    <th>Name</th>
    <th>Email</th>
    <th>Date/Time Added</th>
    <th>User Roles</th>
    <th>Cellphone</th>
    <th>SMS Credit</th>
    <th>Operations</th>
{{--</tr>--}}
@endslot

@slot('tfoot')
{{--<tr>--}}
    <th>Name</th>
    <th>Email</th>
    <th>Date/Time Added</th>
    <th>User Roles</th>
    <th>Cellphone</th>
    <th>SMS Credit</th>
    <th>Operations</th>
{{--</tr>--}}
@endslot

@slot('tbody')
@foreach ($users as $user)
    <tr>

        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
        {{--Retrieve array of roles associated to a user and convert to string--}}
        <td>{{  $user->roles()->pluck('name')->implode(' ') }}</td>
        <td>{{ $user->cellphone_number }}</td>
        <td>{{  number_format($user->sms_credit, 2) }}</td>
        <td>
            <a href="{{ route('users.edit', \Illuminate\Support\Facades\Crypt::encrypt($user->id)) }}" class="btn btn-default pull-left" style="margin-right: 3px;">
                <i class="fa fa-edit"></i> </a>

            {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id], 'id' => 'delete-form' ]) !!}
            {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';
            }) !!}
            {!! Form::SubmitBtn() !!}
            {!! Form::close() !!}

        </td>
    </tr>
@endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent