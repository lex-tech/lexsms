@component('components.tables.table')

@slot('title')
Permissions
@endslot
{{--<a href="{{ URL::to('permissions/create') }}" class="btn btn-success">Add Permission</a>--}}

@slot('thead')
<th>#</th>
<th>Permissions</th>
<th>Operation</th>
@endslot

@slot('tfoot')
<th>#</th>
<th>Permissions</th>
<th>Operation</th>
@endslot

@slot('tbody')
@foreach ($permissions as $permission)
    <tr>
        <td>{{ $permission->id }}</td>
        <td>{{ $permission->name }}</td>
        <td>
            <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}"
               class="btn btn-default pull-left" style="margin-right: 3px;"> <i class="fa fa-edit"></i> </a>

            {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
            {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';
            }) !!}
            {!! Form::SubmitBtn() !!}

            {!! Form::close() !!}

        </td>
    </tr>
@endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot

@endcomponent