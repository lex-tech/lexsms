@component('components.tables.table')
@slot('title')
Contacts
@endslot

{{--<a href="{{ route('recipients.index') }}" class="btn btn-success">Add Contact</a>--}}

@slot('thead')
<th>#</th>
<th>Name</th>
<th>Cellphone Number</th>
<th>Operations</th>

@endslot

@slot('tfoot')
<th>#</th>
<th>Name</th>
<th>Cellphone Number</th>
<th>Operations</th>
@endslot

@slot('tbody')
@foreach($recipients as $recipient)
    <tr>
        <td>{{ $recipient->id }}</td>
        <td>{{ $recipient->name }}</td>
        <td> {{ $recipient->cellphone_number }} </td>
        <td>
            <a href="{{ route('recipients.edit', $recipient->id) }}" class="btn btn-default pull-left" style="margin-right: 3px;">
                <i class="fa fa-edit"></i> </a>

            {!! Form::open(['method' => 'DELETE', 'route' => ['recipients.destroy', $recipient->id], 'id' => 'delete-form' ]) !!}
            {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';
            }) !!}
            {!! Form::SubmitBtn() !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent
