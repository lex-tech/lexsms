@component('components.tables.table')
@slot('title')
    Bills
@endslot
{{--<a href="{{ route('users.create') }}" class="btn btn-success">Add User</a>--}}

@slot('thead')
{{--<tr>--}}
    <th>#</th>
    <th>Client</th>
    <th>Month</th>
    <th>Amount</th>
    <th>Payment Status</th>
    <th>Payment Date</th>
    <th>Operations</th>
{{--</tr>--}}
@endslot

@slot('tfoot')
{{--<tr>--}}
    <th>#</th>
    <th>Client</th>
    <th>Month</th>
    <th>Amount</th>
    <th>Payment Status</th>
    <th>Payment Date</th>
    <th>Operations</th>
{{--</tr>--}}
@endslot

@slot('tbody')
@foreach ($bills as $bill)
    <tr>
        <td>{{ $bill->id }}</td>
        <td>{{ $bill->client->name }}</td>
        <td>{{ $bill->subscription_month }}</td>
        <td>N$ {{ number_format($bill->amount, 2) }}</td>
        <td>
            @if($bill->payment_status)
                <i style="color: green" class="fa fa-check"></i>
            @else
                <i style="color: red" class="fa fa-times"></i>
            @endif
        </td>
        <td>
            @if($bill->payment_status)
                {{ $bill->payment_date }}
            @else
                {{'pending'}}
            @endif
        </td>
        <td>
            @can('Update Billings')
                <a href="{{ route('billing.edit', $bill->id) }}" class="btn btn-default pull-left" style="margin-right: 3px;">
                    <i class="fa fa-edit"></i> </a>
            @endcan

            <a href="{{ route('billing.show', $bill->id) }}" class="btn btn-outline-info pull-left">
                <i class="fa fa-binoculars"></i> </a>


                @if($bill->invoice)
                    <a href="{{ route('invoices.show', $bill->invoice->id) }}" class="btn btn-outline-info pull-left">
                        <i class="fa fa-file-text"></i> </a>
                @endif

            {{--{!! Form::open(['method' => 'DELETE', 'route' => ['bills.destroy', $bill->id], 'id' => 'delete-form' ]) !!}--}}
            {{--{!! Form::macro('SubmitBtn',function (){--}}
                {{--return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';--}}
            {{--}) !!}--}}
            {{--{!! Form::SubmitBtn() !!}--}}
            {{--{!! Form::close() !!}--}}

        </td>
    </tr>
@endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent