@component('components.tables.table')

@slot('title')
Recipients in Group: {{ $group->name }}
@endslot
{!! Form::model($group, array('route' => array('groups.update', $group->id), 'method' => 'PUT')) !!}
<button class="btn btn-danger" type="submit"><i class="fa fa-remove"></i> Remove Selected Recipient(s) From Group
</button>
@slot('thead')

<th style="width: 100px"><input type="checkbox" class="checkAll"> <span style="margin-left: 5px">Select All</span></th>
<th>Name</th>
<th>Number</th>

@endslot

@slot('tfoot')

<th><input type="checkbox" class="checkAll"> <span style="margin-left: 5px">Select All</span></th>
<th>Name</th>
<th>Number</th>

@endslot

@slot('tbody')
{!! Form::text('recipients-in-group',null, array('value' => 'recipients-in-group', 'hidden' )) !!}

<div class="{{ $errors->has('recipients') ? ' has-error' : '' }}">
    @if ($errors->has('recipients'))
        <span class="help-block">
                        <strong>{{ $errors->first('recipients') }}</strong>
                    </span>
    @endif
    @foreach ($recipients as $recipient)
        <tr>
            <td>
                <div>
                    <input type="checkbox" name="recipients[]" id="recipient-{{$recipient->id}}"
                           value="{{$recipient->id}}">
                </div>
            </td>
            <td>{{ $recipient->name }}</td>
            <td>{{ $recipient->cellphone_number }}</td>
        </tr>
    @endforeach
</div>
{!! Form::close() !!}
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot

@endcomponent