@component('components.tables.table')
@slot('title')
Groups
@endslot

{{--<a href="{{ route('groups.create') }}" class="btn btn-success">Add Group</a>--}}

@slot('thead')

<th>#</th>
<th>Name</th>
<th>Description</th>
<th>Recipients</th>
<th>Operations</th>
@endslot

@slot('tfoot')
<th>#</th>
<th>Name</th>
<th>Description</th>
<th>Recipients</th>
<th>Operations</th>
@endslot

@slot('tbody')
    @foreach($groups as $group)

        <tr>
            <td>{{$group->id}}</td>
            <td>{{$group->name}}</td>
            <td class="read-more-target">{{$group->description}}</td>
            <td>{{ $group->recipients->count() }}</td>
            <td>
                @if($group->creator->id == \Illuminate\Support\Facades\Auth::id())
                    <a href="{{ route('groups.edit', $group->id) }}" class="btn btn-default pull-left" style="margin-right: 3px;">
                    <i class="fa fa-edit"></i> </a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['groups.destroy', $group->id], 'id' => 'delete-form' ]) !!}
                    {!! Form::macro('SubmitBtn',function (){
                        return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';
                    }) !!}
                    {!! Form::SubmitBtn() !!}
                @endif
                {!! Form::close() !!}

            </td>
        </tr>
        @endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent
