@component('components.tables.table')
@slot('title')
SMS Categories
@endslot

{{--<a href="{{ route('recipients.index') }}" class="btn btn-success">Add Contact</a>--}}

@slot('thead')
<th>#</th>
<th>Name</th>
<th>Description</th>
<th>Operations</th>

@endslot

@slot('tfoot')
<th>#</th>
<th>Name</th>
<th>Description</th>
<th>Operations</th>
@endslot

@slot('tbody')
@foreach($categories as $category)
    <tr>
        <td>{{ $category->id }}</td>
        <td>{{ $category->name }}</td>
        <td>{{ $category->description }}</td>
        <td>
            <a href="{{ route('sms-categories.edit', $category->id) }}" class="btn btn-default pull-left" style="margin-right: 3px;">
                <i class="fa fa-edit"></i> </a>

            {!! Form::open(['method' => 'DELETE', 'route' => ['sms-categories.destroy', $category->id], 'id' => 'delete-form' ]) !!}
            {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';
            }) !!}
            {!! Form::SubmitBtn() !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent
