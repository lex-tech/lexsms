@component('components.tables.table')
@slot('title')
Credit Requests
@endslot

{{--<a href="{{ route('groups.create') }}" class="btn btn-success">Add Group</a>--}}

@slot('thead')

<th>#</th>
<th>Requested by</th>
<th>Credit Requested</th>
<th>Credit Has Been Allocated</th>
<th>Time</th>
<th>Operations</th>
@endslot

@slot('tfoot')
<th>#</th>
<th>Requested by</th>
<th>Credit Requested</th>
<th>Credit Has Been Allocated</th>
<th>Time</th>
<th>Operations</th>
@endslot

@slot('tbody')
@foreach($credit_requests as $credit_request)

    <tr>
        <td>{{$credit_request->id}}</td>
        <td>{{$credit_request->request_made_by->name}}</td>
        <td class="read-more-target">{{$credit_request->credit}}</td>
        <td class="text-center">@if($credit_request->noted)
                <i style="color: green" class="fa fa-check"></i>
            @else
                <i style="color: red" class="fa fa-times"></i>
            @endif
        </td>
        <td>{{ $credit_request->created_at }}</td>
        <td>
            @if(($credit_request->noted == 0 && $credit_request->request_made_by->user_created_by->id == \Illuminate\Support\Facades\Auth::id()))
                {!! Form::open(['method' => 'PUT', 'route' => ['credits.update', $credit_request->id], 'id' => 'delete-form' ]) !!}
                    {!! Form::text('credit', $credit_request->credit, ['hidden'] ) !!}
                    {!! Form::text('client', $credit_request->requested_by, ['hidden'] ) !!}
                    {!! Form::text('credit_request_id', $credit_request->id, ['hidden'] ) !!}
                    {!! Form::macro('SubmitBtn',function (){
                        return '<button type="submit" class="btn btn-primary"> <i class="fa fa-check"></i> Allocate Credit </button>';
                    }) !!}
                    {!! Form::SubmitBtn() !!}
                {!! Form::close() !!}
            @endif
        </td>
    </tr>
@endforeach
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent
