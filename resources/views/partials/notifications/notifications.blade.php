<?php
//    show all the unresolved credit requests
    $creditRequests = \App\CreditRequest::all()->where('noted', 0)->filter(function (\App\CreditRequest $credit_Request){
        return $credit_Request->request_made_by->user_created_by->id == \Illuminate\Support\Facades\Auth::id();
    });
?>
{{--<li class="nav-item dropdown">--}}
    {{--<a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown"--}}
       {{--aria-haspopup="true" aria-expanded="false">--}}
        {{--<i class="fa fa-fw fa-envelope"></i>--}}
        {{--<span class="d-lg-none">--}}
                    {{--Messages--}}
                    {{--<span class="badge badge-pill badge-primary">{{ count($creditRequests) }} New</span>--}}
                {{--</span>--}}
        {{--<span class="indicator text-primary d-none d-lg-block"><i class=""></i></span>--}}
    {{--</a>--}}
    {{--<div class="dropdown-menu" aria-labelledby="messagesDropdown">--}}
        {{--<h6 class="dropdown-header">New Messages:</h6>--}}
        {{--<div class="dropdown-divider"></div>--}}
        {{--@foreach($creditRequests as $creditRequest)--}}
            {{--<a class="dropdown-item" href="#">--}}
                {{--<strong>{{ $creditRequest->request_made_by->name }}</strong>--}}
                {{--<span class="small float-right text-muted">{{ $creditRequest->created_at  }}</span>--}}
                {{--<div class="dropdown-message small">{{ $creditRequest->request_made_by->name . ' has made a credit request of ' . $creditRequest->credit . ' SMS ' }}--}}
                {{--</div>--}}
            {{--</a>--}}
            {{--<div class="dropdown-divider"></div>--}}
        {{--@endforeach--}}
        {{--<a class="dropdown-item small" href="#">View all messages</a></div>--}}
{{--</li>--}}
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-fw fa-bell"></i>
        {{--<span class="d-lg-none">--}}
            {{--Alerts--}}
            {{--<span class="badge badge-pill badge-warning">6 New--}}
            {{--</span>--}}
        {{--</span>--}}
        <span class="indicator text-warning d-none d-lg-block">
            <i class="{{ count($creditRequests) > 0? 'fa fa-fw fa-circle': '' }}"></i></span></a>
    <div class="dropdown-menu" aria-labelledby="alertsDropdown">
        <h6 class="dropdown-header">{{ count($creditRequests)? 'New Requests:': 'No New Credit Requests' }}</h6>
       @foreach($creditRequests as $creditRequest )
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#"><span class="text-success"><strong><i
                                class="fa fa-long-arrow-up fa-fw"></i>Credit Request</strong></span><span
                        class="small float-right text-muted">{{ $creditRequest->created_at }}</span>
                <div class="dropdown-message small"> <b> {{ $creditRequest->request_made_by->name }} </b> {{ ' has made a credit request of ' . $creditRequest->credit . ' SMS ' }}
                </div>
            </a>
            <div class="dropdown-divider"></div>
       @endforeach
        {{--<a class="dropdown-item" href="#"><span class="text-danger"><strong><i--}}
                            {{--class="fa fa-long-arrow-down fa-fw"></i>Status Update</strong></span><span--}}
                    {{--class="small float-right text-muted">11:21 AM</span>--}}
            {{--<div class="dropdown-message small">This is an automated server response message. All systems--}}
                {{--are online.--}}
            {{--</div>--}}
        {{--</a>--}}
        {{--<div class="dropdown-divider"></div>--}}
        <a class="dropdown-item small" href="{{ route('credits.index'), session()->now('tab', 'view_credit_requests') }}">{{ count($creditRequests)? 'View All Requests:': '' }}</a>
    </div>
</li>