<div class="col-md-8">
    {{--{!! Form::open(array('route' => 'memberships.store', 'method' => 'POST')) !!}--}}
    {!! Form::open(array('route'=>'memberships.store', 'method'=>'POST', 'enctype' => 'multipart/form-data'))  !!}
    <div class="form-group{{ $errors->has('membership') ? ' has-error' : '' }}">
        {!! Form::label('membership', 'Membership:') !!}
        {{--instantiate lists component--}}
        {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
        {!! Form::customSelect('membership', ['class="selectpicker form-control show-menu-arrow"',
                                'data-live-search="true"', 'data-header="Select membership"',
                                 'showSubtext="true"', 'data-width="75%"', 'title="Choose your membership"'], [], $memberships)
         !!}
        @if ($errors->has('membership'))
            <span class="help-block">
            <strong >{{ $errors->first('membership') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group{{ $errors->has('group') ? ' has-error' : '' }}">
        {!! Form::label('group', 'Group:') !!}
        {!! Form::component('customSelect', 'components.select.select_groups', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
        {!! Form::customSelect('group', ['class="selectpicker form-control show-menu-arrow"',
                                'data-live-search="true"', 'data-header="Select group"',
                                 'showSubtext="true"', 'data-width="75%"', 'title="Choose your group"'], [], $groups)
         !!}
        @if ($errors->has('group'))
            <span class="help-block">
            <strong >{{ $errors->first('group') }}</strong>
        </span>
        @endif
    </div>

    {!! Form::label('csv_file', 'Select a CSV Or Excel file with messages to import') !!}
    <div class="form-group panel panel-body panel-default {{ $errors->has('csv_file') ? ' has-error' : '' }}">
        {!! Form::file('csv_file',null, array('class' => 'form-control', 'id'=>'csv_file')) !!}
        {{--<input name="csv_file" id="csv_file" type="file" class="" formenctype="multipart/form-data">--}}
        @if ($errors->has('csv_file'))
            <span class="help-block">
                    <strong>{{ $errors->first('csv_file') }}</strong>
                </span>
        @endif
    </div>

    {{-- used to track the form --}}
    {!! Form::text('messages_upload_form', null, array('value' => 'messages_upload_form', 'hidden') ) !!}

    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
        {{ Form::label('message', 'Message') }}
        {{ Form::textarea('message', null, array('class' => 'form-control')) }}
        @if ($errors->has('message'))
            <span class="help-block">
            <strong>{{ $errors->first('message') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
        {{ Form::label('time', 'Recurring Time:') }}
        {!! Form::component('dateTimePicker', 'components.timepicker.timepicker', ['name', 'id', 'attributes']) !!}
        {!! Form::dateTimePicker('time', 'time-time-picker' , []) !!}
        @if ($errors->has('time'))
            <span class="help-block">
                <strong>{{ $errors->first('time') }}</strong>
            </span>
        @endif
    </div>

    {{--<div class="form-group{{ $errors->has('day') ? ' has-error' : '' }}">--}}
        {{--{!! Form::label('day', 'Select days of the week:') !!}--}}
        {{--{!! Form::select('day[]', ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] )!!}--}}
        {{--{!! Form::customSelect('day[]', ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] )!!}--}}
        {{--@if ($errors->has('day'))--}}
            {{--<span class="help-block">--}}
            {{--<strong style="color: red" >{{ $errors->first('day') }}</strong>--}}
        {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}


    {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-primary"> <i class="fa fa-plus" ></i> Create Membership Messaging </button>';
    }) !!}

    <div class="form-group">
        {!! Form::SubmitBtn() !!}
    </div>
    {!! Form::close() !!}

</div>


