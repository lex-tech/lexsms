@component('components.tables.table')
    @slot('title')
        Credit Transactions
    @endslot

    @slot('thead')
        <th>#</th>
        <th>Transactor</th>
        <th>Credit</th>
        <th>Client</th>
        <th>Time</th>

    @endslot

    @slot('tfoot')
        <th>#</th>
        <th>Transactor</th>
        <th>Credit</th>
        <th>Client</th>
        <th>Time</th>

    @endslot
    {{--{{dd(var_dump($transactions->toJson()))}}--}}
    @slot('tbody')
        @foreach($transactions as $transaction)
            <tr>
                <td>{{$transaction->id}}</td>
                <td>{{$transaction->transactor->name}}</td>
                <td>{{$transaction->credit}}</td>
                <td>{{$transaction->client->name}}</td>
                <td>{{$transaction->created_at}}</td>
            </tr>
            @endforeach
    @endslot

    @slot('card_footer')
        Updated yesterday at 11:59 PM
    @endslot
@endcomponent
