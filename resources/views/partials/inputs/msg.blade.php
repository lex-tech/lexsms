<div class="form-group{{ $errors->has('msg') ? ' has-error' : '' }}">
    {!! Form::label('msg', 'Message:') !!}
    {!! Form::textarea('msg',null, array('class' => 'form-control', 'rows' => 5)) !!}
    @if ($errors->has('msg'))
        <span class="help-block">
            <strong >{{ $errors->first('msg') }}</strong>
        </span>
    @endif
</div>