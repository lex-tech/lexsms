<div class="panel panel-default panel-body">
   <div class="row">
       <div class="col-md-3 text-center" >
           <div class="card mb-3  panel panel-primary btn-outline-info">
               <div class="card-header">SMS Sent Statistics For This Month</div>
               <div class="card-body">
                   {{--<h1>{{ $total_messages_for_the_month }} <i class="fa fa-arrow-up"></i></h1>--}}
               </div>
               {{--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>--}}
           </div>
       </div>
   </div>
    <div class="row">
        <div class="col-md-8" >
            <div class="card mb-3 panel panel-primary btn-outline-info">
                <div class="card-header">Current Month Stats</div>
                <div class="card-body">
                    {{--{!! $monthly_stats_chart->html() !!}--}}
                </div>
                {{--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>--}}
            </div>
        </div>
    </div>
</div>