<div class="panel panel-default panel-body">
        <div class="row">
            <div class="col-md-12" >
                <div class="col-md-6">
                    <div class="card mb-3 panel panel-primary btn-outline-info text-center">
                        <div class="card-header">
                            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('Admin'))
                                Total Messages Sent By Users This Month
                            @else
                                SMS Sent This Month
                            @endif
                        </div>
                        <div class="card-body">
                            <h1>{{ $total_messages_for_the_month }} <i class="fa fa-arrow-up"></i></h1>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 pull-right">
                    @if(\Illuminate\Support\Facades\Auth::user()->hasrole('Admin'))
                        {{-- Nothing should be displayed --}}
                    @else
                        <div class="card mb-3 panel panel-primary btn-outline-info text-center">
                            <div class="card-header">Available SMS Credit</div>
                            <div class="card-body">
                                <span> <h1><i class="fa fa-dollar"></i>  {{   round(\Illuminate\Support\Facades\Auth::user()->sms_credit, 2)}} </h1> </span>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12" >
                <div class="card mb-3 panel panel-primary">
                    <div class="card-header text-center">Monthly Stats</div>
                    <div class="card-body">
                        {!! $monthly_stats_chart->html() !!}
                    </div>
                </div>
            </div>
        </div>
</div>