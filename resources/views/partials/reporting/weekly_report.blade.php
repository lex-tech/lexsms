<div class="panel panel-default panel-body">
    <div class="col-md-4" >
        <div class="card mb-3 panel panel-primary btn-outline-info">
            <div class="card-header">SMS Sent Statistics For This Week</div>
            <div class="card-body">
                {{--<h1>{{ $total_messages_for_the_week }} <i class="fa fa-arrow-up"></i></h1>--}}
            </div>
            {{--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>--}}
        </div>
        {{--<div class="card mb-3">--}}
        {{--<div class="card-header">Remaining SMS Credit</div>--}}
        {{--<div class="card-body">--}}
        {{--<span> <h1><i class="fa fa-dollar"></i>  {{   round(\Illuminate\Support\Facades\Auth::user()->sms_credit, 2)}} </h1> </span>--}}
        {{--</div>--}}
        {{--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>--}}
        {{--</div>--}}
    </div>
    <div class="col-md-8" >
        <div class="card mb-3 panel panel-primary btn-outline-info">
            <div class="card-header">Current Week Stats</div>
            <div class="card-body">
                {{--{!! $weekly_stats_chart->html() !!}--}}
            </div>
            {{--<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>--}}
        </div>
    </div>
</div>