<?php $total = 0 ?>
@foreach ($bills as $bill)
    <tr>
        <td>{{ $bill->id }}</td>
        <td>{{ $bill->subscription_month }}</td>
        <td>{{ $bill->client->name }}</td>
        <td>N$ {{ number_format($bill->amount, 2) }}</td>
    <?php $total += $bill->amount ?>


    {{--{!! Form::open(['method' => 'DELETE', 'route' => ['bills.destroy', $bill->id], 'id' => 'delete-form' ]) !!}--}}
    {{--{!! Form::macro('SubmitBtn',function (){--}}
    {{--return '<button type="submit" class="btn btn-default"> <i class="fa fa-remove" style="color: red;"></i> </button>';--}}
    {{--}) !!}--}}
    {{--{!! Form::SubmitBtn() !!}--}}
    {{--{!! Form::close() !!}--}}

    {{--</td>--}}
    {{--</tr>--}}
@endforeach
<tr>
    <td></td>
    <td></td>
    <td>Total</td>
    <td><strong>N$ {{ number_format($total, 2) }}</strong></td>
</tr>