 @foreach ($yearlyMessages as $yearlyMessage)
        <tr>
            <td>{{ $yearlyMessage->id }}</td>
            <td>{{ $yearlyMessage->year}}</td>
            <td>{{ $yearlyMessage->sender->name }}</td>
            <td>{{ $yearlyMessage->total_messages_for_the_year }}</td>
            {{--<td>N$ {{ number_format($yearlyMessage->amount, 2) }}</td>--}}
            {{--<td>--}}
            {{--@if($yearlyMessage->payment_status)--}}
            {{--<i style="color: green" class="fa fa-check"></i>--}}
            {{--@else--}}
            {{--<i style="color: red" class="fa fa-times"></i>--}}
            {{--@endif--}}
            {{--</td>--}}
            {{--<td>--}}
            {{--@if($bill->payment_status)--}}
            {{--{{ $bill->payment_date }}--}}
            {{--@else--}}
            {{--{{'pending'}}--}}
            {{--@endif--}}
            {{--</td>--}}
            <td>
                {{--@can('Update Billings')--}}
                {{--<a href="{{ route('billing.edit', $bill->id) }}" class="btn btn-default pull-left" style="margin-right: 3px;">--}}
                {{--<i class="fa fa-edit"></i> </a>--}}
                {{--@endcan--}}

                {{--<a href="{{ route('billing.show', $bill->id) }}" class="btn btn-outline-info pull-left">--}}
                {{--<i class="fa fa-binoculars"></i> </a>--}}

                {!! Form::open(['method' => 'POST', 'route' => ['reports.store'] ]) !!}
                    {!! Form::text('expand-year',$yearlyMessage->sender->id,array('hidden')) !!}
                    {!! Form::text('year',$yearlyMessage->id,array('hidden')) !!}
                    {!! Form::macro('SubmitBtn',function (){
                        return '<button type="submit" class="btn btn-default"> <i class="fa fa-binoculars" ></i> </button>';
                    }) !!}
                    {!! Form::SubmitBtn() !!}
                {!! Form::close() !!}

            </td>
        </tr>
    @endforeach
