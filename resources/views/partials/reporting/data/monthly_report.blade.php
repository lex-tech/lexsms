    @foreach ($monthlyMessages as $monthlyMessage)
        <tr>
            <td>{{ $monthlyMessage->id }}</td>
            <td>{{ $monthlyMessage->month}}</td>
            <td>{{ $monthlyMessage->sender->name }}</td>
            <td>{{ $monthlyMessage->total_messages_for_the_month }}</td>
            {{--<td>N$ {{ number_format($yearlyMessage->amount, 2) }}</td>--}}
            {{--<td>--}}
            {{--@if($yearlyMessage->payment_status)--}}
            {{--<i style="color: green" class="fa fa-check"></i>--}}
            {{--@else--}}
            {{--<i style="color: red" class="fa fa-times"></i>--}}
            {{--@endif--}}
            {{--</td>--}}
            {{--<td>--}}
            {{--@if($bill->payment_status)--}}
            {{--{{ $bill->payment_date }}--}}
            {{--@else--}}
            {{--{{'pending'}}--}}
            {{--@endif--}}
            {{--</td>--}}
            <td>
                {!! Form::open(['method' => 'POST', 'route' => ['reports.store'] ]) !!}
                {!! Form::text('expand-month',$monthlyMessage->sender->id,array('hidden')) !!}
                {!! Form::text('month',$monthlyMessage->id,array('hidden')) !!}
                {!! Form::macro('SubmitBtn',function (){
                    return '<button type="submit" class="btn btn-default"> <i class="fa fa-binoculars" ></i> </button>';
                }) !!}
                {!! Form::SubmitBtn() !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach