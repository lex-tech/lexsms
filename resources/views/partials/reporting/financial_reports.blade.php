@component('components.tables.table')

@slot('title', 'Financial Report' )

{{--<a href="{{ route('users.create') }}" class="btn btn-success">Add User</a>--}}
@include('partials.forms.financial_report')

@slot('thead')
    @include('partials.reporting.headers.financial_reporting')
@endslot

@slot('tfoot')
    {{--@include('partials.reporting.headers.financial_reporting')--}}
@endslot

@slot('tbody')
    @include('partials.reporting.data.fa_monthly')
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent