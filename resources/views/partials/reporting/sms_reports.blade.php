@component('components.tables.table')

@slot('title', 'SMS Report' )
@slot('default')
{{--customise the data-table here--}}
    <script>
        $(document).ready(function(){
            $(".dataTable").DataTable()
        });
    </script>
@endslot
<br>
{{-- filtering forms and modals are found in this file --}}
@include('partials.forms.sms_report')
<br>
@slot('thead')
    @if($day)
        <span class="text-center" style="text-decoration-line: underline"><h4 ><strong>Daily Report</strong></h4></span>
        @include('partials.reporting.headers.day')
    @endif

    @if($month)
        <span class="text-center" style="text-decoration-line: underline"><h4 ><strong>Monthly Report</strong></h4></span>
        @include('partials.reporting.headers.month')
    @endif

    @if($year)
        <span class="text-center" style="text-decoration-line: underline"><h4 ><strong>Yearly Report</strong></h4></span>
        @include('partials.reporting.headers.year')
    @endif

@endslot

@slot('tfoot')
    @if($day)
        @include('partials.reporting.headers.day')
    @endif

    @if($month)
        @include('partials.reporting.headers.month')
    @endif

    @if($year)
        @include('partials.reporting.headers.year')
    @endif
@endslot

@slot('tbody')

@if($year)
    @include('partials.reporting.data.yearly_report')
@endif
@if($day)
    @include('partials.reporting.data.daily_report')
@endif
@if($month)
    @include('partials.reporting.data.monthly_report')
@endif
@endslot

@slot('card_footer')
Updated yesterday at 11:59 PM
@endslot
@endcomponent