<footer class="sticky-footer">
    <div class="container">
        <div class="text-center"><small>Copyright © Lex Technologies 2017 - {{ \Carbon\Carbon::today()->format('Y') }}</small></div>
    </div>
</footer>