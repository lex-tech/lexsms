{{--<script src="{{asset('js/app.js')}}"></script>--}}

<!-- Custom scripts for all pages-->
<script src="{{asset('js/sb-admin.min.js')}}"></script>


{{--js file for searchable dropdown--}}
<script src="{{asset('vendor/bootstrap-select/bootstrap-select.min.js')}}"></script>

{{--js file for moment.js--}}
<script src="{{asset('vendor/moment/min/moment.min.js')}}"></script>

{{--js file for datetime picker--}}
<script src="{{asset('vendor/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js')}}"></script>

{{--CK Editor--}}
<script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>

{{--fade-out timer for the alerts--}}
<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

{{--overlay modals--}}
<script>
    $('#flash-overlay-modal').modal();
</script>
{{--rich text editor--}}
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script>
//    $('textarea').ckeditor();
     $('.textarea').ckeditor(); // if class is prefered.
</script>






