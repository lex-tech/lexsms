<!-- Modal -->
<div data-backdrop="false" id="sms_report_filter" class="modal fade" role="dialog">
    <div class="modal-dialog">

        {!! Form::open(array('route'=>'reports.store', 'method'=>'POST')) !!}
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="margin-top: 10px"> <i class="fa fa-filter"></i> Filter Report</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                {!! Form::label('start_date', 'From:') !!}
                                {{--create datetimepicker component--}}
                                {!! Form::component('dateTimePicker', 'components.timepicker.timepicker', ['name', 'id', 'attributes']) !!}
                                {!! Form::dateTimePicker('start_date', 'start-date-time-picker' , ['required'])
                                 !!}
                                @if ($errors->has('start_date'))
                                    <span class="help-block">
                                        <strong >{{ $errors->first('start_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            {{--Validate so that end date is after the selected start date--}}
                            <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                                {!! Form::label('end_date', 'To:') !!}
                                {{--instantiate datetimepicker component--}}
                                {!! Form::dateTimePicker('end_date', 'end-date-time-picker' , ['required'])
                                 !!}
                                @if ($errors->has('end_date'))
                                    <span class="help-block">
                                        <strong >{{ $errors->first('end_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    {!! Form::text('sms_report_filter',null,array( 'hidden' )) !!}

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-remove"></i> Cancel</button>
                    <button type="submit" class="btn btn-primary" > <i class="fa fa-filter"></i> Filter Report</button>
                </div>
            </div>
        {!! Form::close() !!}

    </div>
    </div>
</div>