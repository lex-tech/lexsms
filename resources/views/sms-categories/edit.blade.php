@extends('layouts.app')

@section('title', 'Edit SMS Category')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    {{--<li class="breadcrumb-item"><a href="{{ url('out/index') }}">SMS Categories</a> </li>--}}
    <li class="breadcrumb-item active">Edit SMS Category</li>
@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel panel-heading"><h4> <i class="fa fa-edit"></i> Edit SMS Category</h4></div>

        <div class="panel panel-body">

            {{ Form::model($sms_category, array('route' => array('sms-categories.update', $sms_category->id), 'method' => 'PUT')) }}

            <fieldset>
                <legend>Category Info</legend>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {{ Form::label('name', 'Name') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                    @if ($errors->has('name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    {{ Form::label('description', 'Description') }}
                    {{ Form::textarea('description', null, array('class' => 'form-control', 'rows' => 3)) }}
                    @if ($errors->has('description'))
                        <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
            </fieldset>
            <br>

            {!! Form::macro('SubmitBtn',function (){
                return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit" ></i> Update Category </button>';
            }) !!}

            <div class="form-group">
                {!! Form::SubmitBtn() !!}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection