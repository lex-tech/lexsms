
@extends('layouts.app')

@section('title', 'Edit Settings')

{{--custom css files--}}
@section('custom-css')
	<style>

	</style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
	<li class="breadcrumb-item active">Edit Settings</li>
@endsection

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
			System Settings
		</div>

		<div class="panel-body">
			@include('partials.forms.settings')
		</div>
	</div>
@endsection