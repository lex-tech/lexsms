@extends('layouts.app')

@section('title', 'Settings')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active">System Settings</li>
@endsection

@section('content')
    @include('flash::message')
    <div class="panel panel-default">
        <div class="panel-heading"><h4>System Settings</h4></div>
        <div class="panel-body">
            @include('partials.forms.settings_view')
        </div>
    </div>

@endsection