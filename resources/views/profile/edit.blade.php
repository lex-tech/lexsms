@extends('layouts.app')

@section('title', 'Edit Post')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    {{--<li class="breadcrumb-item">Profile</li>--}}
    <li class="breadcrumb-item active">Profile</li>
@endsection

@section('content')

    @include('flash::message')
    <div>
        <ul class="nav nav-tabs">
            <li class="{{ session('tab') == 'update_profile' || !session('tab') ? 'active': ''  }}">
                <a href="#update_profile" data-toggle="tab"> <i class="fa fa-user"></i> User Profile</a>
            </li>
        </ul>

        <div class="tab-content">
            {{--@can('Update Profile')--}}
                <div class="tab-pane {{ session('tab') == 'update_profile' || !session('tab')? 'active': ''  }}"
                     id="update_profile">
                    <div class="panel panel-body panel-default">
                        @include('partials.forms.edit_profile')
                    </div>
                </div>
            {{--@endcan--}}
        </div>
    </div>
@endsection