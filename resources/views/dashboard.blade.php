@extends('layouts.app')

@section('title')
    Dashboard
@endsection
{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
    {!! \ConsoleTVs\Charts\Facades\Charts::styles() !!}
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    {{--<li><a href=""></a></li>--}}
@endsection
@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>
        <div class="panel-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                <div>
                    @include('flash::message')
                    <ul class="nav nav-tabs">
                        <li class="{{ session('tab') == 'daily_report' || !session('tab') ? 'active': ''  }}">
                            <a href="#daily_report" data-toggle="tab"> <i class="fa fa-area-chart"></i> Quick Statistics </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                            <div class="tab-panel {{ !session('tab') == 'daily_report'? 'active': ''  }}" id="daily_report">
                                @include('partials.reporting.daily_report')
                            </div>
                    </div>
                </div>
        </div>
    </div>
    {{--    @include('partials.tables')--}}

@endsection


{{--custom javascript--}}

{{--prepend javascript--}}
@section('js-before')
    <script type="text/javascript">

    </script>
@endsection
{{--append javascript--}}
@section('js-after')
    <!-- Custom scripts for this page-->

    {!! \ConsoleTVs\Charts\Facades\Charts::scripts() !!}
    {!! $monthly_stats_chart->script() !!}

@endsection