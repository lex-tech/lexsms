@extends('layouts.app')

@section('title', 'Edit Group')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{url('groups')}}">Groups</a></li>
    <li class="breadcrumb-item active">Edit Group</li>
@endsection

@section('content')

    <div>
        @include('flash::message')
        <ul class="nav nav-tabs">
            @can('Update Groups')
                <li class="{{ session('tab') == 'update_group' || !session('tab') ? 'active': ''  }}">
                    <a href="#update_group" data-toggle="tab"> <i class="fa fa-edit"></i> Edit Group</a>
                </li>
            @endcan

            @can('Remove Recipients From Groups')
                <li class="{{ session('tab') == 'remove_recipients_from_group' ? 'active': ''  }}">
                    <a href="#remove_recipients_from_group" data-toggle="tab"> <i class="fa fa-list-ul"></i> Remove Recipients from group</a>
                </li>
            @endcan

        </ul>

        <div class="tab-content">

            @can('Update Groups')
            <div class="tab-pane {{ session('tab') == 'update_group' || !session('tab')? 'active': ''  }}"
                 id="update_group">
                <div class="panel panel-body panel-default">
                    @include('partials.forms.edit_group')
                </div>
            </div>
            @endcan

            @can('Remove Recipients From Groups')
                <div class="tab-pane {{ session('tab') == 'remove_recipients_from_group'? 'active': ''  }}" id="remove_recipients_from_group">
                    <div class="panel panel-default panel-body">
                        <fieldset>
                            @include('partials.tables.recipients_in_group')
                        </fieldset>
                    </div>
                </div>
            @endcan

        </div>

    </div>

@endsection

@section('js-after')
    <script>

        $(".checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

    </script>
@endsection