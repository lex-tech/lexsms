@extends('layouts.app')

@section('title', 'Groups')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active">Groups</li>
@endsection

@section('content')

        <div>
            @include('flash::message')
            <ul class="nav nav-tabs">
                @can('View Groups')
                    <li class="{{ session('tab') == 'view_groups' || !session('tab') ? 'active': ''  }}">
                        <a href="#view_groups" data-toggle="tab"> <i class="fa fa-list-ul"></i>  Groups</a>
                    </li>
                @endcan

                @can('Assign Recipients To Groups')
                    <li class="{{ session('tab') == 'assign_recipients_to_groups' ? 'active': ''  }}">
                        <a href="#assign_recipients_to_groups" data-toggle="tab"><i class='fa fa-address-book'></i> Assign Recipients To Groups</a>
                    </li>
                @endcan

                @can('Create Groups')
                    <li class="{{ session('tab') == 'create_groups' ? 'active': ''  }}">
                        <a href="#create_groups" data-toggle="tab"> <i class="fa fa-plus"></i> Create Groups</a>
                    </li>
                @endcan

                @can('Give Users Access To Groups')
                    <li class="{{ session('tab') == 'give_users_access_to_groups' ? 'active': ''  }}">
                        <a href="#give_users_access_to_groups" data-toggle="tab"> <i class='fa fa-superpowers'></i> Give Users Access To Groups</a>
                    </li>
                @endcan

            </ul>

            <div class="tab-content">
                @can('View Groups')
                    <div class="tab-pane {{ session('tab') == 'view_groups' || !session('tab')? 'active': ''  }}" id="view_groups">
                        <div class="panel panel-default">
                            <fieldset>
                                @include('partials.tables.groups')
                            </fieldset>
                        </div>
                    </div>
                @endcan

                @can('Assign Recipients To Groups')
                    <div class="tab-pane {{ session('tab') == 'assign_recipients_to_groups'? 'active': ''  }}"
                         id="assign_recipients_to_groups">
                        <div class="panel panel-default panel-body">
                            <fieldset>
                                <legend>Recipient & Groups</legend>
                                @include('partials.forms.assign_recipients_to_group')
                            </fieldset>
                        </div>
                    </div>
                @endcan

                @can('Create Groups')
                    <div class="tab-pane {{ session('tab') == 'create_groups'? 'active': ''  }}"
                         id="create_groups">
                        <div class="panel panel-default panel-body">

                            <fieldset>
                                <legend>
                                    Group Info
                                </legend>
                                @include('partials.forms.create_groups')
                            </fieldset>

                        </div>
                    </div>
                @endcan

                @can('Give Users Access To Groups')
                    <div class="tab-pane {{ session('tab') == 'give_users_access_to_groups'? 'active': ''  }}"
                         id="give_users_access_to_groups">
                        <div class="panel panel-default panel-body">
                            <fieldset>
                                <legend>User & Groups</legend>
                                @include('partials.forms.give_users_access_to_groups')
                            </fieldset>
                        </div>
                    </div>
                @endcan


            </div>
        </div>

@endsection