{{-- \resources\views\users\index.blade.php --}}
@extends('layouts.app')

@section('title', 'Users')

@section('custom-css')

@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active">Users</li>
@endsection

@section('content')


    <div>
        @include('flash::message')
        <ul class="nav nav-tabs">
            @can('View Users')
                <li class="{{ session('tab') == 'view_users' || !session('tab') ? 'active': ''  }}">
                    <a href="#view_users" data-toggle="tab"> <i class="fa fa-users"></i> Users</a>
                </li>
            @endcan
            @can('Create Users')
                <li class="{{ session('tab') == 'create_users' ? 'active': ''  }}">
                    <a href="#create_users" data-toggle="tab"> <i class="fa fa-user-plus"></i> Create Users</a>
                </li>
            @endcan

        </ul>

        <div class="tab-content">
            @can('View Users')
                <div class="tab-pane {{ session('tab') == 'view_users'? 'active': ''  }}" id="view_users">
                    <div class="panel panel-default panel-body">
                        <fieldset>
                            @include('partials.tables.users')
                        </fieldset>
                    </div>
                </div>
            @endcan

            @can('Create Users')
                <div class="tab-pane {{ session('tab') == 'create_users'? 'active': ''  }}"
                     id="create_users">
                        <div class="panel panel-default panel-body">
                            @include('partials.forms.create_users')
                        </div>
                </div>
            @endcan


        </div>
    </div>

@endsection

    {{--custom javascript--}}
@section('js-before')
    <script type="text/javascript">

    </script>
@endsection

@section('js-after')
    <script type="text/javascript">

    </script>
@endsection