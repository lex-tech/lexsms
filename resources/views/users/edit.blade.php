{{-- \resources\views\users\edit.blade.php --}}

@extends('layouts.app')

@section('title', 'Edit User')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="/users">Users</a></li>
    <li class="breadcrumb-item active">Edit User</li>
@endsection

@section('content')

    <div class="panel panel-default panel-flush">
        <div class="panel panel-heading">
            <h4><i class='fa fa-user-plus'></i> Edit {{$user->name}}</h4>
        </div>

        <div class="panel panel-body">
            @include('partials.forms.edit_user')
        </div>
    </div>

@endsection