@extends('layouts.app')

@section('title', 'Edit Membership')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrumb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{url('memberships')}}">Memberships</a></li>
    <li class="breadcrumb-item active">Edit Membership</li>
@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel panel-heading"><h4><i class="fa fa-edit"></i> Edit Membership</h4></div>

        <div class="panel panel-body">

          @include('partials.forms.edit_membership')

        </div>
    </div>
@endsection

@section('js-after')
    @include('components.jsFiles.start_and_end_datepicker')
@endsection