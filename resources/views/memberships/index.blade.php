@extends('layouts.app')

@section('title', 'View Memberships')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active"> Memberships</li>
@endsection

@section('content')

    <div>
        @include('flash::message')
        <ul class="nav nav-tabs">
            @can('View Memberships')
                <li class="{{ session('tab') == 'view_memberships' || !session('tab') ? 'active': ''  }}">
                    <a href="#memberships" data-toggle="tab">Memberships</a>
                </li>
            @endcan
            @can('Create Memberships')
                <li class="{{ session('tab') == 'create_memberships' ? 'active': ''  }}">
                    <a href="#create_memberships" data-toggle="tab"> Create New Membership</a>
                </li>
            @endcan
            @can('Create Client Memberships')
                <li class="{{ session('tab') == 'create_client_memberships'? 'active': ''  }}">
                    <a href="#create_client_memberships" data-toggle="tab">Create Client Memberships</a>
                </li>
            @endcan
                @can('View Client Memberships')
                    <li class="{{ session('tab') == 'view_client_memberships'? 'active': ''  }}">
                        <a href="#view_client_memberships" data-toggle="tab">View Client Memberships</a>
                    </li>
                @endcan
                @can('Messaging')
                    <li class="{{ session('tab') == 'messaging'? 'active': ''  }}">
                        <a href="#messaging" data-toggle="tab">Messaging</a>
                    </li>
                @endcan
        </ul>

        <div class="tab-content">
            @can('View Memberships')
                <div class="tab-pane {{ !session('tab') == 'view_memberships'? 'active': ''  }}" id="memberships">
                    @include('partials.memberships')
                </div>
            @endcan

            @can('Create Memberships')
                <div class="tab-pane {{ session('tab') == 'create_memberships'? 'active': ''  }}"
                     id="create_memberships">
                    <div class="panel panel-default panel-flush">
                        <div class="panel panel-heading">
                            <h4><i class='fa fa-plus'></i>Create New Membership </h4>
                        </div>
                        <div class="panel panel-body">
                            @include('partials.create_memberships')
                        </div>
                    </div>
                </div>
            @endcan

            @can('Create Client Memberships')
                <div class="tab-pane {{ session('tab') == 'create_client_memberships'? 'active': ''  }}" id="create_client_memberships">
                    <div class="panel panel-default panel-flush">
                        <div class="panel panel-heading">
                            <h4><i class='fa fa-get-pocket'></i> Create Client Memberships</h4>
                        </div>
                        <div class="panel panel-body">
                            @include('partials.forms.client_memberships')
                        </div>
                    </div>
                </div>
            @endcan

                @can('View Client Memberships')
                    <div class="tab-pane {{ session('tab') == 'view_client_memberships'? 'active': ''  }}" id="view_client_memberships">
                        <div class="panel panel-default panel-flush">
                            <div class="panel panel-heading">
                                <h4><i class='fa fa-get-pocket'></i> View Client Memberships</h4>
                            </div>
                            <div class="panel panel-body">
                                @include('partials.client_memberships_view')
                            </div>
                        </div>
                    </div>
                @endcan

                @can('Messaging')
                    <div class="tab-pane {{ session('tab') == 'messaging'? 'active': ''  }}" id="messaging">
                        <div class="panel panel-default panel-flush">
                            <div class="panel panel-heading">
                                <h4><i class='fa fa-get-pocket'></i> Messaging </h4>
                            </div>
                            <div class="panel panel-body">
                                @include('partials.messaging')
                            </div>
                        </div>
                    </div>
                @endcan
        </div>
    </div>
@endsection

@section('js-after')
   @include('components.jsFiles.start_and_end_datepicker')
@endsection