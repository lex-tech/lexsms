@extends('layouts.app')

@section('title', 'View Credit Transactions')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
        <li class="breadcrumb-item active">Credit Transactions</li>
@endsection

@section('content')
    @include('flash::message')
    <div>
        <ul class="nav nav-tabs">
            @can('View Credit Transactions')
                <li class="{{ (session('tab') == 'view_credit_transactions' || !session('tab') ) ? 'active' : ''  }}">
                    <a href="#view_credit_transactions" data-toggle="tab"> <i class="fa fa-list-ul"></i> Credit Allocation Transactions</a>
                </li>
            @endcan
            @can('View Credit Requests')
                <li class="{{ (session('tab') == 'view_credit_requests') ? 'active' : ''  }}">
                    <a href="#view_credit_requests" data-toggle="tab"><i class="fa fa-list-ul"></i>  Credit Requests</a>
                </li>
            @endcan
            @can('View Credit Reallocation Transactions')
                <li class="{{ (session('tab')) == 'view_credit_reallocation_transactions'? 'active' : ''  }}">
                    <a href="#credit_reallocation_transactions" data-toggle="tab">Reallocation Transactions</a>
                </li>
            @endcan
           @can('Request Credit')
                    <li class="{{(\Illuminate\Support\Facades\Auth::user()->hasRole(['Sub-Client','Client']) && (session('tab')) == '' ) || (session('tab')) == 'request_credit' ? 'active': ''}}">
                        <a href="#request_credit" data-toggle="tab"><i class='fa fa-exchange'></i> Request SMS Credit</a>
                    </li>
           @endcan

            @can('Create Credit')
                <li class="{{ (session('tab') == 'allocate_credit') ? 'active' : ''  }}">
                    <a href="#allocate_credit" data-toggle="tab"><i class="fa fa-credit-card"></i> Allocate Credit</a>
                </li>
            @endcan

            @can('Reallocate Credit')
                <li class="{{ (session('tab') == 'reallocate_credit') ? 'active' : ''  }}">
                    <a href="#reallocate_credit" data-toggle="tab"><i class="fa fa-reply-all"></i> Reallocate Credit</a>
                </li>
            @endcan

        </ul>

        <div class="tab-content">
                @can('View Credit Transactions')
                <div class="tab-pane {{ (session('tab') == 'view_credit_transactions' || !session('tab') ) ? 'active' : ''  }}" id="view_credit_transactions">
                   <div class="panel panel-default panel-body">
                       <fieldset>
                           @include('partials.credit')
                       </fieldset>
                   </div>
                </div>
                @endcan
                @can('View Credit Requests')
                    <div class="tab-pane {{ (session('tab') == 'view_credit_requests') ? 'active' : ''  }}" id="view_credit_requests">
                        <div class="panel panel-default panel-body">
                            <fieldset>
                                @include('partials.tables.credit_requests')
                            </fieldset>
                        </div>
                    </div>
                @endcan
                @can('View Credit Reallocation Transactions')
                    <div class="tab-pane {{ (session('tab')) == 'view_credit_reallocation_transactions'? 'active' : ''  }}" id="credit_reallocation_transactions">
                        <div class="panel panel-default panel-body">
                            <fieldset>
                                @include('partials.tables.credit_reallocation')
                            </fieldset>
                        </div>
                    </div>
                @endcan
                @can('Request Credit')

                    <div class="tab-pane {{ (\Illuminate\Support\Facades\Auth::user()->hasRole(['Sub-Client','Client']) && (session('tab')) == '' ) || (session('tab')) == 'request_credit' ? 'active': ''  }}" id="request_credit">
                            <div class="panel panel-default panel-body">
                                <fieldset>
                                    @include('partials.request_credit')
                                </fieldset>
                            </div>
                    </div>
                @endcan

                @can('Create Credit')

                    <div class="tab-pane {{ (session('tab')) == 'allocate_credit' ? 'active' : ''  }}" id="allocate_credit">
                            <div class="panel panel-default panel-body">
                                <fieldset>
                                    @include('partials.forms.allocate_credit')
                                </fieldset>
                            </div>
                    </div>

                @endcan
                @can('Reallocate Credit')

                    <div class="tab-pane {{ (session('tab') == 'reallocate_credit') ? 'active' : ''  }}" id="reallocate_credit">
                            <div class="panel panel-default panel-body">
                                <fieldset>
                                    @include('partials.forms.reallocate_credit')
                                </fieldset>
                            </div>
                    </div>
                @endcan
        </div>
    </div>
@endsection