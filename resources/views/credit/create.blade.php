@extends('layouts.app')

{{--page title--}}
@section('title', 'Assign Credit')


{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active">Allocate Credit</li>
@endsection
{{--content--}}
@section('content')

    <div class="panel panel-default">

        <div class="panel panel-heading"><h4>Allocate Credit</h4></div>

        <div class="panel panel-body">
            @include('flash::message')
            {{ Form::open(array('route' => 'credits.store', 'method' => 'POST')) }}

            <div class="form-group{{ $errors->has('credit') ? ' has-error' : '' }}">
                {{ Form::label('name', 'Credit') }}
                {{ Form::text('credit', null, array('class' => 'form-control')) }}
                @if ($errors->has('credit'))
                    <span class="help-block">
                        <strong>{{ $errors->first('credit') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('client') ? ' has-error' : '' }}">
                {{ Form::label('client', 'Description') }}
                {!! Form::component('customSelect', 'components.select.select_clients', ['name', 'select_tag_attributes', 'option_tag_attributes', 'lists']) !!}
                {!! Form::customSelect('client', ['class="selectpicker form-control show-menu-arrow"',
                                                        'data-live-search="true"', 'data-header="Select Client"',
                                                         'showSubtext="true"', 'data-width="75%"', 'title="Choose client to allocate credit to"'], [],  $clients)
                 !!}
                @if ($errors->has('client'))
                    <span class="help-block">
                        <strong>{{ $errors->first('client') }}</strong>
                    </span>
                @endif
            </div>

            {{ Form::submit('Allocate Credit', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}

        </div>
    </div>

@endsection

{{--custom javascript--}}
@section('js-before')
    <script type="text/javascript">

    </script>
@endsection

@section('js-after')
    <script type="text/javascript">

    </script>
@endsection