@extends('layouts.app')

@section('title', 'Edit Contact')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{url('recipients')}}">Contacts</a></li>
    <li class="breadcrumb-item active">Edit Contact</li>
@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel panel-heading"><h4> <i class="fa fa-edit"></i> Edit Contact</h4></div>

        <div class="panel panel-body">
            @include('partials.forms.edit_recipient')
        </div>
    </div>
@endsection
