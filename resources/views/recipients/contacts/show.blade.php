@extends('layouts.app')

@section('title', 'View {{ put some dynamic data here }}')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="/recipients">Recipients</a> </li>
    <li class="breadcrumb-item active">Show Recipient {{ 'id' }}</li>
@endsection

@section('content')


    <div class="panel panel-default">
        <div class="panel panel-heading">

        </div>


        <div class="panel panel-body">

            @include('flash::message')

        </div>


    </div>


@endsection