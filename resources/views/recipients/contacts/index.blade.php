@extends('layouts.app')

@section('title', 'Contacts')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active">Contacts</li>
@endsection

@section('content')

    <div>
        @include('flash::message')
        <ul class="nav nav-tabs">
            @can('View Recipients')
                <li class="{{ (session('tab') == 'view_recipients' || !session('tab')) ? 'active': ''  }}">
                    <a href="#view_recipients" data-toggle="tab"> <i class="fa fa-address-card"></i> Recipients</a>
                </li>
            @endcan
            @can('Create Recipients')
                <li class="{{ session('tab') == 'create_recipients' ? 'active': ''  }}">
                    <a href="#create_recipients" data-toggle="tab"><i class='fa fa-plus'></i> Create Recipients</a>
                </li>
            @endcan

            @can('Create Bulk Recipients')
                <li class="{{ session('tab') == 'create_bulk_recipients' ? 'active': ''  }}">
                    <a href="#create_bulk_recipients" data-toggle="tab"><i class="fa fa-upload"></i> Create Bulk Recipients</a>
                </li>
            @endcan

        </ul>

        <div class="tab-content">
            @can('View Recipients')
                <div class="tab-pane {{ (session('tab') == 'view_recipients' || !session('tab')) ? 'active': ''  }}" id="view_recipients">
                   <div class="panel panel-default panel-body">
                       <fieldset>
                           @include('partials.tables.contacts')
                       </fieldset>
                   </div>
                </div>
            @endcan

            @can('Create Recipients')
                <div class="tab-pane {{ session('tab') == 'create_recipients'? 'active': ''  }}"
                     id="create_recipients">
                        <div class="panel panel-default panel-body">
                                @include('partials.forms.create_recipients')
                        </div>
                </div>
            @endcan

            @can('Create Bulk Recipients')
                <div class="tab-pane {{ session('tab') == 'create_bulk_recipients'? 'active': ''  }}"
                     id="create_bulk_recipients">
                    <div class="panel panel-body panel-default ">
                        <fieldset>
                            @include('partials.forms.import_bulk_recipients')
                        </fieldset>
                    </div>
                </div>
            @endcan
        </div>
    </div>

@endsection

{{--custom javascript--}}
@section('js-before')
    <script type="text/javascript">

    </script>
@endsection

@section('js-after')
    <script type="text/javascript">

    </script>
@endsection