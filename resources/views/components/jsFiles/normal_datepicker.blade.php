<script type="text/javascript">
    $(document).ready(function () {

//        sms report
        $('#start-date-time-picker').datetimepicker({
            useCurrent: false,
            format: 'DD/M/Y'
        });
        $('#end-date-time-picker').datetimepicker({
            format: 'DD/M/Y'
        });
        $('#start-date-time-picker').on("dp.change", function (e) {
            $('#end-date-time-picker').data("DateTimePicker").minDate(e.date);
        });
        $('#end-date-time-picker').on("dp.change", function (e) {
            $('#start-date-time-picker').data("DateTimePicker").maxDate(e.date);
        });
//        financial report
        $('#start-date-time-picker-financial').datetimepicker({
            useCurrent: false,
            format: 'MM/Y'
        });
        $('#end-date-time-picker-financial').datetimepicker({
            format: 'MM/Y'
        });
        $('#start-date-time-picker-financial').on("dp.change", function (e) {
            $('#end-date-time-picker-financial').data("DateTimePicker").minDate(e.date);
        });
        $('#end-date-time-picker-financial').on("dp.change", function (e) {
            $('#start-date-time-picker-financial').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>