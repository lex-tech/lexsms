@extends('layouts.app')

@section('title', 'Reports')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active"> Reports </li>
@endsection

@section('content')

    @can('View Reports')
        @include('flash::message')
        <ul class="nav nav-tabs">
            @can('View SMS Reports')
                <li class="{{ session('tab') == 'sms_reports' || !session('tab') ? 'active': ''  }}">
                    <a href="#sms_reports" data-toggle="tab"> <i class="fa fa-envelope"></i> SMS Reports </a>
                </li>
            @endcan
            @can('View Financial Reports')
                <li class="{{ session('tab') == 'financial_reports' ? 'active': ''  }}">
                    <a href="#financial_reports" data-toggle="tab"> <i class="fa fa-dollar"></i> Financial Reports</a>
                </li>
            @endcan

        </ul>

        <div class="tab-content">
            @can('View SMS Reports')
                <div class="tab-pane {{ session('tab') == 'sms_reports' || !session('tab') ? 'active': ''  }}" id="sms_reports">
                    <div class="panel panel-default panel-body">
                        <fieldset>
                            @include('partials.reporting.sms_reports')
                        </fieldset>
                    </div>
                </div>
            @endcan


            @can('View Financial Reports')
                <div class="tab-pane {{ session('tab') == 'financial_reports'? 'active': ''  }}"
                     id="financial_reports">
                    <div class="panel panel-default panel-body">
                        <fieldset>
                            @include('partials.reporting.financial_reports')
                        </fieldset>
                    </div>
                </div>
            @endcan

        </div>
    @endcan
    {{--    @include('partials.tables')--}}

    @endsection


@section('js-after')
    @include('components.jsFiles.normal_datepicker')
@endsection