@extends('layouts.app')

@section('title', 'Edit Post')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="{{url('subscriptions')}}">Subscriptions</a></li>
    <li class="breadcrumb-item active">Edit Subscription</li>
@endsection

@section('content')

    <div class="panel panel-default">

        <div class="panel panel-heading"><h4> <i class="fa fa-edit"></i> Edit Subscription</h4></div>

        <div class="panel panel-body">

            {{ Form::model($subscription, array('route' => array('subscriptions.update', $subscription->id), 'method' => 'PUT')) }}
                <fieldset>
                    <legend>Subscription Info</legend>
                    <div class="form-group">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name',null, array('class' => 'form-control', 'required')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('duration', 'Duration:') !!}
                        {!! Form::text('duration',null, array('class' => 'form-control', 'required')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('sms_number', 'SMS Quantity:') !!}
                        {!! Form::text('sms_number',null, array('class' => 'form-control', 'required')) !!}
                    </div>
                </fieldset>
                <br>

                {!! Form::macro('SubmitBtn',function (){
                    return '<button type="submit" class="btn btn-primary"> <i class="fa fa-edit" ></i> UpdateSubscription </button>';
                }) !!}

            <div class="form-group">
                {!! Form::SubmitBtn() !!}
            </div>
            {{ Form::close() }}

        </div>
    </div>
@endsection