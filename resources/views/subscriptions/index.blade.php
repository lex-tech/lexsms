@extends('layouts.app')

@section('title', 'View Subscriptions')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active"> Subscriptions</li>
@endsection

@section('content')

    <div>
        @include('flash::message')
        <ul class="nav nav-tabs">
            @can('View Subscriptions')
                <li class="{{ session('tab') == 'view_subscriptions' || !session('tab') ? 'active': ''  }}">
                    <a href="#view_subscriptions" data-toggle="tab"> <i class="fa fa-list-ul"></i>  Subscriptions</a>
                </li>
            @endcan
            @can('Create Subscriptions')
                <li class="{{ session('tab') == 'create_subscriptions' ? 'active': ''  }}">
                    <a href="#create_subscriptions" data-toggle="tab"> <i class="fa fa-plus"></i> Create Subscription</a>
                </li>
            @endcan
            @can('Subscribe Users')
                <li class="{{ session('tab') == 'subscribe_users'? 'active': ''  }}">
                    <a href="#subscribe_users" data-toggle="tab"> <i class='fa fa-get-pocket'></i> Subscribe Clients</a>
                </li>
            @endcan
        </ul>

        <div class="tab-content">
            @can('View Subscriptions')
                <div class="tab-pane {{ session('tab') == 'view_subscriptions'? 'active': ''  }}" id="view_subscriptions">
                    <div class="panel panel-default">
                        <fieldset>
                            @include('partials.subscriptions')
                        </fieldset>
                    </div>
                </div>
            @endcan

            @can('Create Subscriptions')
                <div class="tab-pane {{ session('tab') == 'create_subscriptions'? 'active': ''  }}"
                     id="create_subscriptions">
                    <div class="panel panel-default panel-flush">
                        <div class="panel panel-body">
                            @include('partials.create_subscriptions')
                        </div>
                    </div>
                </div>
            @endcan

            @can('Subscribe Users')
                <div class="tab-pane {{ session('tab') == 'subscribe_users'? 'active': ''  }}" id="subscribe_users">
                    <div class="panel panel-default panel-flush">
                        {{--<div class="panel panel-heading">--}}
                            {{--<h4><i class='fa fa-get-pocket'></i> Subscribe Clients</h4>--}}
                        {{--</div>--}}
                        <div class="panel panel-body">
                            @include('partials.subscribe')
                        </div>
                    </div>
                </div>
            @endcan
        </div>
    </div>
@endsection