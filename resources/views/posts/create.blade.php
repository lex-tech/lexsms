@extends('layouts.app')

@section('title', 'Create New Post')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item">Posts</li>
    <li class="breadcrumb-item active">Create Post</li>
@endsection

@section('content')
    {{--  <div class="row">  --}}
        {{--  <div class="col-md-8 col-md-offset-2">  --}}

    <div class="panel panel-default">
    
        <div class="panel panel-heading">
            <h4>Create New Post</h4>
        </div>

        <div class="panel panel-body">
            @include('flash::message')

            {{-- Using the Laravel HTML Form Collective to create our form --}}
        {{ Form::open(array('route' => 'posts.store')) }}

        <div class="form-group">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, array('class' => 'form-control')) }}
            <br>

            {{ Form::label('body', 'Post Body') }}
            {{ Form::textarea('body', null, array('class' => 'form-control')) }}
            <br>

            {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block')) }}
            {{ Form::close() }}
        </div>
        
        </div>
    </div>

    {{--  </div>  --}}
    {{--  </div>  --}}


@endsection