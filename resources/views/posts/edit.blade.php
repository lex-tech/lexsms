@extends('layouts.app')

@section('title', 'Edit Post')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item">Posts</li>
    <li class="breadcrumb-item active">Edit Post</li>
@endsection

@section('content')
    {{--  <div class="row">  --}}

    {{--  <div class="col-md-8">  --}}

    <div class="panel panel-default">

        <div class="panel panel-heading"><h4>Edit Post</h4></div>

        <div class="panel panel-body">

            {{ Form::model($post, array('route' => array('posts.update', $post->id), 'method' => 'PUT')) }}
            <div class="form-group">
                {{ Form::label('title', 'Title') }}
                {{ Form::text('title', null, array('class' => 'form-control')) }}<br>

                {{ Form::label('body', 'Post Body') }}
                {{ Form::textarea('body', null, array('class' => 'form-control')) }}<br>

                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}

            </div>

        </div>
        {{--  </div>  --}}
        {{--  </div>  --}}
    </div>

@endsection