@extends('layouts.app')

@section('title', 'View Post')

@section('content')

    {{--  <div class="container">  --}}

    <div class="panel panel-default">
        <div class="panel panel-heading">
            <h4>{{ $post->title }}</h4>
        </div>


        <div class="panel panel-body">
            <p class="lead">{{ $post->body }} </p>
            <hr>
            {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $post->id] ]) !!}
            <a href="{{ url()->previous() }}" class="btn btn-primary"> << Back</a>
            @can('Update Posts')
                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-info" role="button">Edit</a>
            @endcan
            @can('Delete Posts')
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            @endcan
            {!! Form::close() !!}
        </div>


    </div>



    {{--  </div>  --}}

@endsection