@extends('layouts.app')

@section('title', 'Create Role')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="/roles">Roles</a></li>
    <li class="breadcrumb-item active">Create Role</li>
@endsection

@section('content')

    {{--  <div class='col-lg-4 col-lg-offset-4'>  --}}

    <div class="panel panel-default">
        <div class="panel panel-heading">
            <h4><i class='fa fa-key'></i>Create Role</h4>
        </div>
        <div class="panel panel-body">

            @include('flash::message')

            {{ Form::open(array('url' => 'roles')) }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', null, array('class' => 'form-control')) }}
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <h5><b>Assign Permissions To Role</b></h5>
            <div class='form-group{{ $errors->has('permissions') ? ' has-error' : '' }}'>
                @if ($errors->has('permissions'))
                    <span class="help-block">
                        <strong>{{ $errors->first('permissions') }}</strong>
                    </span>
                @endif
                @foreach ($permissions as $permission)
                    {{ Form::checkbox('permissions[]',  $permission->id ) }}
                    {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
                @endforeach
            </div>

            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
    </div>

    {{--  </div>  --}}

@endsection