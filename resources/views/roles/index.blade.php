{{-- \resources\views\roles\index.blade.php --}}
@extends('layouts.app')

@section('title', '| Roles')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item active">Roles</li>
@endsection

@section('content')

    @include('flash::message')

    <ul class="nav nav-tabs">
        @can('View Roles')
            <li class="{{ session('tab') == 'view_roles' || !session('tab') ? 'active': ''  }}">
                <a href="#view_roles" data-toggle="tab"> <i class="fa fa-list-ul"></i>  Roles</a>
            </li>
        @endcan
        @can('Create Roles')
            <li class="{{ session('tab') == 'create_roles' ? 'active': ''  }}">
                <a href="#create_roles" data-toggle="tab"> <i class="fa fa-plus"></i>  Create Roles</a>
            </li>
        @endcan

    </ul>

    <div class="tab-content">
        @can('View Roles')
            <div class="tab-pane {{ session('tab') == 'view_roles'? 'active': ''  }}" id="view_roles">
                <div class="panel panel-default panel-body">
                    <fieldset>
                        @include('partials.tables.roles')
                    </fieldset>
                </div>
            </div>
        @endcan

        @can('Create Roles')
            <div class="tab-pane {{ session('tab') == 'create_roles'? 'active': ''  }}"
                 id="create_roles">
                {{--<div class="panel panel-default panel-flush">--}}
                    {{--<div class="panel panel-heading">--}}
                        {{--<h4><i class='fa fa-pocket'></i> Create Roles </h4>--}}
                    {{--</div>--}}
                    <div class="panel panel-default panel-body">
                            @include('partials.forms.create_roles')
                    </div>
                {{--</div>--}}
            </div>
        @endcan




    </div>

@endsection