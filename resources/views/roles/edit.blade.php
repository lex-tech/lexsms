@extends('layouts.app')

@section('title', 'Edit Role')

{{--custom css files--}}
@section('custom-css')
    <style>

    </style>
@endsection

{{--breadcrmb links--}}
@section('breadcrumb-item')
    <li class="breadcrumb-item"><a href="/roles">Roles</a> </li>
    <li class="breadcrumb-item active">Edit Role</li>
@endsection

@section('content')

    {{--  <div class='col-lg-4 col-lg-offset-4'>  --}}

    <div class="panel panel-default">
        <div class="panel panel-heading">
            <h4><i class='fa fa-key'></i> Edit Role: {{$role->name}}</h4>
        </div>

        <div class="panel panel-body">
            {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {{ Form::label('name', 'Role Name') }}
                {{ Form::text('name', null, array('class' => 'form-control')) }}
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <h5><b>Assign Permissions</b></h5>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
            @foreach ($permissions as $permission)

                {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
                {{Form::label($permission->name, ucfirst($permission->name)) }}<br>

            @endforeach
            <br>
            {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
    </div>



    {{--  </div>  --}}

@endsection